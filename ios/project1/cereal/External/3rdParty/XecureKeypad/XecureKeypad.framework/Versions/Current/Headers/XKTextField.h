//
//  XKTextField.h
//  CustomViewTest
//
//  Created by maeng on 13. 7. 25..
//  Copyright (c) 2013년 softforum. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, XKeypadViewType)
{
	XKeypadViewTypeNormalView = 0,			// 0 : 일반 키패드 뷰 타입
	XKeypadViewTypeFullView					// 1 : 전체화면 타입
};

typedef NS_ENUM (NSInteger, XKeypadType)
{
	XKeypadTypeNumber = 0,				// 0 : 숫자 키패드
	XKeypadTypeQwerty,					// 1 : 쿼티 키패드
	XKeypadTypeLetter,					// 2 : 문자 키패드
	XKeypadTypeUpperLetter,				// 3 : 문자 키패드 (대문자)
	XKeypadTypeLowerLetter				// 4 : 문자 키패드 (소문자)
	
};

typedef NS_ENUM (NSInteger, XKeypadPreViewType)
{
    XKeypadPreViewOn = 0,            // 0 : 가이드뷰 켜짐
    XKeypadPreViewOff                // 1 : 가이드뷰 꺼짐
};

@class XKTextField;

@protocol XKTextFieldDelegate
@optional
- (void) keypadInputCompleted:(NSInteger)aCount;

- (void) keypadInputCompleted:(XKTextField *) textField
						count:(NSInteger) count;

- (void) keypadE2EInputCompleted:(NSString *)aSessionID
                           token:(NSString *)aToken
                      indexCount:(NSInteger)aCount;

- (void) keypadE2EInputCompleted:(XKTextField *) textField
					   sessionID:(NSString *) sessionID
						   token:(NSString *) token
					  indexCount:(NSInteger) count;

- (void) keypadCanceled;

- (BOOL) getXKPreViewCancleButtonHidden;

- (void) keypadCreateFailed:(NSInteger) errorCode;
//youngmoon.nah 20180828 add
- (void) keypadCreateStart;
- (void) keypadCreateCompleted;

- (BOOL)textField:(XKTextField *)textField shouldChangeLength:(NSUInteger)length;
- (BOOL)textFieldShouldDeleteCharacter:(XKTextField *)textField;
- (void)textFieldSessionTimeOut:(XKTextField *)textField;

@end

@interface XKTextField : UITextField <UITextFieldDelegate, XKTextFieldDelegate>

@property (nonatomic, retain) id                    returnDelegate;
@property (nonatomic, assign) XKeypadViewType	    xkeypadViewType;
@property (nonatomic, assign) XKeypadType           xkeypadType;
@property (nonatomic, assign) XKeypadPreViewType    xkeypadPreViewType;
@property (nonatomic, retain) NSString *            e2eURL;
@property (nonatomic, retain) NSNumber *            keypadID;
@property (nonatomic, retain) NSString *            subTitle;
@property (nonatomic, assign) BOOL                  isFromFullView;
@property (nonatomic, assign) BOOL				    returnCompletedTextField;
@property (nonatomic, assign) BOOL                  xkPreViewCancleButtonHidden;
@property (nonatomic, assign) BOOL                  xkNormalViewDimOff;
//youngmoon.nah 20180822 user requested
@property (nonatomic, assign) NSInteger             xkInputMaxLength;

- (void) startKeypadWithSender:(UIViewController *)sender;
- (void) cancelKeypad;

//youngmoon.nah 20180822 user requested
- (void) setCustomNaviBar:(UIBarButtonItem *)barBttonItem
                 barTitle:(NSString *)barTitle;

- (void) setBackgroundImage:(UIImage *)backgroundImage
                      alpha:(CGFloat)alpha;

- (void) setBackgroundColor:(UIColor *)backgroundColor;

- (void) setBlankLogoImage:(UIImage *)blankLogoImage;

- (void) setUseInputButton:(BOOL)flag;

- (NSString *) getData;
- (NSString *) getDataE2E;
//youngmoon.nah
- (NSString *) getSessionIDE2E;
- (NSString *) getTokenE2E;
- (NSUInteger) getSessionTimeE2E;

- (NSString *) getEncryptedDataWithKey:(NSData *) key;

- (NSString *) getExternalEncryptedDataWithKeypadVendor:(NSString *) keypadVendor
                                                    Key:(NSData *) key;
@end
