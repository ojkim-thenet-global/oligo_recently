#ifndef _XASObfuscate_h
#define _XASObfuscate_h


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct _memory_t
{
    // memory size used
    int used;
    // memory size left
    int left;
    
    // internal memory pointer
    void *_mem;
} memory_t;


static void memory_init(memory_t *mem, int maxsize)
{
    mem->left = maxsize;
    mem->used = 0;
    mem->_mem = malloc(maxsize);
}

static void *memory_get(memory_t *mem, int size)
{
    if(mem->left < size) return NULL;
    
    void *ret = ((unsigned char *) mem->_mem) + mem->used;
    mem->used += size;
    mem->left -= size;
    
    return ret;
}

static void memory_status(memory_t *mem, int *used, int *left)
{
    *used = mem->used;
    *left = mem->left;
}

static void memory_destroy(memory_t *mem)
{
    free(mem->_mem);
    memset(mem, 0, sizeof(memory_t));
}


#define FORCEDINLINE __attribute__((always_inline))

// simple encryption/decryption function which
// xor's the input with 0x42.
FORCEDINLINE static void _object_crypt(void *obj, int size)
{
    for (int i = 0; i < size; i++) {
        ((unsigned char *) obj)[i] ^= 0x42;
    }
}

FORCEDINLINE static void memory_init_obf(memory_t *mem, int maxsize)
{
    memory_init(mem, maxsize);
    // encrypt memory context
    _object_crypt(mem, sizeof(memory_t));
}

FORCEDINLINE static void *memory_get_obf(memory_t *mem, int size)
{
    // decrypt memory context
    _object_crypt(mem, sizeof(memory_t));
    // call original function
    void *ret = memory_get(mem, size);
    // encrypt memory context
    _object_crypt(mem, sizeof(memory_t));
    // return the.. return value..
    return ret;
}

FORCEDINLINE static void memory_status_obf(memory_t *mem, int *used, int *left)
{
    // decrypt memory context
    _object_crypt(mem, sizeof(memory_t));
    // call original function
    memory_status(mem, used, left);
    // encrypt memory context
    _object_crypt(mem, sizeof(memory_t));
}

FORCEDINLINE static void memory_destroy_obf(memory_t *mem)
{
    // decrypt memory context
    _object_crypt(mem, sizeof(memory_t));
    // call original function
    memory_destroy(mem);
    // no need to encrypt, context is no longer
    // valid anyway
}

#define memory_init memory_init_obf
#define memory_get memory_get_obf
#define memory_status memory_status_obf
#define memory_destroy memory_destroy_obf



FORCEDINLINE static void obfuscate ()
{
    memory_t mem;
    memory_init(&mem, 1000);
    
    void *a = memory_get(&mem, 10);
    void *b = memory_get(&mem, 20);
    
    // do something with `a' and `b'
    
    // get the status (and compare it with
    // the "encrypted" status)
    int used, left;
    memory_status(&mem, &used, &left);
    //    printf("Real status: %d %d\n", used, left);
    //    printf("Encrypted status: %d, %d\n", mem.used, mem.left);
    
    memory_destroy(&mem);
}



#endif

