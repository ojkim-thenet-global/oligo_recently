#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define XAS_DEVICE_STATUS_NORMAL			0x00
#define XAS_DEVICE_STATUS_UPDATE			0x01        // XAS_DEVICE_STATUS_JAILBREAK 에서 분석하기 어렵게 하기위해 UPDATE로 수정
#define XAS_DEVICE_STATUS_DEBUGGING			0x02
#define XAS_DEVICE_STATUS_NONENCRYPTED		0x04

typedef enum{
    SHA1=1, SHA256
} HashType;

@interface XASInterface : NSObject
{
    NSInteger	SID;
	NSString *	Token;
    bool Base64Enable;
    HashType HashFunction;
}

@property (nonatomic, readonly)	NSInteger	SID;
@property (nonatomic, readonly)	NSString *	Token;
@property (nonatomic) bool Base64Enable;
@property (nonatomic) HashType HashFunction;

- (NSInteger) checkApp:(NSString *) aURL
			   appInfo:(NSString *) aAppInfo;

- (NSInteger) checkApp:(NSString *) aURL
			   appInfo:(NSString *) aAppInfo
            identifier:(NSString *) aIdentifier;

- (NSString *) errorMessage:(NSInteger) aErrorNumber;

+ (NSString *) getXASVersion;

- (void) checkAntiDebug;

@end
