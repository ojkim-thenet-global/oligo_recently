//
//  GlobalFunction.swift
//  cereal
//
//  Created by srkang on 2018. 7. 3..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation
import XCGLogger
import AlamofireActivityLogger

//let log = XCGLogger.default

let log: XCGLogger? = {
    let log = XCGLogger(identifier: "CerialLogger", includeDefaultDestinations: false)
    
    // Customize as needed
    
    let systemDestination = AppleSystemLogDestination(identifier: "CerialLogger.systemDestination")
    
    // Optionally set some configuration options
    systemDestination.outputLevel = .debug
    systemDestination.showLogIdentifier = true
    systemDestination.showFunctionName = true
    systemDestination.showThreadName = true
    systemDestination.showLevel = true
    systemDestination.showFileName = true
    systemDestination.showLineNumber = true
    systemDestination.showDate = true
    
    // Add the destination to the logger
    log.add(destination: systemDestination)
    
    log.setup(level: .debug, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true)
    
    #if DEBUG
    log.setup(level: .debug, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true)
    return log
    #else
    return nil
    #endif

}()

// alamofire_activity_logger 로그레벨
var alamofireLevel :  LogLevel = {
    var level = LogLevel.none
    #if DEBUG
    level = LogLevel.all
    #endif
    
    return level
}()


//NetworkActivityLogger.shared.level = .all

typealias VoidClousre = () -> ()

func delay(_ delay:Double, closure:@escaping ()-> Void) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func print(debug: Any = "", function: String = #function, file: String = #file, line: Int = #line) {
    #if DEBUG
    var filename: NSString = file as NSString
    filename = filename.lastPathComponent as NSString
    Swift.print("\(filename), \(line), \(function)\(debug)")
    #endif
}







