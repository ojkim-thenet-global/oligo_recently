//
//  MBProgressHUD+Cereal.swift
//  cereal
//
//  Created by srkang on 2018. 7. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SnapKit

private class GlobalHudWrapper
{
    static var globalHUD : MBProgressHUD? = nil
}


extension MBProgressHUD {
    
//    static var gloabalHud : MBProgressHUD? {
//        get {
//            return GlobalHudWrapper.globalHUD
//        }
//        set {
//            GlobalHudWrapper.globalHUD = newValue
//        }
//    }
    
    static var notificationTimer : Timer?
        
    static func showCerealCHUDAdded(to view : UIView , animated: Bool) -> MBProgressHUD {
        
        let hud = MBProgressHUD.showAdded(to: view, animated: animated)
        hud.backgroundView.backgroundColor = .clear
        hud.mode = .customView
        hud.customView = loadingCustomView()
        
        hud.bezelView.backgroundColor = .clear
        hud.bezelView.color = .clear
        hud.bezelView.style = .solidColor
        hud.layer.zPosition = 20;
        
//        notificationTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(MBProgressHUD.timerTrigger), userInfo: nil, repeats: false)
        
        return hud
    }
    
    
//    @objc static func timerTrigger () {
//        gloabalHud?.hide(animated: false)
//    }
    
    
    static func loadingCustomView() -> UIView {
        
        var imageArray = [UIImage]()
        
        for index in 1...30 {
            let imageName = String(format: "loading%d", index)
            imageArray.append(UIImage(named: imageName)!)
        }
        
        // Normal Animation`
        let animationImageView = UIImageView(frame: .zero)
        animationImageView.animationImages = imageArray;
        animationImageView.animationDuration = 1.0;
        
        animationImageView.startAnimating()
        

        
        
        return animationImageView
    }
}
