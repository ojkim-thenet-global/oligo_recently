//
//  Optional+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 7. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation
import Alamofire

extension Optional {
    
    var isNil: Bool {
        if case .none = self {
            return true
        }
        
        return false
    }
}


