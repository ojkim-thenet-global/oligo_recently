//
//  UITransitionContextViewControllerKey+Extension.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit


extension UITransitionContextViewControllerKey {
    
    public static let top: UITransitionContextViewControllerKey  = UITransitionContextViewControllerKey(rawValue: "top")
    public static let move: UITransitionContextViewControllerKey  = UITransitionContextViewControllerKey(rawValue: "move")
}
