//
//  NotificationName+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 7. 6..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

extension Notification.Name {
    
    static let DOAlertActionEnabledDidChangeNotification              = Notification.Name("DOAlertActionEnabledDidChangeNotification")
    
    static let ActionLogin              = Notification.Name("ActionLogin")
    static let ActionLogout             = Notification.Name("ActionLogout")
    
    static let BadgeCount              = Notification.Name("BadgeCount")
    
    
    static let ShowSecurityKeyboard     = Notification.Name("ShowSecurityKeyboard")
    static let ShowLogin                = Notification.Name("ShowLogin")
    
    static let ShowProfileSetting       = Notification.Name("ShowProfileSetting")
    static let ShowPermission           = Notification.Name("ShowPermission")
    static let ShowAnalytics            = Notification.Name("ShowAnalytics")
    
    static let OpenMenu                 = Notification.Name("OpenMenu")
    static let GetPicker                = Notification.Name("GetPicker")
    
    static let ChangeWebUrl             = Notification.Name("ChangeWebUrl")
    static let ShowKakaoTest            = Notification.Name("ShowKakaoTest")
    
    static let PhotoChange              = Notification.Name("PhotoChange")
    static let NickNameChange           = Notification.Name("NicknameChange")
    static let InvestTypeChange         = Notification.Name("InvestTypeChange")
    static let DefaultImageChange       = Notification.Name("DefaultImageChange")
    
    static let ForgroundPushLink       = Notification.Name("ForgroundPushLink")
    static let ForgroundURLLink        = Notification.Name("ForgroundURLLink")
    
    static let MenuSlide               = Notification.Name("MenuSlide")
    
    
    func post(object:Any? = nil, userInfo:[AnyHashable: Any]? = nil) {
        NotificationCenter.default.post(name: self, object: object, userInfo: userInfo)
    }
    
    
    func addObserver(_ observer: Any, selector aSelector: Selector , object anObject: Any? = nil) {
        NotificationCenter.default.addObserver(observer, selector: aSelector, name: self, object: anObject)
    }
    
    
    func removeObserver(_ observer: Any,  object anObject: Any? = nil) {
        NotificationCenter.default.removeObserver(observer, name: self, object: anObject)
    }

    
   
}

