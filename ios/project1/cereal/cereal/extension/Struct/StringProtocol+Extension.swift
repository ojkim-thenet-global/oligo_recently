//
//  StringProtocol+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 7. 17..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

extension StringProtocol where Index == String.Index {
    func nsRange<T: StringProtocol>(of string: T, options: String.CompareOptions = [], range: Range<Index>? = nil, locale: Locale? = nil) -> NSRange? {
        guard let range = self.range(of: string, options: options, range: range ?? startIndex..<endIndex, locale: locale ?? .current) else { return nil }
        return NSRange(range, in: self)
    }
    func nsRanges<T: StringProtocol>(of string: T, options: String.CompareOptions = [], range: Range<Index>? = nil, locale: Locale? = nil) -> [NSRange] {
        var start = range?.lowerBound ?? startIndex
        let end = range?.upperBound ?? endIndex
        var ranges: [NSRange] = []
        while start < end, let range = self.range(of: string, options: options, range: start..<end, locale: locale ?? .current) {
            ranges.append(NSRange(range, in: self))
            start = range.upperBound
        }
        return ranges
    }
}

// https://stackoverflow.com/questions/32305891/substring-from-a-string-in-swift-index-of-a-substring-in-a-string

extension StringProtocol where Index == String.Index {
    func index<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.lowerBound < range.upperBound ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
    func ranges<T: StringProtocol>(of string: T, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while start < endIndex, let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.lowerBound < range.upperBound  ? range.upperBound : index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}

/*
 https://stackoverflow.com/questions/32305891/substring-from-a-string-in-swift-index-of-a-substring-in-a-string
 
 https://stackoverflow.com/questions/43233070/how-to-convert-range-in-nsrange
 
 
 let string = "Hello USA 🇺🇸 !!! Hello World !!!"
 
 if let range = string.range(of: "Hello World") {
 let nsRange = string.nsRange(from: range)
 (string as NSString).substring(with: nsRange) //  "Hello World"
 }
 
 let string = "Hello USA 🇺🇸 !!! Hello World !!!"
 
 if let nsRange = string.nsRange(of: "Hello World") {
 (string as NSString).substring(with: nsRange) //  "Hello World"
 }
 let nsRanges = string.nsRanges(of: "Hello")
 print(nsRanges)   // "[{0, 5}, {19, 5}]\n"

 
*/
