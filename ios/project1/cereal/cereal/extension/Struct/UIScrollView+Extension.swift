//
//  UIScrollView.swift
//  cereal
//
//  Created by srkang on 2018. 8. 24..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

extension UIScrollView {
    var currentPage: Int {
        
//        return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)
        
        get {
            return Int((self.contentOffset.x + (0.5*self.frame.size.width))/self.frame.width)
        }
        set {
            self.contentOffset.x = self.frame.size.width * CGFloat(newValue)
        }
        
    }
}
