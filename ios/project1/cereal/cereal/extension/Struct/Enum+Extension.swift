//
//  Enum+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 7. 2..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation


enum DelegateButtonAction {
    case close
    case login
    case logout
    case memberJoin
    case changeID
    case change90Pw
    case findPw
    case invalidToken
}


