//
//  Date+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 9. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

extension Date {
    
    static func todayCompare(compare : String) ->  ComparisonResult  {
        
        let todayString = Date.todayString()
        
        return todayString.compare(compare, options: .numeric)
        
    }
    
    static func todayString() ->  String  {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let todayString = dateFormatter.string(from: Date())
        
        return todayString
        
    }
    
    
    
//    static func todayLessOrEqualThan(compare : String) -> Bool {
//
//    }
//
//    static func todayGreaterThan(compare : String) -> Bool {
//
//    }
//
//    static func todayGreateerOrEqualThan(compare : String) -> Bool {
//
//    }
    
    

}
