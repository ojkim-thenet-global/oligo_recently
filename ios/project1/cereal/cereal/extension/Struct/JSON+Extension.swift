//
//  JSON+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 9. 4..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SwiftyJSON

extension JSON{
    mutating func appendIfArray(json:JSON){
        if var arr = self.array{
            arr.append(json)
            self = JSON(arr);
        }
    }
    
    mutating func appendIfDictionary(key:String,json:JSON){
        if var dict = self.dictionary{
            dict[key] = json;
            self = JSON(dict);
        }
    }
}
