//
//  UIViewController+SlidingViewController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit


extension UIViewController  {
    
    func slidingViewController() -> OKSlidingViewController? {
        var viewController = self.parent ?? self.presentingViewController
        
        while !(viewController == nil || viewController is OKSlidingViewController)  {
            viewController = viewController?.parent ?? viewController?.presentingViewController
        }
        
        return viewController as? OKSlidingViewController
        
    }
    
    @objc func mainWebViewChangeWebUrl(_ urlString:String) {
        
    }
    
    static func windowTopViewController() -> UIViewController? {
        
        guard let window = UIApplication.shared.keyWindow else { return nil}
        guard let rootViewController = window.rootViewController else { return nil}
        
        return UIViewController.topViewControllerWithViewController(rootViewController)
        
    }
    
    static func topViewControllerWithViewController(_ viewController : UIViewController) -> UIViewController? {
        
        if viewController is UITabBarController  {
            let tapBarController = viewController as! UITabBarController
            return UIViewController.topViewControllerWithViewController(tapBarController.selectedViewController!)
        } else if viewController is UINavigationController  {
            let navigationController = viewController as! UINavigationController
            return UIViewController.topViewControllerWithViewController(navigationController.visibleViewController!)
        } else if let  presentedViewController = viewController.presentedViewController {
            return UIViewController.topViewControllerWithViewController(presentedViewController)
        } else {
            return viewController
        }
       
    }
    
    
   
    
    
//    + (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
//
//    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
//    UITabBarController* tabBarController = (UITabBarController*)rootViewController;
//    return  [[self class] topViewControllerWithRootViewController:tabBarController.selectedViewController];
//    }
//    else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
//    UINavigationController* navigationController = (UINavigationController*)rootViewController;
//    return  [[self class] topViewControllerWithRootViewController:navigationController.visibleViewController];
//    }
//    else if (rootViewController.presentedViewController) {
//    UIViewController* presentedViewController = rootViewController.presentedViewController;
//    return  [[self class] topViewControllerWithRootViewController:presentedViewController];
//    }
//    else {
//    return rootViewController;
//    }
//    }
}

