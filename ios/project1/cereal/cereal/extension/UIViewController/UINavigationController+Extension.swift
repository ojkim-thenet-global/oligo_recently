//
//  UINavigationController+Extension.swift
//  cereal
//
//  Created by srkang on 2018. 8. 7..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

extension UINavigationController  {
    func secondPushViewController(_ viewController:UIViewController) {
        
        let mainViewController = self.viewControllers[0]
        
        self.viewControllers = [mainViewController , viewController]
    }
}
