//
//  AppDelegate.swift

//
//  Created by srkang on 2018. 6. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import SnapKit
import Reachability
import LaunchScreenSnapshot
import Fabric
import WebKit
import GLNotificationBar

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability: Reachability?
    
    
    var firstCheckReachability = false
    
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    
    var doAlertController : DOAlertController!
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ApplicationShare.timeInterval =  Date().timeIntervalSince1970
        
        // 디바이스 정보 ( 아이폰 5,5s,SE 처럼 해상도 작은 버전 체크 용도) => OKMenuViewController 의 메뉴 화면에서 해상도 낮은 버전에 constraint  적용 용도
        screenInfoSave()
    
        // WK 웹뷰 캐시 지움.
        webViewClearCache()
        
        
        // UIApplicationLaunchOptionsRemoteNotificationKey
        
        // Remote  Push 또는 OpenURL 통해서 Launch (앱구동) 된 경우
        if let launchOptions = launchOptions {
            
            // kakaod518fc58b4558ef9f39f2e6152bdb395://kakaolink?param1=value1&param2=value2
//             log?.debug("launchOptions url: \(String(describing: launchOptions[.url]))")
            // com.iwilab.KakaoTalk
//             log?.debug("launchOptions sourceApplication: \(String(describing: launchOptions[.sourceApplication]))")
            
            
            // FCM  APNS 푸시 통해 온 경우
            if let remoteNotification = launchOptions[.remoteNotification] as?  [AnyHashable : Any] {
                
                let aps = remoteNotification["aps" as String] as? [String:AnyObject]
                let apsString =  String(describing: aps)
                log?.debug("\n last incoming aps: \(apsString)")
                
                // 푸시 메시지안의 url 정보를 뽑아서 ApplicationShare.cerealPushLinkURLString 에 대입
                // 나중에 메인화면(CerealMainWebViewController) 의 웹뷰 url 로 사용 용도
                if let url = remoteNotification["url"] as? String {
                    ApplicationShare.cerealPushLinkURLString = url
                    log?.debug("\n url: \(url)")
                }
                
                
            } else if let url = launchOptions[.url] as? URL {
                
                // App2App URL Open 으로 들어 오는 경우
                // 올리고 앱에서는 2가지만 처리 ( 카카오톡 링크 에서 온 경우,  웹뷰에서 cereal:// 이라는 URL 스킴으로 들어 온 경우)
                
                // 카카오톡 링크 샘플 kakaod518fc58b4558ef9f39f2e6152bdb395://kakaolink?param1=value1&param2=value2
                
                log?.debug("url: \(url))")
                
                // 1번 케이스 카카오 링크 인 경우
                if KLKTalkLinkCenter.shared().isTalkLinkCallback(url) {
                    
                    // 디버깅 용도 - 카카오톡 맞는지 //sourceApplication (com.iwilab.KakaoTalk)
                    if #available(iOS 9, *) {
                        log?.debug("sourceApplication: \(String(describing: launchOptions[.sourceApplication]))")
                    }
                    
                     //  NSURL query 는 iOS7 에 소개됨
                    if let params = url.query {
                        
//                        let template = "<<<Hello>>>"
//                        let indexStartOfText = template.index(template.startIndex, offsetBy: 3)
//                        let newStr = params[..<indexStartOfText]
                        
//                        if let index = params.index(of:"&param") {
//                             let newStr = params[..<index]
//                        }
                        
                        if let range = params.range(of: "&param=") ,  range.isEmpty == false {
                            //url=mem/memIntro.do
                            let newStr1  = params[..<range.lowerBound]
                            let newStr2  = params[range.upperBound..<params.endIndex]
                            
                            
                            log?.debug("newStr1:\(newStr1)")
                            log?.debug("newStr2:\(newStr2)")
                            
                        }
                    }

                     // 1번 케이스 카카오 링크 인 경우
                } else if url.absoluteString.hasPrefix("cereal") {
                    // 2번 케이스 cereal:// 스킴으로 들어 오는 경우
                    let absoluteUrlString = url.absoluteString
                    
                    // 올리고는 cereal://movepage?url=URL정보로 약속함.
                    if let rangeCereal = absoluteUrlString.range(of: "cereal://movepage?url=") {
                        
                        let substring = absoluteUrlString[rangeCereal.upperBound...]
                        var string = String(substring)
                        string = string.replacingOccurrences(of: ".do&", with: ".do?")
                        
                        
                        let cerealAppLinkURLString = string
                        
                        // URL 스킴의 URL 정보에서 올리고 웹뷰 url 정보를 뽑아서 cerealAppLinkURLString 에 대입
                        // 나중에 메인화면(CerealMainWebViewController) 의 웹뷰 url 로 사용 용도
                        
                        // App2App URL 방식은 Push 와 다르게
                        // App2App 통해 Lanch(구동) 되는 경우 didFinishLaunchingWithOptions 이벤트도 타고,
                        // open url 이벤트도 탄다.   open url 메소드 안에서 didFinishLaunchingWithOptions 에서
                        // 처리 했으니, 무시하라는 용도로 cerealAppLinkLaunch 라는 Bool 값으로 처리.
                        
                        ApplicationShare.cerealAppLinkLaunch = true
                        ApplicationShare.cerealAppLinkURLString = cerealAppLinkURLString
                        
                    }
                } // else if url.absoluteString.hasPrefix("cereal")
            } // else if let url = launchOptions[.url] as? URL
            
            
            log?.debug("launchOptions:\(launchOptions)")
        }
        
         //Notification.Name.UIApplicationWillResignActive.addObserver(self, selector: #selector(backgroundEnter(_:)))
        Notification.Name.UIApplicationWillEnterForeground.addObserver(self, selector: #selector(enterForground(_:)))
        
        // 백신 체크
        protect()

        // Fabric Crashlystics (크래시 리포트) 설정
        Fabric.sharedSDK().debug = true
        
        // FirebaseApp (FCM) 설정
        FirebaseApp.configure()
        
        // 앱 권한 설정 셋팅 : 1.푸시 , 2.주소록, 3.카메라(사용안함), 4.사진
        addPermissionScope()
        
        notificationCheck()
     
        
        windowMakeKeyAndVisible()
        
        startHost()
        
        // 백그라운드 진입시, 스크린샷 이미지를 보여줌 ( 보안 이슈로 보편적인 방법)
        backgroundProtect()
        
        setApplicationBadge(badgeNumber: 0)
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        
        
        // UserDefault 에 keyChainInstalled 가 없을 경우는, 신규설치하거나, 앱삭제후 설치 . 키체인 내용을 삭제 한다
        if !defaults.bool(forKey: CerealConstrants.NSUserDefaultsKeys.keyChainInstalled)  {
            if !keyChainWrapper.removeAllKeys() {
            }
        }
        
        defaults.set(true,forKey: CerealConstrants.NSUserDefaultsKeys.keyChainInstalled)
        defaults.synchronize()
        
        
        if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey )  {
            // 키체인에 존재
            log?.debug("custItem:\(custItem)")
        } else {
            
            // 키체인 없을 경우 새롭게 생성
            let keyChainCustItem = KeyChainCustItem()
            keyChainCustItem.auto_login = true
            keyChainWrapper.set(keyChainCustItem, forKey: Configuration.keyChainKey)
            
        }
        
        // FIXME://
//        if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey ) as? KeyChainCustItem  {
//            // 키체인에 존재
//            custItem.auto_login = true
//            custItem.token = "LO-fdde53f149af4789950b17f438a44b89"
//            custItem.user_hp = "01075168891"
//            custItem.user_no = "R5VG6807"

//
//            keyChainWrapper.set(custItem, forKey: Configuration.keyChainKey)
//        }
//
        
        
        if let bundleIdentifier = Configuration.bundleIdentifier {
            log?.debug("bundleIdentifier:\(bundleIdentifier)")
        }
        
        return true
    }
    
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if var topController = application.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController;
            }
            if topController is OKSlidingViewController {
                let oKSlidingViewController = topController as? OKSlidingViewController;
                let topViewController = oKSlidingViewController?.topViewController;
                if topViewController is UINavigationController {
                    let navigationController = topViewController as? UINavigationController;
                    let lastViewController = navigationController?.viewControllers.last;
                    if lastViewController is CerealMainWebViewController {
                        let cerealMainWebViewController = lastViewController as? CerealMainWebViewController;
                        cerealMainWebViewController?.didEnterBackground();
                    }
                }
            }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if var topController = application.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController;
            }
            if topController is OKSlidingViewController {
                let oKSlidingViewController = topController as? OKSlidingViewController;
                let topViewController = oKSlidingViewController?.topViewController;
                if topViewController is UINavigationController {
                    let navigationController = topViewController as? UINavigationController;
                    let lastViewController = navigationController?.viewControllers.last;
                    if lastViewController is CerealMainWebViewController {
                        let cerealMainWebViewController = lastViewController as? CerealMainWebViewController;
                        cerealMainWebViewController?.willEnterForeground();
                    }
                }
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //MARK:// Open URL
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if KLKTalkLinkCenter.shared().isTalkLinkCallback(url) {
            let params = url.query
            UIAlertController.showMessage("카카오링크 메시지 액션\n\(params ?? "파라미터 없음")")
            return true
        } else {
            
        }
        
        return false
    }
    
    
     //MARK:// Open URL iOS9
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        if KLKTalkLinkCenter.shared().isTalkLinkCallback(url) {
            
//            log?.debug(options[.annotation])
//            log?.debug(options[.openInPlace])
            
            if #available(iOS 9, *) {
                //com.iwilab.KakaoTalk
                log?.debug("sourceApplication: \(String(describing: options[.sourceApplication]))")
            }
            
            let params = url.query
            UIAlertController.showMessage("카카오링크 메시지 액션\n\(params ?? "파라미터 없음")")
            return true
        } else if url.absoluteString.hasPrefix("cereal") {
            
            // 론칭 통해 올 경우 무시. 중복처리 할 필요 가 없음.
            // 론칭 통해 오지않고, 앱 실행중일때만 처리한다.
            if !ApplicationShare.cerealAppLinkLaunch {
                let absoluteUrlString = url.absoluteString
                
                if let rangeCereal = absoluteUrlString.range(of: "cereal://movepage?url=") {
                    
                    let substring = absoluteUrlString[rangeCereal.upperBound...]
                    var string = String(substring)
                    string = string.replacingOccurrences(of: ".do&", with: ".do?")
                    
                    let cerealForgroundAppLinkURLString = string
                    
                    ApplicationShare.cerealForgroundAppLinkURLString = cerealForgroundAppLinkURLString
                    
                    Notification.Name.ForgroundURLLink.post()
                    
                }
            }
            
            ApplicationShare.cerealAppLinkLaunch = false
            
            
            
        } else {
            
        }
        return false
    }

    //MARK:// Push iOS9
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
            
        }
        
        NSLog("Firebase APNs token retrieved: \(deviceToken)")
        
        log?.debug(token)
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    
    
    // iOS9 이하 버전
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
       
        if let messageID = userInfo[CerealConstrants.FCM.gcmMessageIDKey] {
            log?.debug("didReceiveRemoteNotification Message ID: \(messageID)")
        }
        
        
        let aps     = userInfo["aps"] as! [String : Any]
        let alert   = aps["alert"]  as! [String : Any]
        let title   = alert["title"] as! String
        let body    = alert["body"]  as! String
        
        
        let style:GLNotificationStyle = .simpleBanner
        
        _ = GLNotificationBar(title: title, message:body , preferredStyle:style) { (bool) in
           
            if bool {
                log?.debug("bool true")
                
                if let url = userInfo["url"] as? String {
                    ApplicationShare.cerealForgroundPushLinkURLString = url
                    
                    Notification.Name.ForgroundPushLink.post()
                }
                
            } else {
                log?.debug("bool false ")
            }
        }
        
        // Print full message.
        log?.debug(userInfo)
    }
    
    // 푸시 사용 권한이 허가(authorized) 된 경우 fcmRegister() 메소드 호출 하여 FCM 및 Push 수신 등록한다.
    func notificationCheck() {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusNotifications(completion: { (status) in
                
                delay(0.0) {
                    if status ==  .authorized {
                        self.fcmRegister()
                    }
                }
            })
        }
    }
    
    func protect() {
        
        if let secuManager = IxSecureManager.shared() {
            secuManager.initLicense("TEST_LICENSE")
            secuManager.start()
            if secuManager.check() {
                log?.debug("Problem")
            }else {
                log?.debug("Normal")
            }
        }
        
        
    }
    
    // FCM 수신 등록 Messaging.messaging().delegate 및 remotePush register 한다.
    func fcmRegister() {
        
        let application = UIApplication.shared
        // remotePush register
        application.registerForRemoteNotifications()
        // FCM Message  Delegate 등록
        Messaging.messaging().delegate = self
       
        // iOS10 이상에서는 UNUserNotificationCenter 통해서 메시지를 수신 받음.
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        
        let token = Messaging.messaging().fcmToken
        log?.debug("FCM token: \(token ?? "")")
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey ) as? KeyChainCustItem {
            log?.debug("custItem:\(custItem)")
            custItem.fcm_token = token
            keyChainWrapper.set(custItem, forKey: Configuration.keyChainKey)
        }
        
    }
    
    
    func windowMakeKeyAndVisible() {
        
        let rect = UIScreen.main.bounds
        
        if window == nil {
            window = UIWindow(frame: rect)
        }
        
        if let window = window {
         
            let storyboard = UIStoryboard.storyboard(.intro)
            let  introViewController = storyboard.instantiateViewController() as UINavigationController
            window.rootViewController = introViewController
           
            
            window.makeKeyAndVisible()
        }
        
    }
    
    
    
   
    func checkNotificationType(type : UIUserNotificationType , completion: (Bool) -> Void ) {
        if let  currentSettings = UIApplication.shared.currentUserNotificationSettings {
            return completion(currentSettings.types.contains(type))
            
        } else {
            return completion(false);
        }
        
        
    }
    
    
    @available(iOS 10, *)
    func checkNotificationType(options : UNAuthorizationOptions  , completion: @escaping (Bool) -> Void ) {
        let  center = UNUserNotificationCenter.current()
        
        
        //UNNotificationSettings
        center.getNotificationSettings { (settings) in
            
            switch options {
            case .alert:
                completion(settings.alertSetting == .enabled)
            case .sound:
                completion(settings.soundSetting == .enabled)
            case .badge:
                completion(settings.badgeSetting == .enabled)
            case .carPlay:
                completion(settings.carPlaySetting == .enabled)
            default:
                completion(false)
            }
        }
        
    }
  
    
    
    // 앱 권한 설정 셋팅 : 1.푸시 , 2.주소록, 3.카메라(사용안함), 4.사진
    func addPermissionScope() {
        
        let permissionScope = PermissionScope()
        
        // 1. Notification 푸시 권한
        // iOS10 은 UNNotificationCenter 이용함.
        if #available(iOS 10, *) {
            permissionScope.addPermission(NotificationsPermission(unNotificationCategories: nil),
                                          message: "We use this to send you\r\nspam and love notes")
        } else {
            permissionScope.addPermission(NotificationsPermission(uiNotificationCategories:  nil),
                                          message: "We use this to send you\r\nspam and love notes")
        }
        
        // 2. 주소록 Contacts  권한
        permissionScope.addPermission(ContactsPermission(),
                                      message: "We use this to ContactsPermission\r\nyour friends")
        
        // 3. 카메라 Camera  권한 => 사용하지 않으기로 함. (프로필 사진은 사진앱 통해서만, 카메라 사용하는 것 없음)
        permissionScope.addPermission(CameraPermission(),
                                      message: "We use this to CameraPermission\r\nyour friends")
        
        // 4. 사진 Photo 권한 => 프로필 사진 등록 용도
        permissionScope.addPermission(PhotosPermission(),
                                      message: "We use this to PhotosPermission\r\nyour friends")
        
        
        ApplicationShare.shared.permissionInfo.permissionScope = permissionScope
    }
    
    func setApplicationBadge(badgeNumber : Int) {
        
       
        if #available(iOS 10, *) {
            checkNotificationType(options: .alert) { (enabled) in
                if enabled {
                    delay(0.01) {
                        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
                    }
                }
            }
        } else {
            checkNotificationType(type: .alert) { (enabled) in
                if enabled {
                    delay(0.01) {
                        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
                    }
                }
            }
        }
      
    }
    
    // // WK 웹뷰 캐시 지움.
    func webViewClearCache() {
        
        // iOS 9 이상  WKWebsiteDataStore 는 iOS9 에서 소개됨
        if #available(iOS 9.0, *)
        {
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
            let date = NSDate(timeIntervalSince1970: 0)
            
            WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler:{ })
        } else  {
             // iOS 8 이하
            var libraryPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, false).first!
            libraryPath += "/Cookies"
            
            do {
                try FileManager.default.removeItem(atPath: libraryPath)
            } catch {
                log?.debug("error")
            }
            URLCache.shared.removeAllCachedResponses()
        }
        
    }
    
    // 디바이스 정보 ( 아이폰 5,5s,SE 처럼 해상도 작은 버전 체크 용도) => OKMenuViewController 의 메뉴 화면에서 해상도 낮은 버전에 constraint  적용 용도
    func screenInfoSave() {
        let width  = UIScreen.main.bounds.size.width
        let height  = UIScreen.main.bounds.size.height
        
        if  width <= 320 {
            // 아이폰 5,5s,SE 처럼 4' 인치 작은 버전은 smallScreen true
            ApplicationShare.shared.screenSize.smallScreen = true
        }  else if  width <= 375 && height <= 667 {
            // 아이폰 6, 6s, 7 , 8 처럼 4'7 인치 버전  middleScreen true
            ApplicationShare.shared.screenSize.middleScreen = true
        }
    }
    
    
    @objc func enterForground(_ notification : Notification) {
        
        if !firstCheckReachability && doAlertController != nil  {
             doAlertController.dismiss(animated: false, completion: nil)
        }
        
    }
    
  
    
}

extension AppDelegate {
    
    func startHost() {
        
        stopNotifier()
        setupReachability(NetworkInterface.SERVER_URL)
        startNotifier()
    
    }
    
    // 백그라운드 진입시, 스크린샷 이미지를 보여줌 ( 보안 이슈로 보편적인 방법)
    func backgroundProtect() {
        let launchScreenSnapshot = LaunchScreenSnapshot.shared
        
        
        let restoreAnimationOptions = LaunchScreenSnapshot.Animation(duration: 0.3  , delay: 0.1 )
        
        
        let securityViewController = UIStoryboard.storyboard(.security).instantiateInitialViewController()
        
        launchScreenSnapshot.protect(with: securityViewController?.view, trigger: .didEnterBackground , animation:restoreAnimationOptions)
    }
    
  
    
    func setupReachability(_ hostName: String?) {
        let reachability: Reachability?
        
        let hostname = NetworkInterface.SERVER_ROOT_DOMAIN
        
        
        reachability = Reachability(hostname: hostname)
        
        self.reachability = reachability
        
        // 접속 했을 때
        reachability?.whenReachable = { reachability in
            
            if !self.firstCheckReachability {
                self.firstCheckReachability = true
            } else {
                self.alertNetwork(true)
            }
            
        }
        // 접속 불가능 할 때
        reachability?.whenUnreachable = { reachability in
            self.alertNetwork(false)
        }
       
    }
    
    func alertNetwork(_ isConnected : Bool) {
        
        if let rootViewController = self.window?.rootViewController {
            
            var message : String!
            
            if isConnected {
                message = "네트워크 재연결 되었습니다."
            }else {	
                message = "네트워크를 연결할 수 없습니다.\n네트워크 상태를 확인해 주세요."
                
                var presentPossible = false
                if doAlertController != nil && doAlertController.isBeingPresented {
                    doAlertController.dismiss(animated: false, completion: nil)
                    presentPossible = true
                } else if rootViewController.presentedViewController == nil {
                    presentPossible = true
                }
                
                if presentPossible {
                    doAlertController = DOAlertController(title: nil, message: message, preferredStyle: .alert)
                    doAlertController!.addAction(DOAlertAction(title: "확인", style: .default) { _ in
                        
                        if !ApplicationShare.mainEntered {
                            exit(0)
                        }
                        
                    })
                    rootViewController.present(doAlertController!, animated:true)
                }
            }
            
            ApplicationShare.isConnected = isConnected

        }
    }
    
    func startNotifier() {
        log?.debug("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            return
        }
    }
    
    func stopNotifier() {
        log?.debug("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
}



@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        log?.debug("willPresent userInfo ID: \(userInfo)")
        
        if let messageID = userInfo[CerealConstrants.FCM.gcmMessageIDKey] {
            log?.debug("Message ID: \(messageID)")
        }
        
        if let url = userInfo["url"] {
            log?.debug("url:\(url)")
        }
        
        // Print full message.
        log?.debug(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert,  .sound])
    }
    
    // 카테고리 Custom Action
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[CerealConstrants.FCM.gcmMessageIDKey] {
            log?.debug("didReceive Message ID: \(messageID)")
        }
        
        if let url = userInfo["url"] as? String {
            ApplicationShare.cerealForgroundPushLinkURLString = url
            
            Notification.Name.ForgroundPushLink.post()
        }
        
        // Print full message.
        log?.debug(userInfo)
        
        completionHandler()
    }
}


extension AppDelegate :  MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        NSLog("AppDelegate Firebase registration token:%@",fcmToken);
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        log?.debug("AppDelegate data message: \(remoteMessage.appData)")
    }
    
}


