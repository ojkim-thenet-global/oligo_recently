//
//  SecureNumPadInterface.swift
//  cereal
//
//  Created by srkang on 2018. 7. 3..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit


class SecurePadInterface {
    
    static let COMMAND_SECURE_KEY_SHOW = "secureKeyShow"
    
    let e2eURLString =  NetworkInterface.ServerOperation.keyboardE2E.fullURLString()
    
    var xkTextField: XKTextField!
    var xkKeypadViewType : XKeypadViewType? = .normalView
  
    var isFirst = false
    
    weak var viewController : UIViewController!
    var command : Command!
    var resultCallback : ResultCallback!
    
    var allFinished = false
    
    var securityTapView: UIView!
    var keyboardShow = false // 보안 키보드가 올라갔는지 체크
    
    /// init 초기화 메소드
    /// command 는 Array<Any> 타입 : 하이브리드 인터페이스 에서 받은 message.body 임
    // message 는 3개로 나누어짐. [0] : 콜백 배열 , CallbackArray , [1] : action명 , [2] : 파라미터 배열, Arguments Array
    init(viewController: UIViewController, command: Command, result: @escaping (HybridResult) -> ()) {
        self.command(viewController: viewController, command: command, result: result)
    }
    
    
    /// mainKeypadInputCompleted( aCount , finished, tuple)
    /// 보안키패드 입력 될 때 마다 받는 콜백
    /// - Parameters:
    ///   - aCount: 입력 length
    ///   - finished:  true : 입력완료 , false : 입력 완료 아님
    ///   - tuple: finished 가 true 일 때  tuple 입력 받음. ( 보안 키패드 관련된 sessionid, token , e2edata)
    func mainKeypadInputCompleted(_ aCount: Int,  finished : Bool = false, tuple : (String,String)? = nil ) {
        callbackInputCompleted(aCount,finished: finished,tuple : tuple)
    }

    
    /// callbackInputCompleted( aCount , finished, tuple)
    /// 보안키패드 입력에 대해서 하이브리드에 리턴 함. resultCallback 클로저 함수 통해서
    /// - Parameters:
    ///   - aCount: 입력 length
    ///   - finished:  true : 입력완료 , false : 입력 완료 아님
    ///   - tuple: finished 가 true 일 때  tuple 입력 받음. ( 보안 키패드 관련된 sessionid, token , e2edata)
    func callbackInputCompleted(_ aCount: Int , finished : Bool = false , tuple : (String,String)?  = nil) {
        
        guard !allFinished else {
            return
        }
        
        var retMessage : [String:Any] = [:]
        
        let ret : [String : Any] = ["count" :  aCount ]
        
        if let   ( sessionid ,  token) = tuple {
            let mData = xkTextField.getDataE2E()
            
            var aSessionID = xkTextField.getSessionIDE2E()
            
            if aSessionID!.count < 8 {
                let appendZero = "0"
                let count = aSessionID!.count
                let paddingCount = 8 - count
                let i = 0
                for  _ in i ..< paddingCount {
                    aSessionID = appendZero + aSessionID!
                }
                
            }
          
            
            
            
            let append : [String : Any] =  ["sessionid" : aSessionID! , "token" :  token , "e2edata" :  mData! ]
            let mergeResult = ret.merging(append) { $1 }
            retMessage = mergeResult
        } else {
            retMessage["result"] = ret
        }
        
        // 입력 완료 (finished == true) 일 경우 , 보안키패드 관련된 항목 ( sessionid, token , e2edata) 를 JSON 형태로 보낸다.
        if finished {
            
            allFinished = true
            keyboardDismiss()
            do {
                let data =  try JSONSerialization.data(withJSONObject: retMessage, options:.prettyPrinted)
                if let dataString = String.init(data: data, encoding: .utf8) {
                    let argsAarray = command[2] as! [Any]
                    if let maxLen = argsAarray[1] as? Int   {
                        if ( aCount < maxLen ) {
                            resultCallback( HybridResult.fail(error: CerealError.businessError(reason: "보안키패드 입력 길이 자리수 오류"), errorMessage: "보안키패드 입력 길이 자리수 오류") )
                        } else {
                            resultCallback( HybridResult.success(message: dataString ))
                        }
                    }
                }
            } catch {
                resultCallback( HybridResult.fail(error: error, errorMessage: "보안키패드 결과 JSON 리턴 오류") )
            }
        } else {
            // 입력 완료 (finished == false) 가 아닌경우, 즉 입력단어 길이 변화 되얼때, 문자 길이를 리턴한다.
            resultCallback( HybridResult.progress(message: "\(aCount)" ))
        }
        
    }
    
    deinit {
        log?.debug("SecureNumPad deinit")
        xkTextField.removeFromSuperview()
    }
    
    
    // 키패드 닫기
    func keyboardDismiss() {
        xkTextField.resignFirstResponder()
        securityTapView.isUserInteractionEnabled = false
        securityTapView.removeFromSuperview()
    }
    
    func keyboardSessionTimeout() {
        let alert = UIAlertController(title: "", message: "보안 세션이 만료되었습니다.\n다시 실행해 주세요.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "확인", style: .default, handler: nil));
        self.viewController.present(alert, animated: true, completion: nil);
    }
    
    func uiControl() {
        
        securityTapView = UIView()
        viewController.view.addSubview(securityTapView)
        
        securityTapView.snp.makeConstraints { (make) in
            make.edges.equalTo(viewController.view)
        }
        
    }
    
    //MARK:// GestureRecognized
    // securityTapView 눌렀을때 키패드 닫기
    @objc func tapGestureRecognized(_ gestureRecognizer:UITapGestureRecognizer) {
        keyboardDismiss()
    }
   
}


extension SecurePadInterface : HybridInterface {
    
    func command(viewController: UIViewController, command: Command, result: @escaping (HybridResult) -> ()) {
        
        self.viewController = viewController
        self.command = command
        self.resultCallback = result
        
        xkTextField = XKTextField(frame: CGRect.zero)
        viewController.view.addSubview(xkTextField)
        xkTextField.returnDelegate = self
        
        let argsAarray = command[2] as! [Any]
        
        if let keypadType = argsAarray[0] as? String , keypadType == "number"  {
            xkTextField.xkeypadType = .number // 숫자 키패드
        } else {
            xkTextField.xkeypadType = .qwerty // 문자 키패드
        }
        
        xkTextField.xkeypadViewType = xkKeypadViewType! // 올리고에서는 Full 뷰 사용하지 않고, Normal 뷰 ( 화면 하단에서만 나오는 보안키패드)
        xkTextField.subTitle = "비밀번호"
        xkTextField.e2eURL = e2eURLString

        self.uiControl()
        self.securityTapView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.3)
        self.securityTapView.isUserInteractionEnabled = true
        
        let tapGestureRecognizer =  UITapGestureRecognizer(target: self, action: #selector(self.tapGestureRecognized(_:)))
        self.securityTapView.addGestureRecognizer(tapGestureRecognizer)
        
        delay(0.1) {
            self.xkTextField.becomeFirstResponder()
        }
    }
}


extension SecurePadInterface : XKTextFieldDelegate {
    
    // 보안키패드 입력 완료 했을때 콜백
    func keypadE2EInputCompleted(_ aSessionID: String!, token aToken: String!, indexCount aCount: Int) {
        log?.debug("ABC keypadE2EInputCompleted aSessionID \(aSessionID) aToken \(aToken) aCount \(aCount)")
        
        if !isFirst && aCount == 0{
            isFirst = true
            resultCallback( HybridResult.fail(error: CerealError.businessError(reason: "사용자 취소"), errorMessage: "사용자 취소") )
            keyboardDismiss()
        } else {
            mainKeypadInputCompleted(aCount,finished: true,tuple: (aSessionID, aToken ) )
        }
        
    }

     // 보안키패드 생성 실패했을때  콜백
    func keypadCreateFailed(_ errorCode: Int) {
        log?.debug("keypadCreateFailed: errorCode : \(errorCode) ")
    }
    
     // 보안키패드 사용자 취소
    func keypadCanceled() {
        log?.debug("ABC keypadCanceled ")
        resultCallback( HybridResult.fail(error: CerealError.businessError(reason: "사용자 취소"), errorMessage: "사용자 취소") )
        keyboardDismiss()
    }
    
    /// 입력 길이가 변화 할 때 마다 XKTextFieldDelegate 의 콜백 메소드
    /// - Parameters:
    ///   - length: 입력 length
    func textField(_ textField: XKTextField!, shouldChangeLength length: UInt) -> Bool {
        log?.debug("ABC shouldChangeLength  \(length) ")
        
        if(textField == xkTextField) {
            let argsAarray = command[2] as! [Any]
            let maxLen = argsAarray[1] as! Int
                
            if length == maxLen {
                
                let aSessionID = xkTextField.getSessionIDE2E()
                let aToken = xkTextField.getTokenE2E()
                
                // 입력중이면서, maxlen 길이에 도달할때, complete 콜백
                mainKeypadInputCompleted(Int(length),finished: true,tuple: (aSessionID!, aToken! ) )
                keyboardDismiss()
                
            }
            else {
                // 입력중일때 input  콜백으로 리턴 용도
                mainKeypadInputCompleted(Int(length))
            }
        }
        return true
    }
    
    /// 보안키패드 취소(한단어 삭제) 키 입력 XKTextFieldDelegate 의 콜백 메소드
    /// - Parameters:
    ///   - length: 입력 length
    func textFieldShouldDeleteCharacter(_ textField: XKTextField!) -> Bool {
        log?.debug("ABC textFieldShouldDeleteCharacter length:\(String(describing: textField.text?.count))")
        mainKeypadInputCompleted((textField.text?.count)!)
        return true
    }
    
    /// 보안키패드 세션타임 났을떼 XKTextFieldDelegate 의 콜백 메소드
    /// - Parameters:
    ///             없음.
    func textFieldSessionTimeOut(_ textField: XKTextField!) {
        log?.debug("ABC textFieldSessionTimeOut ")
        resultCallback( HybridResult.fail(error: CerealError.businessError(reason: "sessionTimeOut"), errorMessage: "sessionTimeOut") )
        textField.cancelKeypad();
        keyboardSessionTimeout();
    }
}
