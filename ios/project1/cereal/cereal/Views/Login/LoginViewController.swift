//
//  LoginViewController.swift
//  cereal
//
//  Created by srkang on 2018. 7. 2..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import FirebaseAnalytics
import Firebase


protocol LoginViewControllerDelegate : class {
    func loginViewController(  _ controller : LoginViewController , action : DelegateButtonAction ,   info: Any?    ) -> Void
    
}

extension LoginViewControllerDelegate {
    func loginViewController(  _ controller : LoginViewController , action : DelegateButtonAction ,   info: Any?    ) -> Void {
        log?.debug("LoginViewControllerDelegate Dummy")
    }
}


class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField! // 로그인 ID - 휴대전화번호 TextField
    @IBOutlet weak var passwordTextField: UITextField! // 로그인 PW - 비밀번호 TextField , 실제로 비밀번호를 입력할 수 없고, passwordTextField 를 누르면 xkPasswordTextField 를 becomeFirstResponder 해서, 보안키패드를 띄운다.
    @IBOutlet weak var loginLineView: UIView! // loginTextField 가 활성화 되면, 밑줄 녹색 라인
    @IBOutlet weak var passwordLineView: UIView! // passwordTextField 가 활성화 되면, 밑줄 녹색 라인
    @IBOutlet weak var loginCheckButton: UIButton! // 체크 박스 ( 자동로그인)
    
    @IBOutlet weak var loginButton: UIButton! // 로그인 버튼 (활성화, 비활성화) 처리 위해 IBOutlet 으로 지정
    
    @IBOutlet weak var btnMember: UIButton! // 하단 회원가입 버튼
    @IBOutlet weak var btnID: UIButton! // 하단 아이디 찾기  버튼
    @IBOutlet weak var bntPw: UIButton! // 하단 비밀번호 찾기 버튼
    
    @IBOutlet weak var passwordErrorLabel: UILabel! // 로그인 실패 횟수 알려주는 에러 레이블(빨간색상)
    
    var securityTapView: UIView!// 보안키패드 올라갔을때, 보안키패드 영역 아닌곳 touch 했을때, 보안키패드 내리기 위한 용도
    var keyboardShow = false // 보안 키보드가 올라갔는지 체크
    
    var isAutoLoginCheck = false // 자동로그인 체크
    
    var tapGestureRecognizer : UITapGestureRecognizer! // 탭제스처 Recognizer : 탭 할때, 일반키패드, 보안키패드 내리는 용도
    
    var xkKeypadType : XKeypadType?
    var xkKeypadViewType : XKeypadViewType?
    var xkPasswordTextField: XKTextField!
    
    // 10.05 추가
//    var hybridDirect: Bool = false
    
    var finalData : String?
    
    var info : Any?
    
    // 로그인 아이디 , 비밀번호 시도 최대 횟수
    let MAX_PW_TRY_COUNT = 5
    var pwTryCount = 0 // 현재 로그인 아이디 , 비밀번호 시도  횟수
    
    
    let lineColor = UIColor.init(hexString: "e5e5e5") // loginTextField , passwordTextField 포커스 없는 상태 밑줄 색상 ( 그레이 색상)
    let lineSelectedColor = UIColor.init(hexString: "7d4bd2")  // loginTextField , passwordTextField 포커스 있을때 상태 밑줄 색상 (녹색 색상)
    
    
    var e2eURLString = NetworkInterface.ServerOperation.keyboardE2E.fullURLString() // 보안 키패드 띄웠을때, 보안키패드 sessionId, token 받는 URL
    
    
    var appshield_token : String? // 로그인 시점에
    var appshield_session_id : String?
    
    static let NUMBER_LEN = 6
    static let ID_MAX_LEN = 11
    
    
    weak var delegate : LoginViewControllerDelegate? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 보안키패드 이외 나머지 컨트롤 셋팅
        uiControl()
        uiXKTextField()
        
        setLineColor(focus: 3)
        
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognized(_:)))
        self.view.addGestureRecognizer(tapGestureRecognizer)

        appShieldCheck()
    }
    
    func appShieldCheck() {
        
        
        let xasManager = XASManager.shared
        let result = xasManager.chekApp()
        
        // 성공
        if result == 0 {
            self.appshield_session_id = String(format: "%08X", xasManager.sessionID!)
            self.appshield_token = xasManager.token!
        } else {
            
            // 앱 위변조 실패시 프로그램 종료 
            let alertController = UIAlertController(title: nil, message: xasManager.errorMessage!, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "확인", style: .cancel) { _ in
                exit(0)
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uiXKTextField() {
        
        xkKeypadType = .number
        xkKeypadViewType = .normalView
        
        xkPasswordTextField = XKTextField(frame: CGRect.zero)
        xkPasswordTextField.returnDelegate = self
        xkPasswordTextField.xkeypadType = xkKeypadType!
        xkPasswordTextField.xkeypadViewType = xkKeypadViewType!
//        xkPasswordTextField.xkeypadPreViewType =  XKeypadPreViewType.off
        xkPasswordTextField.e2eURL = e2eURLString
        
//        xkPasswordTextField.startKeypad(withSender: self)
        
        view.addSubview(xkPasswordTextField)
    }
    
    deinit {
        log?.debug("Login deinit")
    }
    
    // 보안키패드 이외 나머지 컨트롤 셋팅
    func uiControl() {
        
        loginTextField.delegate = self
        passwordTextField.delegate = self
        passwordErrorLabel.text = ""
        
        loginButton.isSelected = false
        
        if ApplicationShare.shared.screenSize.smallScreen  {
            btnMember.titleLabel!.font =   UIFont(name: "SpoqaHanSans-Regular", size: 12)
            btnID.titleLabel!.font =   UIFont(name: "SpoqaHanSans-Regular", size: 12)
            bntPw.titleLabel!.font =   UIFont(name: "SpoqaHanSans-Regular", size: 12)
        }
        
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
        // 자동로그인 여부를 키체인에서 가져와서 셋팅
        if custItem.auto_login {
            isAutoLoginCheck = true
            loginCheckButton.isSelected = true
//            loginTextField.text = custItem.user_hp
        }
        
        // 보안키패드 올라갈때 securityTapView 통해서 보안키패드를 닫는 용도
        securityTapView = UIView()
        view.addSubview(securityTapView)
//        securityTapView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.3)
        securityTapView.backgroundColor = .clear
        securityTapView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        securityTapView.isUserInteractionEnabled = false
        
        
        let tapGestureRecognizer =  UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognized(_:)))
        securityTapView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    
    
    // focus : 1 일때, 아이디 포커스 : 2일때 패스워드 포커스 , 3일때 아이디/패스워드 포커스 없음
    func setLineColor(focus: Int ) {
        
        if focus == 1 {
            loginLineView.backgroundColor = lineSelectedColor
            passwordLineView.backgroundColor = lineColor
        } else if focus == 2 {
            loginLineView.backgroundColor = lineColor
            passwordLineView.backgroundColor = lineSelectedColor
        } else if focus == 3 {
            loginLineView.backgroundColor = lineColor
            passwordLineView.backgroundColor = lineColor
        }
        
    }
    
    // FCM 토큰 등록.  로그인 성공 하자 마자 FCM 토큰 등록한다.
    func fcmTokenRegister() {
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
        
        if let fcm_token = custItem.fcm_token {
            let parameters  = ["user_no" : custItem.user_no! , "push_token" : fcm_token]
            
            NetworkInterface.transferServer(viewController: self, operation: .pushToken, parameters: parameters ) { [weak self]  (error, response) in
                
                if error == nil {
                    if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                        if let data = response["data"] as? [String:Any]   {
                            log?.debug(data)
                        }
                    }
                }
            }
        }
        
        
    }


  
    // MARK:// IBAction
    // 닫기 버튼
    @IBAction func btnCloseAction(_ sender: Any) {
        
        self.delegate?.loginViewController(self, action: .close, info: nil)
    }
    
    // 로그인 버튼
    // 로그인 프로세스를 탄다.
    @IBAction func btnLoginAction(_ sender: Any) {
        guard loginButton.isSelected else  {return}
        
        
        var parameters  : [String:Any] = [:]
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
       

        if  let loginString = loginTextField.text ,
            let passwordString = passwordTextField.text ,
            loginString.count > 0 , passwordString.count > 0 {
            
            let mData = xkPasswordTextField.getDataE2E()
            var aSessionID = xkPasswordTextField.getSessionIDE2E()
            let aToken = xkPasswordTextField.getTokenE2E()
            
            if aSessionID!.count < 8 {
                let appendZero = "0"
                let count = aSessionID!.count
                let paddingCount = 8 - count
                let i = 0
                for  _ in i ..< paddingCount {
                    aSessionID = appendZero + aSessionID!
                }
                
            }
            
            
            log?.debug("mData:\(mData!)")
            log?.debug("finalData:\(finalData!)")
            log?.debug("aSessionID:\(aSessionID!)")
            
            parameters = ["login_type" : "P"  ,
                          "user_hp" :  loginTextField.text!,
                          "xksessionid" : aSessionID! ,
                          "xksectoken" : aToken! ,
				          "xkindexed" : finalData!,
                          "appshield_session_id" : appshield_session_id! ,
                          "appshield_token" : appshield_token!]
			
            parameters["auto_login"] = "N"
            
            if isAutoLoginCheck {
                parameters["auto_login"] = "Y"
            } else {
                parameters["auto_login"] = "N"
            }
              if let token = custItem.token {
                parameters["token"] = token
            }
            if let  user_no = custItem.user_no {
                parameters["user_no"] = user_no
            }
            
        } else {
            return
        }

       
         /*
            파라미터 전송 샘플 
             auto_login=N&
             login_type=P&
             token=LO-d441b4664df74386be328d592b514831&
             user_hp=01010002000&
             user_no=TUA00002&
             xkindexed=14%2C12%2C18%2C11%2C17%2C16&
             xksectoken=BbPzVxx09T4FbqcDTx54DIKKe8N8S6ceui24I1wMa3E%3D&
             xksessionid=B514834
         */
        
        let progressHUD = MBProgressHUD.showCerealCHUDAdded(to: self.view, animated: true)
        
        
        NetworkInterface.transferServer(viewController: self, operation: .login, parameters: parameters ) { [weak self]  (error, response) in
            
            guard let strongSelf = self else {
                return
            }
            
            progressHUD.hide(animated: true)
            
            
            
            if error == nil {
                
                /*
                 {
                 "msg" : "로그인 핀번호 복호화 오류",
                 "data" : {
                 "aResultCode" : "50006",
                 "aResultMessage" : "[DB fail]-[Does not exist, can not be select, deleted or updated to] "
                 },
                 "code" : "1001"
                 }
                */
                
                // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                    if let data = response["data"] as? [String:Any]   {
                       
                        strongSelf.pushMarketingProcess(data: data)
                        //strongSelf.loginProcess(data: data)
                        
                    }
                } else if let response =  response,  let msg = response["msg"] as? String {
                  // code 가 Success 가 아닌 경우
                    
                    strongSelf.passwordTextField.text = ""
                    strongSelf.loginButtonEnableCheck()
                    strongSelf.setLineColor(focus: 3)
                    
                    
                    if let code = response["code"] as? String  {
                        
                        
                        // 90일 동안 비밀번호 변경 안한 경우
                        if code == "1006" {
                                
                            var info : Any?
//
                            let url = MenuInfo.menuURL(from: .logChangePw90)!
                            info = ["webURL" : url]
                            
                            
                            
                            if let  delegate = strongSelf.delegate {
                                log?.debug("\(info)")
                                 delegate.loginViewController(strongSelf, action: .change90Pw, info: info)
                                
                                
                                //FIXME:// 로그인처리
                                if let data = response["data"] as? [String:Any]   {
                                    strongSelf.loginProcess(data: data,pwChange: true)
                                }
                                
                            }
                                
                          
                            
                        } else if code == "1004" {
                            // 유효한 토큰이 아닙니다.
                            let alertController = UIAlertController(title: nil, message: "고객님의 안전한 정보보호를 위해 앱 재설치 시, 휴대폰 본인인증이 필요합니다.", preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "확인", style: .cancel) { handler in
                                
                                /*
                                {
                                    "msg" : "유효한 토큰이 아닙니다.",
                                    "data" : {
                                        
                                    },
                                    "code" : "1004"
                                }
                                */
                                
                                 strongSelf.delegate?.loginViewController(strongSelf, action: .invalidToken, info: nil)
                                
                               
                                
                            }
                            alertController.addAction(alertAction)
                            strongSelf.present(alertController, animated: true, completion: nil)
                            
                        } else if code == "1005" || code == "1002" {
                            // 패스워드 불일치 (1005) , 아이디 존재 안함 (1002)
                            strongSelf.pwTryCount += 1
                            
                            if  strongSelf.pwTryCount == strongSelf.MAX_PW_TRY_COUNT {
                                strongSelf.delegate?.loginViewController(strongSelf, action: .findPw, info: nil)
                            } else {
                                self?.passwordErrorLabel.text = "ID, 비밀번호가 일치하지 않습니다. (\(strongSelf.pwTryCount)/\(strongSelf.MAX_PW_TRY_COUNT))"
                            }
                            
                        } else {
                            let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                            alertController.addAction(alertAction)
                            strongSelf.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                    else {
                        let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                        alertController.addAction(alertAction)
                        strongSelf.present(alertController, animated: true, completion: nil)
                    }
                    
                } else {
                      // response 없는 경우
                }
            } else {
                //  AlamoFire 오류
               
            }
        }
        
    }
    
    func loginButtonEnableCheck() {
        
        var loginEnabled = false
        
        if let loginText = loginTextField.text , loginText.count > 10 ,
            let passwordText = passwordTextField.text , passwordText.count == LoginViewController.NUMBER_LEN  {
            loginEnabled = true
        }
        
        if loginEnabled {
            loginButton.isSelected = true
        } else {
            loginButton.isSelected = false
        }
    }
    
    func pushMarketingProcess(data : [String:Any] , pwChange : Bool = false) {
        let user_no = data["user_no"]
        let push_yn: String? = data["push_yn"] as? String

        if (user_no == nil) {
            self.loginProcess(data: data, pwChange: pwChange)
            return
        }
        
        if MenuInfo.pushMarketingJSON == nil {
            self.loginProcess(data: data, pwChange: pwChange)
            return
        }
        
        if push_yn == nil || push_yn == "N" {
            self.loginProcess(data: data, pwChange: pwChange)
            return
        }
        
        let popupViewController = UIStoryboard.storyboard(.main).instantiateViewController() as SystemPopupViewController
        popupViewController.modalPresentationStyle = .overFullScreen
        let innerSystemPopupVC = InnerSystemPopupViewController2()
        innerSystemPopupVC.parentVC = self
        innerSystemPopupVC.data = data
        innerSystemPopupVC.pwChange = pwChange
        popupViewController.delegate = innerSystemPopupVC;
        popupViewController.isPushMargetingPopup = true
		popupViewController.user_no = data["user_no"] as? String
        present(popupViewController, animated: true, completion: nil)
    }
    
    class InnerSystemPopupViewController2: SystemPopupViewControllerDelegate {
        var parentVC: LoginViewController!
        var data : [String:Any]!
        var pwChange : Bool!
        func systemPopupViewControllerDidFinished(_ viewController : SystemPopupViewController , action : SystemPopupAction) {
            viewController.dismiss(animated: true, completion: nil)
            parentVC.loginProcess(data: data, pwChange: pwChange)
        }
    }

    func loginProcess(data : [String:Any] , pwChange : Bool = false) {
        
        let last_login_time = data["Last_login_time"]
        let token           = data["token"]
        let user_no         = data["user_no"]
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as? KeyChainCustItem  {
            log?.debug("custItem:\(custItem)")
            
            if let last_login_time = last_login_time {
                custItem.last_login_time = last_login_time as? String
            }
            
            if let token = token {
                custItem.token = token as? String
            }
            
            if let user_no = user_no {
                custItem.user_no = user_no as? String
            }
            //
            if self.isAutoLoginCheck {
                custItem.auto_login = true
            } else {
                custItem.auto_login = false
            }
            
            custItem.user_hp = self.loginTextField.text!
            
            keyChainWrapper.set(custItem, forKey: Configuration.keyChainKey)
            
        }
        
        let loginInfo = ApplicationShare.shared.loginInfo
        loginInfo.isLogin = true
        
        if let user_level = data["user_level"] as? String {
            loginInfo.user_level = user_level
        } else if let user_level = data["user_level"] as? Int {
            loginInfo.user_level = String(user_level)
        }
        
        if let push_count = data["push_count"] as? String {
            loginInfo.push_count = push_count
        } else if let push_count = data["push_count"] as? Int {
            loginInfo.push_count = String(push_count)
        }
        
        if let simul_nm = data["simul_nm"] as? String {
            loginInfo.simul_nm = simul_nm
        }
        if let nickname = data["nickname"] as? String {
            loginInfo.nickname = nickname
        }
        if let asset_info = data["asset_info"] as? [String:Any]  ,
            let balance      =  asset_info["balance"] as? Int ,
            let asset_amt    =  asset_info["asset_amt"] as? Int {
            loginInfo.balance   = String(balance)
            loginInfo.asset_amt = String(asset_amt)
            
        }
        if let user_img_url = data["user_img_url"] as? String  {
            loginInfo.user_img_url = user_img_url
        }
        
        
        // badge_list 의 item 의 badge_cd 로 sort 한다.
        if let badge_list = data["badge_list"] as? [Dictionary<String,Any>] {
            var tempBadge_list : [Dictionary<String,Any>] = []
            for badge_item in badge_list {
                tempBadge_list.append(badge_item)
                //                                loginInfo.badge_list.append(badge_item)
            }
            
            tempBadge_list.sort() { (item1,item2) in
                if Int(item1["badge_cd"] as! String) < Int(item2["badge_cd"] as! String) {
                    return true
                } else {
                    return false
                }
            }
            
            log?.debug(tempBadge_list)
            loginInfo.badge_list = tempBadge_list
        }
        
        
        // GA 로그인 이벤트
        Analytics.logEvent("login",parameters:["sign_up_method" : "P"])
        
        // GA 이벤트 샘플
        Analytics.logEvent("ecommerce_purchase",parameters:["firebase_screen_id" : "3"])
        
        Analytics.logEvent("screen_view",parameters:["firebase_event_origin" : "4"])
        // FCM 토큰 등록
        delay(0.01) {
            self.fcmTokenRegister()
        }
        
        delay(0.2) {
            // 비밀 번호 변경 아닐 때 만 정상적인 로그인
            if !pwChange {
                if let  delegate = self.delegate {
                    delegate.loginViewController(self, action: .login, info: self.info)
                }
            } else {
                /*  delegate 에게 change90Pw 넘겨 줬음 */
                
                // 90일 연장  정상적인 로그인 처리 ( 메뉴화면 )
                Notification.Name.ActionLogin.post()
 
            }
        }
      
    }
    
    // 회원가입 이동
    @IBAction func btnMemberJoinAction(_ sender: Any) {
        self.delegate?.loginViewController(self, action: .memberJoin, info: nil)
    }
    
    
    // ID 변경 이동
    @IBAction func btnChangeIDAction(_ sender: Any) {
        self.delegate?.loginViewController(self, action: .changeID, info: nil)
    }
    
    
    // 비밀번호 찾기 이동
    @IBAction func btnChangePwAction(_ sender: Any) {
        self.delegate?.loginViewController(self, action: .findPw, info: nil)
    }
    
   
    
    @IBAction func loginCheckToggleAction(_ sender: Any) {
        
        isAutoLoginCheck = !isAutoLoginCheck
        
        self.loginCheckButton.isSelected = isAutoLoginCheck
        
    }
    
    
    //MARK:// GestureRecognized
    @objc func tapGestureRecognized(_ gestureRecognizer:UITapGestureRecognizer) {
        keyboardDismiss()
    }
    
    func keyboardDismiss() {
        keyboardShow = false
        loginTextField.resignFirstResponder()
        xkPasswordTextField.resignFirstResponder()
        securityTapView.isUserInteractionEnabled = false
//        securityTapView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.3)
        securityTapView.backgroundColor = .clear
    }
    
    func keyboardSessionTimeout() {
        let alert = UIAlertController(title: "", message: "보안 세션이 만료되었습니다.\n다시 실행해 주세요.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "확인", style: .default, handler: nil));
        self.present(alert, animated: true, completion: nil);
    }
    
    func whenKeyboardShow() {
        keyboardShow = true
        securityTapView.isUserInteractionEnabled = true
        securityTapView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.3)
//        securityTapView.backgroundColor = .clear
    }
    
    
    func mainKeypadInputCompleted(_ aCount: Int,  finished : Bool = false, tuple : (String,String)? = nil ) {
      
        
        callbackInputCompleted(aCount,finished: finished,tuple : tuple)
        
        if finished {
            finalData = xkPasswordTextField.getDataE2E()
        }
        
    }
    
    
    func callbackInputCompleted(_ aCount: Int , finished : Bool = false , tuple : (String,String)?  = nil) {
        
        let dummyString = makeDummyString(aCount)
        self.passwordTextField.text = dummyString
       
    }
    
    
    func makeDummyString(_ aCount:Int) -> String {
        var mutableString = ""
        for _ in   0 ..< aCount {
            mutableString += "*"
        }
        return mutableString
    }
    
    

}


// MARK:// UITextFieldDelegate
extension LoginViewController : UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        log?.debug("textFieldShouldBeginEditing called")
        
        
        if textField == passwordTextField {
            
            guard !keyboardShow else {return false}
            
            securityTapView.backgroundColor = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.3)
            setLineColor(focus: 2)
            passwordTextField.text = ""
            loginTextField.resignFirstResponder()
            
            delay(0.01) {
                self.whenKeyboardShow()
                self.xkPasswordTextField.becomeFirstResponder()
                self.setLineColor(focus: 2)
            }
           
            
            return false
        } else if textField == loginTextField{
            
            setLineColor(focus: 1)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        log?.debug("textFieldDidEndEditing called")
        setLineColor(focus: 3)
        loginButtonEnableCheck()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginTextField{
            
            if string == "" {
                  return true
            } else if let loginText = textField.text , loginText.count >=  LoginViewController.ID_MAX_LEN {
                return false
            }
        }
        
        return true
    }
    
   
    
}

// MARK:// XKTextFieldDelegate
extension LoginViewController : XKTextFieldDelegate {

    
    func keypadInputCompleted(_ aCount: Int) {
     
        mainKeypadInputCompleted(aCount,finished: true)
    }
    
    func keypadInputCompleted(_ textField: XKTextField!, count: Int) {
        log?.debug("ABC keypadInputCompleted textField \(textField) count \(count)")
    }
    
    func keypadE2EInputCompleted(_ aSessionID: String!, token aToken: String!, indexCount aCount: Int) {
        log?.debug("ABC keypadE2EInputCompleted aSessionID \(aSessionID) aToken \(aToken) aCount \(aCount)")
        
        setLineColor(focus: 3)
        
        mainKeypadInputCompleted(aCount,finished: true,tuple: (aSessionID, aToken ) )
        xkPasswordTextField.cancelKeypad()
        keyboardDismiss()
        loginButtonEnableCheck()
        
    }
    
    func keypadE2EInputCompleted(_ textField: XKTextField!, sessionID: String!, token: String!, indexCount count: Int) {
        log?.debug("ABC keypadE2EInputCompleted sessionID \(sessionID) token \(token) count \(count)")
    }
    
    func keypadCreateFailed(_ errorCode: Int) {
        log?.debug("keypadCreateFailed: errorCode : \(errorCode) ")
    }
    
    func keypadCanceled() {
        log?.debug("ABC keypadCanceled ")
        self.keyboardDismiss();
    }
    
    func textField(_ textField: XKTextField!, shouldChangeLength length: UInt) -> Bool {
        log?.debug("ABC shouldChangeLength  \(length) ")
        
        if(textField == xkPasswordTextField) {
            
            // 최대 자리수 6자리
            if length == LoginViewController.NUMBER_LEN {
                let aSessionID = xkPasswordTextField.getSessionIDE2E()
                let aToken = xkPasswordTextField.getTokenE2E()
                
                setLineColor(focus: 3)
                
                mainKeypadInputCompleted(Int(length),finished: true,tuple: (aSessionID, aToken ) as? (String, String) )
                xkPasswordTextField.cancelKeypad()
                keyboardDismiss()
                loginButtonEnableCheck()
                
            }
                
//            } else if length > 0 && length < 6   {
//                 setLineColor(focus: 2)
//            }
            else {
                
                mainKeypadInputCompleted(Int(length))
            }
            
            
        }
        return true
    }
    
    func textFieldShouldDeleteCharacter(_ textField: XKTextField!) -> Bool {
        log?.debug("ABC textFieldShouldDeleteCharacter length:\(String(describing: textField.text?.count))")
        mainKeypadInputCompleted((textField.text?.count)!)
        return true
    }
    
    func textFieldSessionTimeOut(_ textField: XKTextField!) {
        log?.debug("ABC textFieldSessionTimeOut ")
        textField.cancelKeypad();
        self.keyboardSessionTimeout();
    }
    
}




