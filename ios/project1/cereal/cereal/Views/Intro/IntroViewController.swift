//
//  IntroViewController.swift
//  cereal
//
//  Created by srkang on 2018. 7. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SwiftyJSON
import SnapKit
import Alamofire
import AlamofireImage

class IntroViewController: UIViewController {
    
    @IBOutlet weak var introImageView: UIImageView!
    @IBOutlet weak var skipButton: UIButton!
   
    
    var firstChain : Chain?
    var currentChain : Chain?
    
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    
    deinit {
        log?.debug("IntroViewController deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        imageViewAnimation()

        
        

        
//        for index in 1...59 {
//            let scale = UIScreen.main.scale
//
//            let scaleSuffix : String = scale == 2.0 ?  "@2x.png" :  "@3x.png"
//
//            let imageName = "intro\(index)\(scaleSuffix)"
//
////            let url = Bundle.main.url(forResource: imageName, withExtension: nil)
////
////            let data = try? Data(contentsOf: url!)
////
////
////            imageArray.append(UIImage(data: data!, scale: scale)!)
//            let path = Bundle.main.path(forResource: imageName, ofType: nil)
//
//            imageArray.append(UIImage(contentsOfFile: path! )!)
//        }
//
        
//        for index in 1...59 {
//            let imageName = "intro\(index)"
//            imageArray.append(UIImage(named: imageName)!)
//        }
//
        
        registerChain()

        
        delay(2) {
            self.processChain()
        }
        
        

        
    }
    
    func imageViewAnimation() {
        var imageArray = [UIImage]()

//OJKIM MODIFIED START - 인트로수정
//        for index in 1...59 {
        for index in 1...29 {
//OJKIM MODIFIED END
            let imageName = "intro\(index)"
            imageArray.append(UIImage(named: imageName)!)
        }
        
        introImageView.animationImages = imageArray
        introImageView.animationDuration = 2
        introImageView.startAnimating()
//OJKIM ADDED START - 인트로수정
		self.perform(#selector(afterAnimation),
					 with: nil,
					 afterDelay: introImageView.animationDuration)
//OJKIM ADDED END
	}
	
//OJKIM ADDED START - 인트로수정
	@objc func afterAnimation() {
		introImageView.stopAnimating()
		introImageView.image = UIImage(named: "intro29")!
	}
//OJKIM ADDED END

    func gifAnimation() {
        introImageView.isHidden = true
        let url = Bundle.main.url(forResource: "intro2.gif", withExtension: nil)
        
        let data = try? Data(contentsOf: url!)
        let image =  FLAnimatedImage.init(animatedGIFData: data!)
        
        let flImageView = FLAnimatedImageView()
        
        view.addSubview(flImageView)
        
        flImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(introImageView)
        }
        
        flImageView.animatedImage = image
    }
    
    func registerChain() {
        
//        let pushAuthorizationChain      = PushAuthorizationChain(context: self) // 푸시 권한 체크
        let permissionInitialCheckChain = PermissionInitialCheckChain(context: self) // 접근권한 알림
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
		let marketingConfirmChain       = MarketingConfirmChain(context: self) //마케팅정보 활용 확인
//OJKIM ADDED END
        let pushCheckChain              = PushCheckChain(context: self) // 푸시 요청 Alert
        let pushAuthorizationChainAfter = PushAuthorizationChain(context: self) // 푸시 권한 체크
        
        let appGuideChain               = AppGuideChain(context: self) // 앱 가이드
        
        let appshieldCheckChain         = AppShieldCheckChain(context: self) // 앱위변조 체크
        let autoLoginChain              = AutoLoginChain(context: self) // 자동 로그인
        let fcmTokenChain               = FcmTokenChain(context: self) // FCM 등록
        
        
//        pushAuthorizationChain.setNextChain(permissionInitialCheckChain)
//OJKIM ROLLBACKED START - 마케팅동의창 띄우기 - 일단 실행부분 원래대로 원복
        permissionInitialCheckChain.setNextChain(pushCheckChain)
        pushCheckChain.setNextChain(pushAuthorizationChainAfter)
////OJKIM MODIFIED START - 마케팅동의창 띄우기 - 확정안됨
////        permissionInitialCheckChain.setNextChain(pushCheckChain)
////        pushCheckChain.setNextChain(pushAuthorizationChainAfter)
//		permissionInitialCheckChain.setNextChain(marketingConfirmChain)
//		marketingConfirmChain.setNextChain(pushCheckChain)
//		pushCheckChain.setNextChain(pushAuthorizationChainAfter)
////OJKIM MODIFIED END
//OJKIM ROLLBACKED END
        pushAuthorizationChainAfter.setNextChain(appGuideChain)
        
        appGuideChain.setNextChain(appshieldCheckChain)
        appshieldCheckChain.setNextChain(autoLoginChain)
        autoLoginChain.setNextChain(fcmTokenChain)
        
        //firstChain = pushAuthorizationChain
        firstChain = permissionInitialCheckChain
        
    }
    
    func processChain() {
        
        firstChain?.doJob()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var topLength       : CGFloat!
        var bottomLength    : CGFloat!
        
        if #available(iOS 11.0, *) {
//            let window = UIApplication.shared.keyWindow
            topLength = view?.safeAreaInsets.top
            bottomLength = view?.safeAreaInsets.bottom

        } else {
            topLength = self.topLayoutGuide.length
            bottomLength = self.bottomLayoutGuide.length
           
        }
        
        ApplicationShare.shared.deviceInfo.topLength = topLength
        ApplicationShare.shared.deviceInfo.bottomLength = bottomLength
        
        
        let duration = Date().timeIntervalSince1970 - ApplicationShare.timeInterval!
        
        // 0.188745975494385
        log?.debug("viewDidLayoutSubviews duration : \(duration)")
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func moveMainViewController() {
        
        let appDelegate =   UIApplication.shared.delegate as! AppDelegate
        let slidingViewController = UIStoryboard.storyboard(.main).instantiateViewController() as OKSlidingViewController
        appDelegate.window!.rootViewController = slidingViewController
        
    }
    
    func permissionCheckFinished() -> Bool {
        if defaults.bool(forKey: CerealConstrants.NSUserDefaultsKeys.permissionInitialCheck) {
            return true
        } else {
            return false
        }
    }
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
	func marketingConfirmFinished() -> Bool {
		if defaults.bool(forKey: CerealConstrants.NSUserDefaultsKeys.marketingConfirm) {
			return true
		} else {
			return false
		}
	}
//OJKIM ADDED END
    
    func appGuideCheckFinished() -> Bool {
        if defaults.bool(forKey: CerealConstrants.NSUserDefaultsKeys.guideShownFinished) {
            return true
        } else {
            return false
        }
    }
    
    let group = DispatchGroup()
    
    // 이벤트 관련 이미지 다운로드
    func processEventDownloadImage() {
        
        if let eventJSON = MenuInfo.filterEventJSON  {
            
            let arrayValue = eventJSON.arrayValue
            
            var eventIdUrlMapping = [Int:(String,Bool) ]()
            
            for json in arrayValue {
                if json["popup_kn"].intValue == 2 || json["popup_kn"].stringValue == "2" {
                    let event_id = json["event_id"].intValue
                    let img_url = json["img_url"].stringValue
                    eventIdUrlMapping[event_id] = (img_url,false)
                }
            }

            
            if eventIdUrlMapping.isEmpty {
                delay(0.1) {
                    self.moveMainViewController()
                }
            } else {
                for (key,value) in eventIdUrlMapping {
                    processDownload(group:group , eventId: key, img_url: value.0 )
                }
                
                group.notify(queue: .main) { [unowned self] in
                     self.moveMainViewController()
                    log?.debug("done")
                }
                
            }
            
        }
        
    }
    
    func processDownload(group : DispatchGroup , eventId:Int , img_url : String ) {
        
        group.enter()
        
        let imageURL = NetworkInterface.buildURL(serverDomainUse: false, appendString: img_url)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let fileManager = FileManager.default
            var documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("temp")
            
            var isDir : ObjCBool = false
            if fileManager.fileExists(atPath: documentsURL.path, isDirectory: &isDir) {
                if isDir.boolValue {
                    
                } else {
                    
                }
            } else {
                // file does not exist
                
                do {
                    try fileManager.createDirectory(at: documentsURL, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    
                }
            }
            
            documentsURL.appendPathComponent("\(eventId).png")
            
            return (documentsURL, [.removePreviousFile])
        }
        
        Alamofire.download(imageURL, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
            log?.debug("Progress: \(progress.fractionCompleted)")
            } .validate().responseData { ( response ) in
                
               group.leave()
                
        }
    }
}
    


extension IntroViewController : ChainContext {
    
    func finishChain() {
        
        var dictionary : [String:String]!
        if let userDicionary =  defaults.object(forKey: CerealConstrants.NSUserDefaultsKeys.eventReadFinished) as? [String:String] {
            dictionary = userDicionary
        } else {
            dictionary = [String:String]()
        }
        //OJKIM ADDED START - 푸시알림동의 내역 초기화
        MenuInfo.pushMarketingJSON = nil
        //OJKIM ADDED END
//		//OJKIM TEST START - 마케팅동의창 띄우가 intro통신으로 받아서 하는 것 테스트
//var aaa = "{\"eventInfo\":[{\"event_id\":\"1\",\"popup_kn\":\"3\",\"title\":\"ojkimtest\",\"note\":\"ojkimnotetest\",\"link_url\":\"http://www.daum.net\"}]}"
//MenuInfo.eventJSON = JSON(
//	aaa.data(using: String.Encoding.utf8)
//)
//ApplicationShare.shared.loginInfo.isLogin = true
//ApplicationShare.shared.loginInfo.push_yn = "Y"
//		//OJKIM TEST END

        if let eventJSON = MenuInfo.eventJSON ,
            !eventJSON["eventInfo"].arrayValue.isEmpty {
            
            var filterAppendJSON = JSON([JSON]())
            
            for childJSON in  eventJSON["eventInfo"].arrayValue {
                if childJSON["popup_kn"].intValue == 3 ||
                    childJSON["popup_kn"].stringValue == "3" ||
                    childJSON["popup_kn"].intValue == 4 ||
                    childJSON["popup_kn"].stringValue == "4" {
                    if ApplicationShare.shared.loginInfo.isLogin == false {
                        MenuInfo.pushMarketingJSON = childJSON
                        continue
                    } else {
                       if ApplicationShare.shared.loginInfo.push_yn == nil ||
                            ApplicationShare.shared.loginInfo.push_yn != "Y" {
                            continue
                        }
                    }
                }
                
                let event_id = childJSON["event_id"].intValue
                
                if dictionary[String(event_id)] == nil {
                    filterAppendJSON.appendIfArray(json:childJSON)
                } else if let eventDate = dictionary[String(event_id)]  {
                    
                    let compareResult = Date.todayCompare(compare: eventDate)
                    
                    if compareResult == .orderedDescending {
                        filterAppendJSON.appendIfArray(json:childJSON)
                    }
                }
                
            }
            
            MenuInfo.filterEventJSON = filterAppendJSON
            
            defaults.set(dictionary,forKey: CerealConstrants.NSUserDefaultsKeys.eventReadFinished)
            defaults.synchronize()
            
            processEventDownloadImage()
            
            
        }
        else {
            delay(0.1) {
                self.moveMainViewController()
            }
        }
        
    }
    
    func byPassAutoLogin() -> Bool {
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        // 키체인에 전화번호가 있고, 자동로그인 설정 된경우, return false 로 하여, ByPass 못하다록 한다. (즉, 자동로그인 Chain 타도록)
        if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as? KeyChainCustItem ,
            let _ =  custItem.user_hp {
            
            if  custItem.auto_login {
                return false
            }
        }
        
        return true
        
    }
    
    
    func isByPass(_ jobType: ChainJobType) -> Bool {
        switch jobType {
        case .pushAuthorization:
            return false
        case .permissionInitialCheck:
            return permissionCheckFinished()
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
		case .marketingConfirm:
			return marketingConfirmFinished()
//OJKIM ADDED END
        case .pushCheck:
            return false
        case .appGuide:
            return appGuideCheckFinished()
        case .appShiled:
//             FIXME://  디폴트 false 임 , iphone5 시뮬레이터에서만  true
            return false
        case .autoLogin:
            return byPassAutoLogin()
        case .fcmRegister:
            return byPassAutoLogin()
        default:
            return true
        }
    }
    
    func notifyBegin(_ jobType: ChainJobType) {
        switch jobType {
        case .appShiled:
            ()
        default:
            ()
        }
    }
    
    func notifySuccess(_ jobType: ChainJobType) {
        switch jobType {
        case .appShiled:
            ()
       
        default:
            ()
        }
    }
    
    func notifyError(_ jobType: ChainJobType) {
        
        switch jobType {
        default:
            ()
        }
    }
    
    func contextAction(_ jobType: ChainJobType, chain : Chain? = nil) {
        
        if let chain = chain {
            currentChain = chain
        }
        switch jobType {
        case .appGuide:
            appGuide()
        case .permissionInitialCheck:
            alertPermission()
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
		case .marketingConfirm:
			alertMarketing();
//OJKIM ADDED END
        case .pushAuthorization:
            alertResult()
        default:
            ()
        }
    }
    
    // 앱 권한 고지 내용 팝업
    func alertPermission() {
        let alertController = UIStoryboard.storyboard(.main).instantiateViewController() as PermissionAlertController
        alertController.delegate = self
        alertController.modalPresentationStyle = .overFullScreen
        self.present(alertController, animated: true, completion: nil)
    }
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
	func alertMarketing() {
		let alertController = UIStoryboard.storyboard(.main).instantiateViewController() as MarketingAlertController
		alertController.delegate = self
		alertController.modalPresentationStyle = .overFullScreen
		self.present(alertController, animated: true, completion: nil)
	}
//OJKIM ADDED END
    
    // 앱 푸시 필수 권한 설정 거부시 
    func alertResult() {
        let alertController = UIStoryboard.storyboard(.main).instantiateViewController() as PermissionRejectViewController
        alertController.delegate = self
        alertController.modalPresentationStyle = .overFullScreen
        self.present(alertController, animated: true, completion: nil)
    }
    
    func appGuide() {
        let storyboard = UIStoryboard.storyboard(.intro)
        let  guideViewController = storyboard.instantiateViewController() as GuideViewController
        guideViewController.delegate = self
        self.navigationController?.pushViewController(guideViewController, animated: true)
    }
    
    
}


extension IntroViewController : PermissionAlertControllerDelegate {
    
    func permissionAlertControllerClosed(_ alertController: PermissionAlertController) {
        dismiss(animated: true, completion: nil)
        
        defaults.set(true, forKey: CerealConstrants.NSUserDefaultsKeys.permissionInitialCheck)
        defaults.synchronize()
        
        if let currentChain = currentChain {
            currentChain.contextResult(success : true)
        }
    }
}

//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
extension IntroViewController : MarketingAlertControllerDelegate {
	func marketingAlertControllerCanceled(_ alertController: MarketingAlertController) {
		dismiss(animated: true, completion: nil)
		defaults.set(true, forKey: CerealConstrants.NSUserDefaultsKeys.marketingConfirm)
		defaults.synchronize()
		//FIXME 저장값을 서버로 올려야 한다.
		if let currentChain = currentChain {
			currentChain.contextResult(success : false)
		}
	}
	
	func marketingAlertControllerConfirmed(_ alertController: MarketingAlertController) {
		dismiss(animated: true, completion: nil)
		defaults.set(true, forKey: CerealConstrants.NSUserDefaultsKeys.marketingConfirm)
		defaults.synchronize()
		//FIXME 저장값을 서버로 올려야 한다.
		if let currentChain = currentChain {
			currentChain.contextResult(success : true)
		}
	}
}
//OJKIM ADDED END
extension IntroViewController : PermissionRejectViewControllerDelegate {
    
    func permissionRejectControllerClosed(_ alertController: PermissionRejectViewController) {
        dismiss(animated: true, completion: nil)
        
        exit(0)
    }
}


extension IntroViewController : GuideViewControllerDelegate {
    func guideViewControllerFinished(_ viewController: GuideViewController, action: GuideAction) {
        self.navigationController?.popViewController(animated: true)
        
        defaults.set(true, forKey: CerealConstrants.NSUserDefaultsKeys.guideShownFinished)
        defaults.synchronize()
        
        if action == .finish {
            log?.debug("finish")
            ApplicationShare.isCerealMemberJoin =   true
        }
        
        if let currentChain = currentChain {
            currentChain.contextResult(success : true)
        }
    }
}
