//
//  CerialConstants.swift
//  cereal
//
//  Created by srkang on 2018. 7. 4..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation
import UIKit

enum CerealConstrants {
    struct FCM {
        static let gcmMessageIDKey = "gcm.message_id"
    }
    
    struct NSUserDefaultsKeys {
        
        static let requestedAfterNotifications      = "requestedAfterNotifications" //  Push 권한을 요청 한 적이 있는지 (iOS10 이상, iOS9 이하 모두 포함)
        
        static let requestedNotifications           = "PS_requestedNotifications" // iOS9 이하 , Push 권한을 요청 한 적이 있는지
        
        
        static let permissionInitialCheck        = "permissionInitialCheck" // 앱 최초 권한(필수,선택) 고지 사항을 읽었는지
        static let guideShownFinished            = "guideShownFinished" //  앱 소개 읽었는지
        
        static let eventReadFinished            = "eventReadFinished" //  Event 목록 읽었는지
        static let keyChainInstalled            = "keyChainInstalled" //  Event 목록 읽었는지
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
		static let marketingConfirm             = "marketingConfrim"
//OJKIM ADDED END
    }
    
    struct NetworkServerResponse {
        static let success             = "0000" // 네트워크 응답 정상 코드
    }
    
    
}
