//
//  XASManager.swift
//  cereal
//
//  Created by srkang on 2018. 8. 10..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class XASManager {
    static let shared = XASManager()
    
    lazy var xasInterface = XASInterface()
    
    var sessionID : Int?
    var token : String?
    var errorMessage : String?
    
    private  init() {}
    
    func chekApp() -> Int {
        
        var result = 0
        
        /**************************************************************************/
        
        /*------------------------------------------------------------------------
         * 난독화 코드 실행.
         *------------------------------------------------------------------------*/
        obfuscate();
        
        /*------------------------------------------------------------------------
         *  안티 디버깅 체크 실행. 실행시 디버깅 상태면 무조건 앱을 종료시킴.
         *------------------------------------------------------------------------*/
        
        //xasInterface.checkAntiDebug()
        
        /*------------------------------------------------------------------------
         * 앱무결성 검증 실행.
         *------------------------------------------------------------------------*/

        result = xasInterface.checkApp(NetworkInterface.APPVERIFY_CHECKAPPURL, appInfo: NetworkInterface.APPVERIFY_APPINFO)
        
        
        if result == 0 {
            sessionID =  xasInterface.sid
            token =     xasInterface.token
        } else {
            errorMessage = xasInterface.errorMessage(result)
        }

        
        return result;
    }
    
    
}
