//
//  MarketingAlertController.swift
//  cereal
//
//  Created by The Net on 14/06/2019.
//  Copyright © 2019 srkang. All rights reserved.
//

import UIKit

protocol MarketingAlertControllerDelegate : class{
	func marketingAlertControllerConfirmed(_ alertController: MarketingAlertController) -> Void
	func marketingAlertControllerCanceled(_ alertController: MarketingAlertController) -> Void
}

class MarketingAlertController: UIViewController {
	@IBOutlet weak var confirmButton: UIButton!
	@IBOutlet weak var cancelButton: UIButton!
	
	weak var delegate : MarketingAlertControllerDelegate? = nil
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		confirmButton.setBackgroundImage(UIColor(hexString: "8bcb2f").createImageFromUIColor(), for: UIControlState())
		confirmButton.setBackgroundImage(UIColor(hexString: "7d4bd2").createImageFromUIColor(), for: .highlighted)
		cancelButton.setBackgroundImage(UIColor(hexString: "8bcb2f").createImageFromUIColor(), for: UIControlState())
		cancelButton.setBackgroundImage(UIColor(hexString: "7d4bd2").createImageFromUIColor(), for: .highlighted)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	@IBAction func confirmButtonClickAction(_ sender: Any) {
		if let delegate = self.delegate {
			delegate.marketingAlertControllerConfirmed(self)
		}
	}

	@IBAction func cancelButtonClickAction(_ sender: Any) {
		if let delegate = self.delegate {
			delegate.marketingAlertControllerCanceled(self)
		}
	}
}

extension MarketingAlertController : UIViewControllerTransitioningDelegate {
	public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return MarketingAlertAnimation(isPresenting: true)
	}
	
	public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return MarketingAlertAnimation(isPresenting: false)
	}
}

open class MarketingAlertAnimation : NSObject, UIViewControllerAnimatedTransitioning {
	let isPresenting: Bool
	
	init(isPresenting: Bool) {
		self.isPresenting = isPresenting
	}
	
	open func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		if (isPresenting) {
			return 0.45
		} else {
			return 0.25
		}
	}
	
	open func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		if (isPresenting) {
			self.presentAnimateTransition(transitionContext)
		} else {
			self.dismissAnimateTransition(transitionContext)
		}
	}
	
	func presentAnimateTransition(_ transitionContext: UIViewControllerContextTransitioning) {
		let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! PermissionAlertController
		let containerView = transitionContext.containerView
		
		alertController.view.alpha = 0.0
		alertController.view.center = alertController.view.center
		alertController.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
		
		containerView.addSubview(alertController.view)
		
		UIView.animate(withDuration: 0.25,
					   animations: {
						alertController.view.alpha = 1.0
						alertController.view.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
		},
					   completion: { finished in
						UIView.animate(withDuration: 0.2,
									   animations: {
										alertController.view.transform = CGAffineTransform.identity
						},
									   completion: { finished in
										if (finished) {
											transitionContext.completeTransition(true)
										}
						})
		})
	}

	func dismissAnimateTransition(_ transitionContext: UIViewControllerContextTransitioning) {
		let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! PermissionAlertController
		UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
					   animations: {
						alertController.view.alpha = 0.0
						alertController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
		},
					   completion: { finished in
						transitionContext.completeTransition(true)
		})
	}
}


