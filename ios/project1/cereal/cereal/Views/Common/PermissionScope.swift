//
//  PermissionScope.swift
//  cereal
//
//  Created by srkang on 2018. 7. 4..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import AVFoundation
import Photos
import Contacts
import AddressBook

class PermissionScope {
    
    typealias PermissionResultClosure   = (_ finished : Bool, _ result : PermissionResult? , _ error : Error? ) -> Void
    typealias StatusRequestClosure = (_ status: PermissionStatus) -> Void
    typealias authClosureType = ( Bool, [PermissionResult] ) -> Void
    
    lazy var permissionMessages: [PermissionType : String] = [:]
    var configuredPermissions: [Permission] = []
    
    var permissionResultClosures : [PermissionType : PermissionResultClosure] = [:]
    var permissionResultFinishedList : [PermissionType : Bool] = [:]
    
    var onAuthChange : authClosureType? = nil
    
//    var notificationTimer : Timer?
    
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    
    init() {
    }
    
    // addPermission 할때 마다 configuredPermissions , permissionMessages 에 등록
    func addPermission(_ permission: Permission, message: String) {
        configuredPermissions.append(permission)
        permissionMessages[permission.type] = message
    }
    
    // PermissionType 에 대해 권한 요청
    func requestPermission(type: PermissionType, permissionResult : @escaping PermissionResultClosure ) {
        
        if let finished = permissionResultFinishedList[type]  {
            
            // false 일 경우 , 다시 시도 못하게
            guard finished else {
                permissionResult(false,nil,nil)
                return
            }
            
            permissionResultFinishedList[type] = false
        } else {
             // 아예 없는 경우, 새롭게 생성
            permissionResultFinishedList[type] = false
        }
        
        permissionResultClosures[type] = permissionResult
        
        delay(0.01) {
            // PermissionType 따라 퍼미션 요청하는 function 값 요청
            let  action =  self.requestAction(type: type)
            //  function 실행
            action()
        }
    }
    
    // ()->Void 형태 function 을 리턴
    // PermissionType 대해 request 하는 function 을 리턴
    func requestAction(type: PermissionType) -> () -> Void {
        switch type {
        case .camera:
            return self.requestCamera
        case .photos:
            return self.requestPhotos
        case .contacts:
            return self.requestContacts
        case .notifications:
            return self.requestNotifications
        }
    }
    
    
    
    func showAlertPermission(type: PermissionType, permissionResult : PermissionResult  ) {
    }
    
    //  푸시 권한 요청
    @objc public func requestNotifications() {
        
        // 권한 요청 하기 위해 status 를 먼저 질의
        statusNotifications { (status) in
            
            // delay 한 이유는 ,대체적으로 request 의 콜백을 메인쓰레드가 아닌 백그라운드 쓰레드 인 경우가 많아서
            // 메인쓰레드 통해 처리 하기 위해  delay 함수를 사용
            delay(0.0) {
                switch status {
                case .unknown:
                    // 권한 요청을 한 적이 없는 경우
                    
                    // configuredPermissions 에서 NotificationsPermission 인 것 찾기
                    let notificationPermission = self.configuredPermissions.first{
                        $0 is NotificationsPermission
                        } as? NotificationsPermission
                    
                    // iOS 10 이상인 경우
                    if #available(iOS 10, *) {
                        
                        // UNUserNotificationCenter 로 부터 권한을 request
                        let center = UNUserNotificationCenter.current()
                        center.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in


                            self.requestPermissionDefaultSet(type: .notifications)

                            guard error == nil else {
                                log?.debug(error as Any)
                                self.requestResult(type: .notifications, status: .error, error: error)
                                return
                            }
                            if granted {
                                if let unNotificationCategories = notificationPermission?.unNotificationCategories {
                                    center.setNotificationCategories(unNotificationCategories)
                                }
                                self.requestResult(type: .notifications, status: .authorized, error: nil)
                            }else {
                                self.requestResult(type: .notifications, status: .unauthorized, error: nil)
                            }


                        }
                    }  else {
                    
                        // iOS 9 이하 인 경우 UIApplication.shared.registerUserNotificationSettings 통해서 권한을 요청하고 ,
                        // request 대한 결과값을 callback 클로저로 받을 수 없어,
                        // trick 으로  Push 권한 요청 하면, 푸시 권한요청 OS 팝업이 나오면, 백그라운드 진입하는것을 착안해서,
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.showingNotificationPermission), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
                    
                        let settings = UIUserNotificationSettings(types: [.alert, .sound, .badge],
                                                                  categories: nil)
                        UIApplication.shared.registerUserNotificationSettings(settings)
                }
                case .unauthorized:
                    ()
                case .disabled:
                    ()
                case .authorized:
                    // 권한이 승인 한 경우
                    self.permissionResultFinishedList[.notifications] = true
                default:
                    ()
                }
            }
        }
        
    }
    
    // 푸시 권한 상태 값 조회
    func statusNotifications( completion : @escaping  (PermissionStatus) -> Void ) {
       
        // iOS 10 이상 인 경우
        if #available(iOS 10, *) {

            let center = UNUserNotificationCenter.current()

            log?.debug("statusNotifications center:\(center)")
            center.getNotificationSettings {
                settings in

                log?.debug("statusNotifications settings:\(settings)")
                log?.debug("statusNotifications settings.authorizationStatus:\(settings.authorizationStatus)")

                switch settings.authorizationStatus {
                case .notDetermined:
                    completion(.unknown)
                case .denied:
                    completion(.unauthorized)
                case .authorized:
                    completion(.authorized)
                default:
                    ()
                }
            }

        } else {
            // iOS 9 이하 인 경우
            let settings = UIApplication.shared.currentUserNotificationSettings
            if let settingTypes = settings?.types , settingTypes != UIUserNotificationType() {
                completion(.authorized)
            } else {
                // unauthorized , unknown 상태를  다음번에 알 수 없으므로, NSUserDefaults에 request 한 적이 있는지를 저장한다.
                if defaults.bool(forKey: CerealConstrants.NSUserDefaultsKeys.requestedNotifications) {
                    completion(.unauthorized)
                } else {
                    completion(.unknown)
                }
            }
       }
    }
    
    
    // 카메라 권한 요청
    @objc public func requestCamera() {
        
        statusCamera { (status) in
            switch status {
            case .unknown:
              
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                    if granted {
                        self.requestResult(type: .camera, status: .authorized, error: nil)
                    }else {
                        self.requestResult(type: .camera, status: .unauthorized, error: nil)
                    }
                })
                
                
            case .unauthorized:
                //showDeniedAlert(.notifications)
                ()
            case .disabled:
                //showDisabledAlert(.notifications)
                ()
            case .authorized:
                self.permissionResultFinishedList[.camera] = true
            default:
                ()
            }
            
        }
        
    }
    
    // 사진  권한 요청
    @objc public func requestPhotos() {
        
        statusPhotos { (status) in
            switch status {
            case .unknown:
                
                // iOS8
                PHPhotoLibrary.requestAuthorization({ (phAuthorizationStatus) in
                    
                    switch phAuthorizationStatus {
                    case .authorized:
                          self.requestResult(type: .photos, status: .authorized, error: nil)
                    case .restricted, .denied:
                        self.requestResult(type: .photos, status: .unauthorized, error: nil)
                    case .notDetermined:
                        self.requestResult(type: .photos, status: .unknown, error: nil)
                    }
                })
                
            case .unauthorized:
                
                //showDeniedAlert(.notifications)
                ()
            case .disabled:
                //            showDisabledAlert(.notifications)
                ()
            case .authorized:
                self.permissionResultFinishedList[.photos] = true
            default:
                ()
            }
            
        }
        
    }
    
    // 연락처   권한 요청
    @objc public func requestContacts() {
        
        statusContacts { (status) in
            switch status {
            case .unknown:
                
                // iOS 9 이상인 경우 CNContactStore 통해서
                 if #available(iOS 9, *) {
                    CNContactStore().requestAccess(for: .contacts, completionHandler: {
                        success, error in

                        if success {
                            self.requestResult(type: .contacts, status: .authorized, error: nil)
                        }else {
                            self.requestResult(type: .contacts, status: .unauthorized, error: nil)
                        }
                       
                    })
                 } else {
                    
                    // iOS 8 이하인 경우 ABAddressBookRequestAccessWithCompletion API  통해서
                    ABAddressBookRequestAccessWithCompletion(nil) { success, error in
                        if success {
                            self.requestResult(type: .contacts, status: .authorized, error: nil)
                        } else {
                            self.requestResult(type: .contacts, status: .unauthorized, error: nil)
                        }
                    }
                    
                }
            case .unauthorized:
                self.permissionResultFinishedList[.contacts] = true
            case .disabled:
                self.permissionResultFinishedList[.contacts] = true
            case .authorized:
                self.permissionResultFinishedList[.contacts] = true
            default:
                ()
            }
        }
    }
    
   
    
    // 카메라 권한 상태 값 조회
    func statusCamera( completion :  @escaping   (PermissionStatus) -> Void ) {
        
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        
        switch status {
        case .authorized:
            return completion(.authorized)
        case .restricted, .denied:
            return completion(.unauthorized)
        case .notDetermined:
            return completion(.unknown)
        }
    }
    
    // 사진  권한 상태 값 조회
    func statusPhotos( completion :  @escaping   (PermissionStatus) -> Void ) {
        
        // iOS8
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .authorized:
            return completion(.authorized)
        case .restricted, .denied:
            return completion(.unauthorized)
        case .notDetermined:
            return completion(.unknown)
        }
    }
    
    // 연락처 권한 상태 값 조회
    func statusContacts( completion :  @escaping (PermissionStatus) -> Void ) {
        
        // iOS 9 이상 인경우
        if #available(iOS 9, *) {
            let status = CNContactStore.authorizationStatus(for: .contacts)

            switch status {
            case .authorized:
                return completion(.authorized)
            case .restricted, .denied:
                return completion(.unauthorized)
            case .notDetermined:
                return completion(.unknown)
            }
        } else {
            // iOS 8 이하 인경우
            let status = ABAddressBookGetAuthorizationStatus()
            switch status {
            case .authorized:
                return completion(.authorized)
            case .restricted, .denied:
                return completion(.unauthorized)
            case .notDetermined:
                return completion(.unknown)
            }
        }
    }
    
    
    
    func requestResult(type: PermissionType, status : PermissionStatus, error: Error?) {
        
        DispatchQueue.main.async {
            self.permissionResultFinishedList[type] = true
            let permissionResultClosure = self.permissionResultClosures[type]
            let permissionResult = PermissionResult(type: type, status: status)
            permissionResultClosure!(true, permissionResult, error)
        }
    }
    
    
    
    // PermissionType 에 따라서 권한 상태   status 관련된 메소드를 호출하여 콜백을 받는다.
    func statusForPermission(_ type: PermissionType, completion : @escaping  (PermissionStatus) -> Void ) {
 
        switch type {
        case .contacts:
            statusContacts { contactsStatus in
                completion(contactsStatus)
            }
        case .notifications:
            statusNotifications { notificationStatus in
                completion(notificationStatus)
            }
        case .camera:
            statusCamera { cameraStatus in
                completion(cameraStatus)
            }
        case .photos:
            statusPhotos { photoStatus in
                completion(photoStatus)
            }
        }
      
      
    }
    
    func requestPermissionDefaultSet(type : PermissionType) {
        
        var permissionDefaultsKey : String!
        switch type {
        case .notifications:
            permissionDefaultsKey = CerealConstrants.NSUserDefaultsKeys.requestedNotifications
            self.defaults.set(true, forKey: permissionDefaultsKey)
        default:
            ()
        }
        
        self.defaults.synchronize()
    }
    
    func requestPermissionAfterDefaultSet(type : PermissionType) {
        
        var permissionDefaultsKey : String!
        switch type {
        case .notifications:
            permissionDefaultsKey = CerealConstrants.NSUserDefaultsKeys.requestedAfterNotifications
            self.defaults.set(true, forKey: permissionDefaultsKey)
        default:
            ()
        }
        
        self.defaults.synchronize()
    }
    
    
    // 권한 거절 했을때 Alert
    func showDeniedAlert(viewController : UIViewController ,  permission: PermissionType , authChange: authClosureType? = nil , callback : VoidClousre? = nil  ) {
       
        
//        let alert = UIAlertController(title: "\(permission.prettyDescription) 권한 거절",
//                                      message: "환경 설정설정에서 \(permission.prettyDescription) 권한 활성화 해야 기능을 사용할수 있습니다.",
//                                      preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "취소",
//                                      style: .cancel,
//                                      handler: nil))
//        alert.addAction(UIAlertAction(title: "이동",
//                                      style: .default,
//                                      handler: { action in
//
//                                        NSNotification.Name.UIApplicationDidBecomeActive.addObserver(self, selector: #selector(self.appForegroundedAfterSettings))
//
//
//                                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
//                                        UIApplication.shared.openURL(settingsUrl!)
//        }))
        
        
        let alert = UIAlertController(title: nil,
            message: "서비스를 이용하시려면\n앱 접근권한 허용이 필요합니다.\n설정에서 앱 권한 설정을\n켜주시기 바랍니다.",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "확인",
                                      style: .cancel) { _ in
                                        
                if let callback = callback {
                    callback()
                }
            })
            
        

        viewController.present(alert,  animated: true, completion: nil)
        
    }
    
    
   
    @objc func appForegroundedAfterSettings() {
        log?.debug("appForegroundedAfterSettings called")
        NSNotification.Name.UIApplicationDidBecomeActive.removeObserver(self)
        
        detectAndCallback()
    }
    
    
    func detectAndCallback() {
        
        DispatchQueue.main.async {
            
            if let onAuthChange = self.onAuthChange {
                self.getResultsForConfig({ results in
                    self.allAuthorized({ areAuthorized in
                        onAuthChange(areAuthorized, results)
                    })
                })
            }
        }
    }
    
    
    // addPersmission 통해서 configuredPermissions 을 저장하고
    // configuredPermissions 에 등록되었던 Permission 을 일괄적으로 status 를 질의 함.
    func getResultsForConfig(_ completionBlock: @escaping ([PermissionResult] ) -> Void) {
        
        var results : [PermissionResult] = []
        
        var index = 1
        
        for config in configuredPermissions {
            
            self.statusForPermission(config.type, completion: { (status) in
                let result = PermissionResult(type: config.type, status: status)
                results.append(result)
            })
            
            while  (index != results.count &&  RunLoop.current .run(mode: .defaultRunLoopMode, before: Date.init(timeIntervalSinceNow: 0.1)) )  {
                log?.debug("completionBlock while")
            }
            
            index += 1
        }
        
        completionBlock(results)
        log?.debug("completionBlock exit")
        
        
    }
    
    func allAuthorized(_ completion: @escaping (Bool) -> Void ) {
        getResultsForConfig{ results in
            let result = results
                .first { $0.status != .authorized }
                .isNil
            completion(result)
        }
    }
    
}

extension PermissionScope {
    
    @objc func showingNotificationPermission() {
        let notifCenter = NotificationCenter.default
        
        notifCenter.removeObserver(self,
                            name: NSNotification.Name.UIApplicationWillResignActive,
                            object: nil)
        notifCenter .addObserver(self,
                         selector: #selector(finishedShowingNotificationPermission),
                         name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
//        notificationTimer?.invalidate()
    }
    
   
    @objc func finishedShowingNotificationPermission () {
        
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIApplicationWillResignActive,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIApplicationDidBecomeActive,
                                                  object: nil)
        
        // 허용불구하고, 때론 Unkown 상태로 나옴. 그래서 delay 여유있게 0.5 초 줌.
        delay(0.5) {
            self.statusNotifications { (status) in
                
                delay(0.1) {
                    
                    switch status {
                        
                    case .unauthorized:
                        self.requestResult(type: .notifications, status: .unauthorized, error: nil)
                    case .authorized:
                        self.requestResult(type: .notifications, status: .authorized, error: nil)
                    default:
                        ()
                    }
                    
                    self.requestPermissionDefaultSet(type: .notifications)
                }
            }
        }
      
    }
    
    
    
}
