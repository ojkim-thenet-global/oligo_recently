//
//  MenuInfo.swift
//  cereal
//
//  Created by srkang on 2018. 8. 10..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SwiftyJSON

class MenuInfo {
    
    static let ID_MAIN      = "MAN_001" // Main
    static let ID_MEM_INTRO = "MEM_000" // 회원가입
    
    static let ID_INVEST    = "MAN_002_1" // 간편투자메인
    static let ID_WALLET    = "MAN_002_2" // 지갑메인
    static let ID_COMMUNITY = "MAN_002_3" // 리얼투자정보메인
    
    static let ID_ALARM     = "ALL_003" // 알림
    static let ID_SETTING   = "ALL_004" // 설정
    static let ID_MYINFO    = "ALL_005" // 내정보 관리
    static let ID_BADGEINFO = "ALL_007" // 뱃지 획득 방법
    
    
    
    
    static let ID_NOTICE    = "ALL_013" // 공지사항
    static let ID_EVENT     = "ALL_015" // 이벤트
    static let ID_POINT     = "ALL_017" // 포인트안내
    static let ID_GIFTYCON  = "MYP_027" // 기프티콘
    static let ID_FAQ       = "ALL_018" // FAQ
    static let ID_QNA       = "ALL_019" // 1:1 문의
    
    
    static let ID_INV_ANA       = "INV_014_01" // 투자 성향분석
    static let ID_RECOMMEND_USER = "ALL_012" // 친구추천
    
    
    static let ID_LOG_FIND_ID   = "LOG_004" // ID변경
    static let ID_LOG_FIND_PW   = "LOG_006" // 비밀번호 찾기
    static let ID_LOG_CHANG_PW_90   = "LOG_028" // 90일 비밀번호 찾기
    
    
    
    
    enum MenuEnum   {
        case main
        case memIntro
        case inveset
        case wallet
        case community
        case alarm
        case setting
        case myInfo
        case badgeInfo
        case notice
        case event
        case point
        case giftycon
        case faq
        case qna
        case invAna
        case recommendUser
        case logFindId
        case logFindPw
        case logChangePw90
        
        
        func menuId() -> String {
            switch self {
            case .main:         return ID_MAIN
            case .memIntro:     return ID_MEM_INTRO
            case .inveset:      return ID_INVEST
            case .wallet:       return ID_WALLET
            case .community:    return ID_COMMUNITY
            case .alarm:        return ID_ALARM
            case .setting:      return ID_SETTING
            case .myInfo:       return ID_MYINFO
            case .badgeInfo:    return ID_BADGEINFO
            case .notice:       return ID_NOTICE
            case .event:        return ID_EVENT
            case .point:        return ID_POINT
            case .giftycon:     return ID_GIFTYCON
            case .faq:          return ID_FAQ
            //                case .faq:          return "234234234.do"
            case .qna:          return ID_QNA
            case .invAna:       return ID_INV_ANA
            case .recommendUser: return ID_RECOMMEND_USER
                
            case .logFindId:     return ID_LOG_FIND_ID
            case .logFindPw:     return ID_LOG_FIND_PW
            case .logChangePw90:     return ID_LOG_CHANG_PW_90
                
                
            }
        }
        
    }
    
    private static var  menuPrivateJSON : JSON?
    
    static  var versionJSON : JSON? // 버전 정보
    static  var eventJSON : JSON? // 이벤트 정보 리스트
    static  var filterEventJSON : JSON? // 오늘 다시보지 않기를 filter 해서 나온 이벤트 정보 리스트
    //OJKIM ADDED START - 푸시알림동의 내역 초기화
    static var pushMarketingJSON: JSON? = nil
    //OJKIM ADDED END
    
    static  var menuJSON : JSON?  {
        
        get {
            log?.debug("menuJSON called")
            if let menuPrivateJSON = menuPrivateJSON {
                return menuPrivateJSON
            } else {
                if let url = Bundle.main.url(forResource: "menu", withExtension: "json") {
                    
                    do {
                        if let data = try? Data(contentsOf: url) {
                            let json = try? JSON(data: data)
                            
                            menuPrivateJSON = json
                            return json
                        }
                        
                    } catch {
                        return nil
                    }
                }
            }
            
            return nil
        }
        set {
            menuPrivateJSON = newValue
        }
        
    }
    
    // menuId 로 부터 menu URL 리턴
    static func menuURL(from menuId : MenuEnum) -> String? {
        
        if let menujson =  menuJSON {
            let menu_list = menujson["menu_list"]
            
            if let firstJson = menu_list.arrayValue.first(where: { (json) -> Bool in
                json["menu_id"].stringValue == menuId.menuId()
            }) {
//OJKIM ADDED START - 1:1문의를 카카오톡문의로 변경
				if menuId.menuId() == "ALL_019" {
					return "https://pf.kakao.com/_zxdeDj"
				}
//OJKIM ADDED END
                let url = firstJson["url"].stringValue
                log?.debug("url:\(url)")
                return url
            }
        }
        
        return nil
    }
    
    // menuIdString 로 부터 menu URL 리턴
    static func menuURL(from menuIdString : String) -> String? {
        
        if let menujson =  menuJSON {
            let menu_list = menujson["menu_list"]
            
            if let firstJson = menu_list.arrayValue.first(where: { (json) -> Bool in
                json["menu_id"].stringValue == menuIdString
            }) {
                let url = firstJson["url"].stringValue
                log?.debug("url:\(url)")
                return url
            }
        }
        
        return nil
    }
    
    // 메뉴가 로그인 필요하는지
    static func menuLoginYn(from menuId : MenuEnum) -> Bool {
        
        if let menujson =  menuJSON {
            let menu_list = menujson["menu_list"]
            
            if let firstJson = menu_list.arrayValue.first(where: { (json) -> Bool in
                json["menu_id"].stringValue == menuId.menuId()
            }) {
//OJKIM ADDED START - 1:1문의를 카카오톡문의로 변경
				if menuId.menuId() == "ALL_019" {
					return false
				}
//OJKIM ADDED END
                let login_check = firstJson["login_check"].stringValue
                
                if login_check == "Y" {
                    return true
                } else {
                    return false
                }
                
            }
        }
        
        return false
    }
    
    
    
    
}
