//
//  Chain.swift
//  cereal
//
//  Created by srkang on 2018. 8. 20..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

// 체인 잡 구분
enum ChainJobType {
    
    case pushAuthorization   // 앱 푸시 등록 여부 체크
    
    case permissionInitialCheck // iOS  //필수/선택 고지 보여줌(최초)
    case pushCheck // 앱 푸시 권한 요청
    case appGuide // 앱 가이드
    
    case appShiled // 앱위변조체크및 Intro 요청
    
    case autoLogin // 자동 로그인
    case fcmRegister // FCM 등록
    
//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
	case marketingConfirm //마케팅정보활용확인
//OJKIM ADDED END
}


// Chain 인터페이스
/*
 jobType : 체인 잡 구분(역할) ChainJobType
 nextChain : 현재 실행 체인 끝난 후에, 다음 실행할 체인
 setNextChain 메소드 : 다음 체인 nextChain 값 설정
 doJob 메소드 : 현재 체인 실행
 byPassJob 메소드 : 현재 체인을  건너뛸지(bypass) ,  true 이면 건너뜀(byPass), false 이면 건너뛰지 않고 , 현재 체인 실행
 contextResult  콜백 메소드 : 현재 체인에 실행하지 않고, context  에 위임 할 경우, context 로 부터 실행 결과 콜백
*/
protocol Chain {
    var jobType : ChainJobType! { get set } //  체인 잡 구분(역할) ChainJobType
    var  nextChain : Chain? { get } // 현재 실행 체인 끝난 후에, 다음 실행할 체인
    func setNextChain(_ nextChain: Chain?) // 다음 체인 nextChain 값 설정
    
    func doJob() // 현재 체인 실행
    func byPassJob()
    func contextResult( success : Bool)
}

// ChainContext 인터페이스
/*
 isByPass 메소드 : 현재 체인을  건너뛸지(bypass) ,  true 이면 건너뜀(byPass), false 이면 건너뛰지 않고 , 현재 체인 실행
 notifyBegin 메소드 : 현재 체인이 실행중 콜백 받은 메소드
 notifySuccess 메소드 : 현재 체인이 성공 콜백 받은 메소드
 notifyError 메소드   : 현재 체인이 성공 콜백 받은 메소드
 finishChain 메소드   : 전체 체인이 완료 콜백 받은 메소드
 contextAction 메소드   : 현재 체인 실행을 ChainContext 에 위임해서 실행함.
*/

protocol ChainContext {
    
    func isByPass(_ jobType : ChainJobType) -> Bool
    func notifyBegin(_ jobType : ChainJobType) -> Void
    func notifySuccess(_ jobType : ChainJobType) -> Void
    func notifyError(_ jobType : ChainJobType) -> Void
    func finishChain()
    func contextAction(_ jobType: ChainJobType, chain : Chain? )
}




// Chain Interface 껍데기를 구현한 Abstract 클래스
// setNextChain 메소드 , nextJob , doJob , byPassJob  메소드 템플릿 구현

class AbstractChain : Chain {
   
    var jobType : ChainJobType!
    
    var context : ChainContext!
    var nextChain: Chain?
    var jobResult: (( Bool,  Error?) -> Void)!
    
    required init(context : ChainContext) {
        self.context = context
    }
    
    func setNextChain(_ nextChain: Chain? = nil) {
        self.nextChain = nextChain
    }
    
    func nextJob(success : Bool, error : Error? ) {
        
        // 성공 했을 경우,  체인컨텍스트에게  notifySuccess 노티, 다음 체인을 실행한다.
        // 다음 체인이 없는 경우는, 모든 체인이 끝났다는것을 체인컨텍스트에게 finishChain 노티
        if success  {
            context.notifySuccess(jobType)
            if let nextChain = self.nextChain {
                nextChain.doJob()
            } else {
                // 전체 체인 완료
                context.finishChain()
            }
        } else {
            // 실패 했을 경우 , 체인컨텍스트에게  notifyError를 노티,  체인컨텍스트가 다음 진행을 어떻게 할지를 결정함.
            context.notifyError(jobType)
            
        }
    }
    
    // 체인 작업이 시작하면
    func doJob()  {
        context.notifyBegin(jobType)
    }
    
    func byPassJob() {
        nextJob(success: true, error: nil)
    }
    
    func contextResult( success : Bool) {
        log?.debug(success)
    }
}


// 푸시 권한 체크
class  PushAuthorizationChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .pushAuthorization
    }
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            self.pushAuthorizationCheck()
            
        }
    }
    
    
    func pushAuthorizationCheck() {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
          
            // 푸시 권한 체크를 permissionScope의 statusNotifications ㄹ
            permissionScope.statusNotifications(completion: { (status) in
                
                delay(0.0) {
                    if status == .unauthorized {
                        //self.context.contextAction(self.jobType,chain: self)
                        
                        
                        let permissionDefaultsKey = CerealConstrants.NSUserDefaultsKeys.requestedAfterNotifications
                        
                        if  !UserDefaults.standard.bool(forKey: permissionDefaultsKey) {
                            permissionScope.showDeniedAlert(viewController: self.context as! UIViewController, permission: .notifications, callback : {
                                UserDefaults.standard.set(true, forKey: permissionDefaultsKey)
                                self.nextJob(success: true, error: nil)
                            })
                        } else {
                            self.nextJob(success: true, error: nil)
                        }
                        
                        
                        
                    } else if status == .authorized {
                        self.nextJob(success: true, error: nil)
                    } else {
                        self.nextJob(success: true, error: nil)
                    }
                }
                
                
            })
            
            
        }
    }
}


class  AppShieldCheckChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .appShiled
    }
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            delay(0.5) {
                self.appShieldCheck()
            }
        }
    }
    
    
    // 앱위변조 체크 하면서, 앱 실행 정보 (메뉴정보, 버전정보 ) 가져오기
    func appShieldCheck() {
        
        //nextJob(success: true, error: nil)
        
        let xasManager = XASManager.shared
        let result = xasManager.chekApp()
        
        var viewController :UIViewController!
        if context is UIViewController {
            viewController = context as! UIViewController
        } else {
            viewController = UIViewController.windowTopViewController()!
        }
        
        // 성공
        if result == 0 {
            
            let  sessionID = String(format: "%08X", xasManager.sessionID!)
            let token = xasManager.token!
            //            sessionID += "3124324"
            //            String(I)  xasManager.sessionID
            
            /*
             {
             "msg" : "정상적으로 처리되었습니다.",
             "data" : {
             "versionInfo" : {
             "version" : "1.0.0",
             "popup_msg" : "테스트 아이폰",
             "compulsion_update" : "Y",
             "market_url" : "iphone:\/\/"
             },
             "eventInfo" : {
             "popup_kn" : "1",
             "title" : "hhhh",
             "note" : "asdf",
             "link_url" : null,
             "img_url" : "\/uploadImg\/PopUPLOAD\/null\/41fb6cb53ce74e24a420c6ffde4a0607.png"
             }
             },
             "code" : "0000"
             }
             */
            
            let parameters = ["os_cd" : "I" , "appshield_session_id" : sessionID , "appshield_token" : token]
            NetworkInterface.transferServer(viewController: viewController,  operation: .verifyApp , parameters: parameters  ) { [weak  self]  (error, response) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if error == nil {
                    
                    // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                    if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                        
                        let json = JSON(response)
                        
                        let jsonData = json["data"]
                        
                        let versionInfo = jsonData["versionInfo"]
                        let eventInfo   = jsonData["eventInfo"]
                        
                        Configuration.serverVersion = versionInfo["version"].stringValue
                        let menu_list = jsonData["menu_list"]
                        
                        MenuInfo.menuJSON = ["menu_list" : menu_list]
                        MenuInfo.versionJSON = ["versionInfo" : versionInfo]
                        MenuInfo.eventJSON = ["eventInfo" : eventInfo]
                        
                      
                        
                        
                        log?.debug("versionInfo:\(versionInfo)")
                        
                        /*
                         "versionInfo" : {
                         "version" : "1.1.1",
                         "popup_msg" : "수정테스트",
                         "compulsion_update" : "N",
                         "market_url" : "https:\/\/mm:update"
                         }
                        */
                        
                        let compulsion_update = versionInfo["compulsion_update"].stringValue
                        let market_url  = versionInfo["market_url"].stringValue
                        let popup_msg   = versionInfo["popup_msg"].stringValue
                        let version     = versionInfo["version"].stringValue
                        
                        let serverVersionComponents = version.components(separatedBy: ".")
                        let localVersionComponents = Configuration.appVersion.components(separatedBy: ".")
                        
                        var serverVersionInt = 0
                        var localVersionInt = 0
                        
                        var weight = 10000
                        for element in serverVersionComponents {
                            serverVersionInt += Int(element)! * weight
                            weight /= 100
                        }
                        
                        weight = 10000
                        for element in localVersionComponents {
                            localVersionInt += Int(element)! * weight
                            weight /= 100
                        }
                        
                        if serverVersionInt > localVersionInt {
                            // 강제 앱 업데이트
                            if compulsion_update == "Y" {
                                let alertController = UIAlertController(title: nil, message: popup_msg, preferredStyle: .alert)
                                let alertAction = UIAlertAction(title: "앱 업데이트", style: .cancel) { _ in
                                    
//                                    let appId = "838696918"
//                                    let url = "itms-apps://itunes.apple.com/app/id\(appId)"
                                    
                                    
                                    
                                    UIApplication.shared.openURL(URL(string: market_url)!)
                                    
                                    delay(0.1) {
                                        exit(0)
                                    }
                                    
                                    
                                }
                                alertController.addAction(alertAction)
                                viewController.present(alertController, animated: true, completion: nil)

                            } else {
                                let alertController = UIAlertController(title: nil, message: popup_msg, preferredStyle: .alert)
                           
                                let updateAction = UIAlertAction(title: "앱 업데이트", style: .default) { _ in
                                    
                                    UIApplication.shared.openURL(URL(string: market_url)!)
                                    
                                    delay(0.1) {
                                        exit(0)
                                    }
                                    
                                }
                                
                                let nextction = UIAlertAction(title: "다음에하기", style: .default) { _ in
                                    strongSelf.nextJob(success: true, error: nil)
                                }
                                
                                
                                alertController.addAction(nextction)
                                alertController.addAction(updateAction)
                                
                                
                                viewController.present(alertController, animated: true, completion: nil)
                            }
                        } else {
                            strongSelf.nextJob(success: true, error: nil)
                        }
                        
//                        strongSelf.nextJob(success: true, error: nil)
                        
                        
                    } else if let response =  response,  let msg = response["msg"] as? String {
                        // code 가 Success 가 아닌 경우
                        let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "확인", style: .cancel) { _ in
                            strongSelf.nextJob(success: false, error: nil)
                        }
                        alertController.addAction(alertAction)
                        viewController.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        // response 없는 경우
                    }
                } else {
                    //  AlamoFire 오류
                    strongSelf.nextJob(success: false, error: nil)
                }
                
            }
        } else {
            
            // FIXME://AlertController 앱위변조 1차 통과 못함.
            // 네트워크 오류 포함.
            let alertController = UIAlertController(title: nil, message: xasManager.errorMessage!, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "확인", style: .cancel) { _ in
                
                
                self.nextJob(success: false, error: nil)
            }
            alertController.addAction(alertAction)
            viewController.present(alertController, animated: true, completion: nil)
            
        }
    }
    
}



class  AutoLoginChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .autoLogin
    }
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            autoLogin()
        }
    }
    
    func autoLogin() {
        
        var parameters  : [String:Any] = [:]
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
        
        if custItem.auto_login , let user_hp = custItem.user_hp  {
            // 자동 로그인
            parameters = ["login_type" : "A"  , "user_hp" :  user_hp,
            ]
            
            if let token = custItem.token, let  user_no = custItem.user_no {
                parameters["token"] = token
                parameters["user_no"] = user_no
                parameters["auto_login"] = "Y"
            }
        }
        
        
        var viewController :UIViewController!
        if context is UIViewController {
            viewController = context as! UIViewController
        } else {
            viewController = UIViewController.windowTopViewController()!
        }
        
        
        let xasManager = XASManager.shared
        let result = xasManager.chekApp()
        
        
        
        // 성공
        if result == 0 {
            
            let appshield_session_id = String(format: "%08X", xasManager.sessionID!)
            let appshield_token = xasManager.token!
            
//            let progressHUD = MBProgressHUD.showCerealCHUDAdded(to: viewController.view, animated: true)
            
            
            var parameters  : [String:Any] = [:]
            let keyChainWrapper = ApplicationShare.shared.keychainWrapper
            let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
            
            
            // 자동 로그인
            parameters = ["login_type" : "A"  , "user_hp" :  custItem.user_hp!]
            
            if let token = custItem.token, let  user_no = custItem.user_no {
                parameters["token"] = token
                parameters["user_no"] = user_no
                parameters["auto_login"] = "Y"
                parameters["appshield_session_id"] = appshield_session_id
                parameters["appshield_token"] = appshield_token
                
            }
            
            
            
            NetworkInterface.transferServer(viewController: viewController, operation: .login, parameters: parameters ) { [weak self]  (error, response) in
                
                guard let strongSelf = self else {
                    return
                }
                
                
//                progressHUD.hide(animated: true)
                
                
                if error == nil {
                    
                    /*
                     {
                     "msg" : "로그인 핀번호 복호화 오류",
                     "data" : {
                     "aResultCode" : "50006",
                     "aResultMessage" : "[DB fail]-[Does not exist, can not be select, deleted or updated to] "
                     },
                     "code" : "1001"
                     }
                     */
                    
                    // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                    if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                        if let data = response["data"] as? [String:Any]   {
                            
                            // GA 로그인 이벤트  자동로그인 Y
                            Analytics.logEvent("login",parameters:["sign_up_method" : "A"])
                            
                            let last_login_time = data["Last_login_time"]
                            let token           = data["token"]
                            let user_no         = data["user_no"]
                            
                            // GA 이벤트 샘플
                            Analytics.logEvent("ecommerce_purchase",parameters:["firebase_screen_id" : "3"])
                            
                            Analytics.logEvent("screen_view",parameters:["firebase_event_origin" : "4"])
                            
                            let keyChainWrapper = ApplicationShare.shared.keychainWrapper
                            if let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as? KeyChainCustItem  {
                                log?.debug("custItem:\(custItem)")
                                
                                if let last_login_time = last_login_time {
                                    custItem.last_login_time = last_login_time as? String
                                }
                                
                                if let token = token {
                                    custItem.token = token as? String
                                }
                                
                                if let user_no = user_no {
                                    custItem.user_no = user_no as? String
                                }
                                //
                                
                                keyChainWrapper.set(custItem, forKey: Configuration.keyChainKey)
                                
                            }
                            
                            let loginInfo = ApplicationShare.shared.loginInfo
                            loginInfo.isLogin = true
                            
                            if let push_yn = data["push_yn"] as? String {
                                loginInfo.push_yn = push_yn
                            }
                            
                            if let user_level = data["user_level"] as? String {
                                loginInfo.user_level = user_level
                            } else if let user_level = data["user_level"] as? Int {
                                loginInfo.user_level = String(user_level)
                            }
                            
                            if let push_count = data["push_count"] as? String {
                                loginInfo.push_count = push_count
                            } else if let push_count = data["push_count"] as? Int {
                                loginInfo.push_count = String(push_count)
                            }
                            
                            if let simul_nm = data["simul_nm"] as? String {
                                loginInfo.simul_nm = simul_nm
                            }
                            if let nickname = data["nickname"] as? String {
                                loginInfo.nickname = nickname
                            }
                            if let asset_info = data["asset_info"] as? [String:Any]  ,
                                let balance      =  asset_info["balance"] as? Int ,
                                let asset_amt    =  asset_info["asset_amt"] as? Int {
                                loginInfo.balance   = String(balance)
                                loginInfo.asset_amt = String(asset_amt)
                                
                            }
                            if let user_img_url = data["user_img_url"] as? String  {
                                loginInfo.user_img_url = user_img_url
                            }
                            
                            // badge_list 의 item 의 badge_cd 로 sort 한다.
                            if let badge_list = data["badge_list"] as? [Dictionary<String,Any>] {
                                var tempBadge_list : [Dictionary<String,Any>] = []
                                for badge_item in badge_list {
                                    tempBadge_list.append(badge_item)
                                    //                                loginInfo.badge_list.append(badge_item)
                                }
                                
                                tempBadge_list.sort() { (item1,item2) in
                                    if Int(item1["badge_cd"] as! String) < Int(item2["badge_cd"] as! String) {
                                        return true
                                    } else {
                                        return false
                                    }
                                }
                                
                                log?.debug(tempBadge_list)
                                loginInfo.badge_list = tempBadge_list
                            }
                            
//                            if push_yn == nil || push_yn != "Y" {
                                strongSelf.nextJob(success: true, error: nil)
//                            } else {
//                                strongSelf.pushMarketingProcess()
//                            }
//
                            
                            
                        }
                    } else if let response =  response,  let msg = response["msg"] as? String {
                        // code 가 Success 가 아닌 경우
//                        let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
//                        let alertAction = UIAlertAction(title: "확인", style: .cancel) { _ in
//                            strongSelf.nextJob(success: false, error: nil)
//                        }
//                        alertController.addAction(alertAction)
//                        viewController.present(alertController, animated: true, completion: nil)
                        
                         strongSelf.nextJob(success: true, error: nil)
                        
                    } else {
                        // response 없는 경우
                    }
                } else {
                    //  AlamoFire 오류
                    strongSelf.nextJob(success: false, error: nil)
                }
            }
            
            
        }
        else {
        }
        
        
        
    }

    func pushMarketingProcess() {
        if (ApplicationShare.shared.loginInfo.isLogin == false) {
            nextJob(success: true, error: nil)
            return;
        }
        
        if MenuInfo.pushMarketingJSON == nil {
            nextJob(success: true, error: nil)
            return;
        }
        
        let popupViewController = UIStoryboard.storyboard(.main).instantiateViewController() as SystemPopupViewController
        popupViewController.modalPresentationStyle = .overFullScreen
        let innerSystemPopupVC = InnerSystemPopupViewController()
        innerSystemPopupVC.parentVC = context as! UIViewController
        innerSystemPopupVC.strongSelf = self
        popupViewController.delegate = innerSystemPopupVC;
        (context as! UIViewController).present(popupViewController, animated: true, completion: nil)
    }
    
    class InnerSystemPopupViewController: SystemPopupViewControllerDelegate {
        var parentVC: UIViewController!
        var strongSelf: AutoLoginChain!
        func systemPopupViewControllerDidFinished(_ viewController : SystemPopupViewController , action : SystemPopupAction) {
            viewController.dismiss(animated: true, completion: nil)
            strongSelf.nextJob(success: true, error: nil)
        }
    }
}

class  FcmTokenChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .fcmRegister
    }
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            fcmTokenRegister()
        }
    }
    
    
    // FCM  Token 서버에 등록
    func fcmTokenRegister() {
        
        
        var viewController :UIViewController!
        if context is UIViewController {
            viewController = context as! UIViewController
        } else {
            viewController = UIViewController.windowTopViewController()!
        }
        
        let keyChainWrapper = ApplicationShare.shared.keychainWrapper
        let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
        
        if let fcm_token = custItem.fcm_token {
            // 파라미터  user_no : 고객번호, push_token : FCM 토큰
            let parameters  = ["user_no" : custItem.user_no! , "push_token" : fcm_token]
            
            //  operation: .pushToken =>  api/pushToken.do
            NetworkInterface.transferServer(viewController: viewController, operation: .pushToken, parameters: parameters ) { [weak self]  (error, response) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if error == nil {
                    if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                        if let data = response["data"] as? [String:Any]   {
                            log?.debug(data)
                            // 성공 했을경우 , 다음체인 실행
                            strongSelf.nextJob(success: true, error: nil)
                        }
                    } else if let response =  response,  let msg = response["msg"] as? String {
                        // code 가 Success 가 아닌 경우
                        let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "확인", style: .cancel) { _ in
                            // 실패 했을 경우 , 다음체인 실행 못하도록 false
                            strongSelf.nextJob(success: false, error: nil)
                        }
                        alertController.addAction(alertAction)
                        viewController.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        // response 없는 경우
                    }
                } else {
                    //  AlamoFire 오류
                    strongSelf.nextJob(success: false, error: nil)
                }
            }
        } else {
            nextJob(success: true, error: nil)
        }
        

    }
    
}


// iOS  필수/선택 고지 보여줌(최초)
class  PermissionInitialCheckChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .permissionInitialCheck
    }
    
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            delay(0.05) {
                self.alertPermission()
            }
        }
    }
    
    
    func alertPermission() {
        //nextJob(success: true, error: nil)
        // 체인에 직접 (nextJob) 으로 처리 하지 않고,
        // context 에 위임. 체인 자체에서 처리하기에  복잡함.
        self.context.contextAction(self.jobType ,chain: self)
    }
    
    // self.context.contextAction 통해서 일을 처리하고 나서 .
    // context 로 부터 contextResult 결과를 받음.
    // success 에 따라서 nextJob 을 진행할지(true) , 안할지(false)
    override func contextResult(success: Bool) {
        if success {
            nextJob(success: true, error: nil)
        } else {
            nextJob(success: false, error: nil)
        }
    }
    
    
}



// iOS Push 사용 동의 요청 (최초)
class  PushCheckChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .pushCheck
    }
    
    
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            delay(0.1) {
                self.pushPermission()
            }
        }
    }
    
    
    
    func pushPermission() {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusNotifications(completion: { (status) in
                
                delay(0.0) {
                    if status == .unknown {
                        
                        permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                            //self.pushResult(status: permissionStatus)
                        }
                        
                        permissionScope.requestPermission(type: .notifications, permissionResult: { (finished, permissionResult,error) in
                            
                            
                            
                            log?.debug("permissionResult:\(String(describing: permissionResult))")
                            if !finished {
                                log?.debug("finished 가 완료되지 않는 상태 \(finished)")
                            }
                            else {
                                if error != nil {
                                    log?.debug("error  가 발생된 상태 \(String(describing: error))")
                                }
                                else {
                                    if let permissionStatus = permissionResult?.status {
                                        self.pushResult(status: permissionStatus)
                                    }
                                }
                            }
                            
                        })
                    }
                    else {
                        self.pushResult(status: status)
                    }
                    
                }
                
                
            })
            
            
        }
        
    }
    
    func pushResult(status : PermissionStatus , final : Bool = false) {
        
        if !final {
            if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                
                // Push 가 동의 실패
                if status == .unauthorized {
                    // 다음 체인 실행하도록 , 일단 true, 다음 체인(pushAuthorizationChainAfter)에서 Push 상태를 읽기 때문 , pushAuthorizationChainAfter 에서 권한이 없으면, Alert 보여주고 , 프로그램 종료 (exit)
                    nextJob(success: true, error: nil)
                }
                // Push 동의
                else if status == .authorized {
                    // 승인하면 바로 FCM Register
                    (UIApplication.shared.delegate as! AppDelegate).fcmRegister()
                    // 다음 체인 실행
                    nextJob(success: true, error: nil)
                }
                
                permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                    
                    let result = permissionResults.first {$0.type == .notifications}
                    
                    
                    self.pushResult(status: result!.status, final: true)
                    
                }
                
            }
        }
        
    }
    
    
}


// App Guid 소개 체인
class  AppGuideChain : AbstractChain  {
    
    required init(context: ChainContext) {
        super.init(context: context)
        jobType = .appGuide
    }
    
    
    
    override func doJob() {
        if context.isByPass(jobType) {
            byPassJob()
        } else {
            delay(0.1) {
                self.context.contextAction(self.jobType ,chain: self)
            }
        }
    }
    
    
    override func contextResult(success: Bool) {
        if success {
            nextJob(success: true, error: nil)
        }
    }
 
    
}






//OJKIM ADDED START - 마케팅동의창 띄우기 - 확정안됨
class  MarketingConfirmChain : AbstractChain  {
	required init(context: ChainContext) {
		super.init(context: context)
		jobType = .marketingConfirm
	}
	
	override func doJob() {
		if context.isByPass(jobType) {
			byPassJob()
		} else {
			delay(0.05) {
				self.alertMarketingConfirm()
			}
		}
	}
	
	func alertMarketingConfirm() {
		self.context.contextAction(self.jobType ,chain: self)
	}
	
	// self.context.contextAction 통해서 일을 처리하고 나서 .
	// context 로 부터 contextResult 결과를 받음.
	// success 에 따라서 nextJob 을 진행할지(true) , 안할지(false)
	override func contextResult(success: Bool) {
		if success {
			nextJob(success: true, error: nil)
		} else {
			nextJob(success: false, error: nil)
		}
	}
}
//OJKIM ADDED END
