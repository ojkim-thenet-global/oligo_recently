//
//  CerealError.swift
//  cereal
//
//  Created by srkang on 2018. 8. 17..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

enum CerealError: Error {
    case businessError(reason: String)
}
