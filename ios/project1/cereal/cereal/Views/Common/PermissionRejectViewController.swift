//
//  PermissionRejectViewController.swift
//  cereal
//
//  Created by srkang on 2018. 8. 20..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

protocol PermissionRejectViewControllerDelegate : class{
    func permissionRejectControllerClosed(_ alertController: PermissionRejectViewController) -> Void
}

// 앱 푸시 필수 권한 설정 거부시 팝업
class PermissionRejectViewController : UIViewController {
    
    @IBOutlet weak var okButton: UIButton!
    
    weak var delegate : PermissionRejectViewControllerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        okButton.setBackgroundImage(UIColor(hexString: "8bcb2f").createImageFromUIColor(), for: UIControlState())
        okButton.setBackgroundImage(UIColor(hexString: "7ac612").createImageFromUIColor(), for: .highlighted)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okButtonClickAction(_ sender: Any) {
        
        if let delegate = self.delegate {
            delegate.permissionRejectControllerClosed(self)
        }
    }
}
