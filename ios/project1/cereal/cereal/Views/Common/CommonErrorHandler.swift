//
//  CommonErrorHandler.swift
//  cereal
//
//  Created by srkang on 2018. 7. 10..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

// 네트워크 통신 오류 관련된 공통 응답 처리 핸들러
class CommonErrorHandler    {
    
    static var errorHandler : CommonErrorHandler?
    var presenter : CerealErrorPresenter?

    static func errorHandler(viewController : UIViewController, error : Error ) {
        
        var devErrorMessage : String!
        var realErrorMessage : String!
        
        
        log?.debug("CommonErrorHandler Error while fetching tags: \(String(describing: error))")
        
        
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                devErrorMessage = "잘못된 URL 입니다.\n\(url) - \(error.localizedDescription)"
                realErrorMessage = "서비스 요청 오류입니다."
            case .parameterEncodingFailed(let reason):
                log?.debug("Parameter encoding failed: \(error.localizedDescription)")
                
                devErrorMessage = "파라미터 인코딩 오류 입니다.\n\(error.localizedDescription)\nFailure Reason: \(reason)"
                realErrorMessage = "서비스 요청 오류입니다."
                
            case .multipartEncodingFailed(let reason):
                
                devErrorMessage = "멀티파트 오류 인코딩 오류 입니다.\n\(error.localizedDescription)\nFailure Reason: \(reason)"
                realErrorMessage = "파일첨부 서비스 요청 오류입니다."
                
            case .responseValidationFailed(let reason):
                log?.debug("Response validation failed: \(error.localizedDescription)")
                log?.debug("Failure Reason: \(reason)")
                
                devErrorMessage = "응답 validate 오류 입니다.\n\(error.localizedDescription)\nFailure Reason: \(reason)"
                realErrorMessage = "서비스 요청 상태값 오류입니다."
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    log?.debug("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    log?.debug("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    log?.debug("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    log?.debug("Response status code was unacceptable: \(code)")
                }
            case .responseSerializationFailed(let reason):
                log?.debug("Response serialization failed: \(error.localizedDescription)")
                log?.debug("Failure Reason: \(reason)")
            }
            
            log?.debug("Underlying error: \(String(describing: error.underlyingError))")
        } else if let error = error as? URLError {
            log?.debug("URLError occurred: \(error)")
            log?.debug("URLError error.errorCode: \(error.errorCode)")
            devErrorMessage = "URL  오류 입니다.\n\(error.localizedDescription)\nFailure errorCode:: \(error.errorCode)"
            realErrorMessage = "서비스 요청 상태값 오류입니다."
            
        } else {
            devErrorMessage = "시스템 오류 입니다.\n\(error.localizedDescription)"
            realErrorMessage = "서비스 요청 상태값 오류입니다."
            log?.debug("Unknown error: \(error)")
        }
        
        
        errorHandler = CommonErrorHandler()
        errorHandler!.errorPresnet(viewController: viewController, titleMessage: devErrorMessage, subMessage: realErrorMessage)
        
     
//        let alertController = UIAlertController(title: "오류발생", message: devErrorMessage, preferredStyle: .alert)
//        let alertAction = UIAlertAction(title: "확인", style: .cancel) { (alertAction) in
//
//        }
//        alertController.addAction(alertAction)
//        viewController.present(alertController, animated: true, completion: nil)
        
    }
    
    private func errorPresnet(viewController : UIViewController , titleMessage: String, subMessage: String) {
        presenter = CerealErrorPresenter()
        presenter!.delegate = self
        presenter!.presentError(viewController, titleMessage: titleMessage, subMessage: subMessage)
    
    
    }
}

extension CommonErrorHandler : CerealErrorPresenterDelegate {
    func cerealErrorViewControllerFinished(_ presenter: CerealErrorPresenter) {

    }


}
