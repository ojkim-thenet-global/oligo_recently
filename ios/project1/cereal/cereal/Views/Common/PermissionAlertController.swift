//
//  PermissionAlertController.swift
//  cereal
//
//  Created by srkang on 2018. 8. 20..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

protocol PermissionAlertControllerDelegate : class{
    func permissionAlertControllerClosed(_ alertController: PermissionAlertController) -> Void
}

// / 앱 권한 고지 내용 팝업
class PermissionAlertController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    
    weak var delegate : PermissionAlertControllerDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        okButton.setBackgroundImage(UIColor(hexString: "7d4bd2").createImageFromUIColor(), for: UIControlState())
        okButton.setBackgroundImage(UIColor(hexString: "7d4bd2").createImageFromUIColor(), for: .highlighted)
        
//        self.providesPresentationContextTransitionStyle = true
//        self.definesPresentationContext = true
//        self.transitioningDelegate = self
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func okButtonClickAction(_ sender: Any) {
        
        if let delegate = self.delegate {
            delegate.permissionAlertControllerClosed(self)
        }
    }
}

extension PermissionAlertController : UIViewControllerTransitioningDelegate {
    
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning {
        return PermissionAlertAnimation(isPresenting: true)
    }
    
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PermissionAlertAnimation(isPresenting: false)
    }
}

open class PermissionAlertAnimation : NSObject, UIViewControllerAnimatedTransitioning {
    
    let isPresenting: Bool
    
    init(isPresenting: Bool) {
        self.isPresenting = isPresenting
    }
    
    open func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if (isPresenting) {
            return 0.45
        } else {
            return 0.25
        }
    }
    
    open func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if (isPresenting) {
            self.presentAnimateTransition(transitionContext)
        } else {
            self.dismissAnimateTransition(transitionContext)
        }
    }
    
    func presentAnimateTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        
        let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! PermissionAlertController
        let containerView = transitionContext.containerView
        
        
        
        alertController.view.alpha = 0.0
        alertController.view.center = alertController.view.center
        alertController.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    
        containerView.addSubview(alertController.view)
        
        UIView.animate(withDuration: 0.25,
                       animations: {
                        
                        alertController.view.alpha = 1.0
                        alertController.view.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                        
        },
                       completion: { finished in
                        UIView.animate(withDuration: 0.2,
                                       animations: {
                                        alertController.view.transform = CGAffineTransform.identity
                        },
                                       completion: { finished in
                                        if (finished) {
                                            transitionContext.completeTransition(true)
                                        }
                        })
        })
    }
    
    func dismissAnimateTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        
        let alertController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as! PermissionAlertController
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                       animations: {
                        
                        
                        alertController.view.alpha = 0.0
                        alertController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                        
        },
                       completion: { finished in
                        transitionContext.completeTransition(true)
        })
    }
}


