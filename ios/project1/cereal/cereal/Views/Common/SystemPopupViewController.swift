//
//  SystemPopupViewController.swift
//  cereal
//
//  Created by srkang on 2018. 9. 4..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

enum SystemPopupAction {
    case close
    case urlClose(String)
}

protocol SystemPopupViewControllerDelegate : class {
    func systemPopupViewControllerDidFinished(_ viewController : SystemPopupViewController , action : SystemPopupAction)
}

// 시스템 팝업, 이벤트 팝업 , 메인화면에서 띄운다.
class SystemPopupViewController: UIViewController {
    
    var delegate : SystemPopupViewControllerDelegate?
    
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    let fileManager = FileManager.default
    
    
    @IBOutlet weak var systemPopupView: UIView!
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
	@IBOutlet weak var confirmPopupView: UIView!
//OJKIM ADDED END
    @IBOutlet weak var eventPopupView: UIView!
    
    
    // systemPopupView
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var todayCheckView: UIView!
	
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
	@IBOutlet weak var confirmTitleLabel: UILabel!
	@IBOutlet weak var confirmMessageLabel: UILabel!
	@IBOutlet weak var confirmButton: UIButton!
	@IBOutlet weak var cancelButton: UIButton!
	var confirmLinkUrl: String!
    var isPushMargetingPopup: Bool = false
	var isFromAgreeFlag: Bool = false
	var user_no: String? = nil
//OJKIM ADDED END

     // eventPopupView
    
    @IBOutlet weak var eventImageView: UIImageView!
    
    var currentIndex : Int = 0
    
    var currentEventId : Int = 0
    var currentLinkeUrl : String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nextDisplayJSON(initial: true)
        // Do any additional setup after loading the view.
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   
    
    func displayJSON(json:JSON) {
        
        let event_id = json["event_id"].intValue
        let title = json["title"].stringValue
        let note = json["note"].stringValue
        
        currentEventId = event_id
        
        if json["popup_kn"].intValue == 1 || json["popup_kn"].stringValue == "1" {
            // 시스템 팝업
            eventPopupView.isHidden = true
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
			confirmPopupView.isHidden = true
//OJKIM ADDED END
            systemPopupView.isHidden = false
            
            titleLable.text = title
            messageLabel.text = note
            messageLabel.textAlignment = .left
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
		} else if json["popup_kn"].intValue == 3 || json["popup_kn"].stringValue == "3" ||
            json["popup_kn"].intValue == 4 || json["popup_kn"].stringValue == "4" {
            if isPushMargetingPopup == true || ApplicationShare.shared.loginInfo.isLogin == true {
                eventPopupView.isHidden = true
                confirmPopupView.isHidden = false
                systemPopupView.isHidden = true
                
                confirmTitleLabel.text = title
                confirmMessageLabel.text = note
                confirmMessageLabel.textAlignment = .center
                
                confirmLinkUrl = json["link_url"].stringValue
                
                cancelButton.setTitleColor(UIColor.init(hex: 000000), for: UIControlState())
                cancelButton.setBackgroundImage(UIColor(hexString: "e1e1e1").createImageFromUIColor(), for: UIControlState())
                
                if json["popup_kn"].intValue == 3 || json["popup_kn"].stringValue == "3" {
                    cancelButton.setTitle("아니오", for: UIControlState.normal)
                    confirmButton.setTitle("예", for: UIControlState.normal)
                } else {
                    cancelButton.setTitle("취소", for: UIControlState.normal)
                    confirmButton.setTitle("확인", for: UIControlState.normal)
                }
            } else {
                MenuInfo.pushMarketingJSON = json;
                nextDisplayJSON()
            }
//OJKIM ADDED END
		} else {
            // 일반 팝업
            eventPopupView.isHidden = false
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
			confirmPopupView.isHidden = true
//OJKIM ADDED END
            systemPopupView.isHidden = true
            
            let link_url = json["link_url"].stringValue
            currentLinkeUrl = link_url
            
            
            
            var documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("temp")
            
            var isDir : ObjCBool = false
            if fileManager.fileExists(atPath: documentsURL.path, isDirectory: &isDir) {
                if isDir.boolValue {
                    
                } else {
                    
                }
            } else {
                // file does not exist
                
                do {
                    try fileManager.createDirectory(at: documentsURL, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    
                }
            }
            
            documentsURL.appendPathComponent("\(event_id).png")
            
            eventImageView.image = UIImage(contentsOfFile: documentsURL.path)
            
        }
    }
    
    func nextDisplayJSON(initial:Bool = false){
        if isPushMargetingPopup == true {
            if MenuInfo.pushMarketingJSON != nil {
                displayJSON(json: MenuInfo.pushMarketingJSON!)
            }
        } else {
            if let eventJSON = MenuInfo.filterEventJSON  {
                
                if !initial {
                    currentIndex += 1
                }
                
                let arrayValue = eventJSON.arrayValue
                
                if currentIndex < arrayValue.count {
                    let json = arrayValue[currentIndex]
                    displayJSON(json: json)
                } else {
                    finishEvent()
                }
            }
        }
    }
    
    func finishEvent() {
        if let delegate = delegate {
            delegate.systemPopupViewControllerDidFinished(self, action: .close)
        }
    }
    
    
    @IBAction func okButtonClickAction(_ sender: Any) {
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
		if isFromAgreeFlag == true {
			finishEvent()
		} else {
//OJKIM ADDED END
        	nextDisplayJSON()
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
		}
//OJKIM ADDED END
    }
    
    @IBAction func closeButtonClickAction(_ sender: Any) {
        nextDisplayJSON()
    }
    
//OJKIM ADDED START - 마케팅 동의창 인트로통신사용시 - 확정안됨
	@IBAction func confirmButtonClickAction(_ sender: Any) {
		if (confirmLinkUrl != nil && confirmLinkUrl.count > 0) {
			sendConfirmOrCancel("Y")
		} else {
			finishEvent()
		}
	}
	
	func sendConfirmOrCancel(_ YesOrNo: String) {
		let keyChainWrapper = ApplicationShare.shared.keychainWrapper
		let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
		
//		//OJKIM TEST START - 마케팅동의창 띄우가 intro통신으로 받아서 하는 것 테스트
//do {
//var data = "{\"msg\":\"테스트를 위해서 띄우는 메시지\"}"
//let json = try JSON(data.data(using: String.Encoding.utf8) )
//self.eventPopupView.isHidden = true
//self.confirmPopupView.isHidden = true
//self.systemPopupView.isHidden = false
//self.titleLable.text = "푸시알림수신동의"
//self.messageLabel.text = json["msg"].stringValue
//print(json["msg"].stringValue)
//self.messageLabel.textAlignment = .center
//self.isFromAgreeFlag = true
//} catch {
//}
//return	
//		//OJKIM TEST END


		var params = Parameters()
		params["agreeFlag"] = YesOrNo
		if self.user_no == nil {
			params["user_no"] = custItem.user_no!
		} else {
			params["user_no"] = self.user_no
		}
		NetworkInterface.defaultManager.request(confirmLinkUrl,
												method: HTTPMethod.post,
												parameters: params,
												encoding: URLEncoding.default,
												headers: nil).responseData { ( response ) in
													if response.data != nil {
														do {
															print(String(data: response.data!, encoding: String.Encoding.utf8) as Any)
															let json = try JSON(data: response.data!)
															if (json["msg"].isEmpty != false) {
																self.eventPopupView.isHidden = true
																self.confirmPopupView.isHidden = true
																self.systemPopupView.isHidden = false
																self.titleLable.text = "푸시알림수신동의"
																self.messageLabel.text = json["msg"].stringValue
																self.messageLabel.textAlignment = .center
																self.isFromAgreeFlag = true
                                                                if self.todayCheckView != nil {
                                                                    self.todayCheckView.isHidden = true
                                                                }
															} else {
																self.finishEvent()
															}
														} catch {
															self.finishEvent()
														}
													} else {
														self.finishEvent()
													}
		}
	}
	
	@IBAction func cancelButtonClickAction(_ sender: Any) {
        if (confirmLinkUrl != nil && confirmLinkUrl.count > 0) {
			sendConfirmOrCancel("N")
		} else {
			finishEvent()
		}
	}
//OJKIM ADDED END
	
    @IBAction func notShowButtonClickAction(_ sender: Any) {
        
        
        if let  dictionary =  defaults.object(forKey: CerealConstrants.NSUserDefaultsKeys.eventReadFinished) as? [String:String] {
            
            var userDictionary = dictionary
            userDictionary[String(currentEventId)] = Date.todayString()
            
            defaults.set(userDictionary,forKey: CerealConstrants.NSUserDefaultsKeys.eventReadFinished)
            defaults.synchronize()
            
        }
        
        
        nextDisplayJSON()
        
    }
    

    @IBAction func imgaeLinkClickAction(_ sender: Any) {
        
        if let delegate = delegate {
            delegate.systemPopupViewControllerDidFinished(self, action: .urlClose(currentLinkeUrl))
        }
    }
}
