//
//  Permissions.swift
//  cereal
//
//  Created by srkang on 2018. 7. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications


// 권한 요청 리소스 타입 : 연락처,  푸시, 사진, 카메라 타입
public enum PermissionType: Int, CustomStringConvertible {
    case contacts, notifications, camera, photos
    
    public var prettyDescription: String {
        switch self {
        case .contacts:         return "연락처"
        case .notifications:    return "알림(PUSH)"
        case .camera:           return "카메라"
        case .photos:           return "사진앨범"
        }
    }
    
    public var description: String {
        switch self {
        case .contacts:         return "contacts"
        case .notifications:    return "Notifications"
        case .camera:           return "Camera"
        case .photos:           return "Photos"
        }
    }
    
    static let allValues = [contacts,  notifications,  camera, photos]
}



// 권한 상태 값
enum PermissionStatus: Int, CustomStringConvertible {
    case authorized, unauthorized, unknown, disabled , error
    
    public var description: String {
        switch self {
        case .authorized:   return "Authorized" // 승인 granted
        case .unauthorized: return "Unauthorized" // 사용자 거절
        case .unknown:      return "Unknown" // 승인인지, 거절인지 모름. 앱 최초 설치시
        case .disabled:     return "Disabled" // 시스템 레벨
        case .error:        return "error" // 권한 질의 중 에라
        }
    }
}


// 권한 타입, 상태값을 PermissionResult 로 표현
struct   PermissionResult : CustomStringConvertible {
    public let type: PermissionType
    public let status: PermissionStatus
    
    init(type:PermissionType, status:PermissionStatus) {
        self.type   = type
        self.status = status
    }
    
    public var description: String {
        return "\(type) \(status)"
    }
}

// 권한 Permission 프로토콜 정의, type 은 카메라,사진,푸시,연락처
protocol Permission {
    /// Permission type
    var type: PermissionType { get }
}

typealias requestPermissionUnknownResult = () -> Void
typealias requestPermissionShowAlert     = (PermissionType) -> Void


// 푸시 권한
public class NotificationsPermission: NSObject, Permission {
    public let type: PermissionType = .notifications
    
    var uiNotificationCategories: Set<UIUserNotificationCategory>?
    
    private var unNotificationCategoriesIOS10 : AnyObject?
    
    
    // iOS10 이상 일  경우 UNNotificationCategory 을 사용
    @available(iOS 10, *)
    var unNotificationCategories: Set<UNNotificationCategory>? {
        get {
            return unNotificationCategoriesIOS10 as? Set<UNNotificationCategory>
        }
        set {
            unNotificationCategoriesIOS10 = unNotificationCategories as AnyObject
        }
    }
   
    // iOS9 이하 일  경우 UIUserNotificationCategory 을 사용
    public init(uiNotificationCategories: Set<UIUserNotificationCategory>? = nil) {
        super.init()
        self.uiNotificationCategories = uiNotificationCategories
    }
    
   // iOS10 이상 일  경우 UNNotificationCategory 을 사용
    @available(iOS 10, *)
    public init(unNotificationCategories: Set<UNNotificationCategory>? = nil) {
        super.init()
        self.unNotificationCategories = unNotificationCategories
    }
    
}

// 연락처 권한
public class ContactsPermission: NSObject, Permission {
    public let type: PermissionType = .contacts
}


// 카메라  권한
public class CameraPermission: NSObject, Permission {
    public let type: PermissionType = .camera
}

// 사진  권한
public class PhotosPermission: NSObject, Permission {
    public let type: PermissionType = .photos
}

