//
//  ApplicationShare.swift
//  cereal
//
//  Created by srkang on 2018. 7. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper


class ApplicationShare {
    
    static var timeInterval : TimeInterval?
    
    static var isConnected = true // 네트워크 접속 여부
    
    static var mainEntered = false // 메인 화면 까지 진입 했는지 
    
    static var cerealAppLinkLaunch = false // App2App 통해서 Launch 되는지 여부
    static var cerealAppLinkURLString : String? // App2App 호출시 파라미터 URL
    static var cerealPushLinkURLString : String? // Push 통해 호출시 파라미터 URL
    
    static var isCerealMemberJoin: Bool? // 가이드에서 회원가입
    
    static var cerealForgroundAppLinkURLString : String? // Forground 상태에서  App2App 통해서 전달 받은 URL 파라미터 정보
    static var cerealForgroundPushLinkURLString : String? // Forground 상태에서  Push  통해서 전달 받은 URL 파라미터 정보
    

    static let shared = ApplicationShare()
    
    // 리소스 권한 정보
    class PermissionInfo {
        var permissionScope : PermissionScope!
    }
    
    // 디바이스 top,bottom 높이 정보
    class DeviceInfo {
        var topLength : CGFloat = 0.0
        var bottomLength : CGFloat = 0.0
    }
    
    // 로그인 정보
    class LoginInfo {
        
        var isLogin = false // 로그인 여부,  로그인된 상태 :true, 로그아웃 상태 : false
        
        var user_level : String? // 사용자 레벨 등급
        var push_count : String? // 푸시 카운트
        
        var nickname : String? // 닉네임
        var simul_nm : String? // 투자 성향명
        
        var balance : String? // 보유 충전금
        var asset_amt : String?// 총자산
        
        var user_img_url : String?// 프로필 이미지 URL
        
        var badge_list : Array<Dictionary<String,Any>> = [] // 미션 달성 뱃지 리스트
        
        var push_yn : String?
        
        
        // 로그아웃 하면, 로그인 관련된 정보를 rest 시킨다.
        func resetLogout() {
            
            isLogin = false
            user_level = nil
            simul_nm = nil
            nickname = nil
            badge_list = []
            balance = nil
            asset_amt = nil
            user_img_url = nil
        }

//      user_no, Last_login_time, token 은 KeyChain 에 저장
     
//        var user_no : String?
//        var Last_login_time : String?
//        var token : String?
    }
    
   
    // 디바이스 스크린 사이즈를 small,middle 인지 체크 
    class ScreenSize {
        var smallScreen = false;
        var middleScreen = false;
    }
    
    
    // 키체인에 저장 Wrapper
    var keychainWrapper =  KeychainWrapper(serviceName: Configuration.bundleIdentifier!, accessGroup: nil)
    
    var permissionInfo  = PermissionInfo()
    var screenSize      = ScreenSize()
    var loginInfo       = LoginInfo()
    var deviceInfo      = DeviceInfo()
    
    private init() {
        
    }
    
}
