//
//  CerealMainWebViewController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

@objc protocol CerealMainWebViewControllerDelegate  {
    @objc optional func okSubWebViewController(_ : UIViewController ,callback : [String:Any]? )
}

class CerealMainWebViewController: UIViewController {
    
//    let switchCase = 3
    
    // 메인 처음 시작 URL
    var  defaultURLString : String  {
        return "main.do"
//        return "https://blooming-shelf-99889.herokuapp.com"
        
//         return "http://localhost:5000"
    }
    
    var webView : WKWebView! // 웹뷰
    
    // iOS WKWebView 웹뷰 하이브리드 인터페이스명
    let SCRIPT_HANDLER_NAME = "hybridscript"
    
    // 현재 하이브리드 인터페이스
    var hybridInterface : HybridInterface?
    // 현재 하이브리드 인터페이스 중 웹뷰 인터페이스
    // COMMAND_CALL_HYBRID_POPUP 일때, 새로운 웹뷰창 ( CerealMainWebViewController 와 동일한 Instance 를 띄움)
    // Hybrid 인터페이스 콜 주체가 2개 이기 때문에, 2개간의 처리를 하기 위해서
    var parentWebViewInterface : WebViewInterface?
    
    var isMaster = true
    var isMasterEvent = false
    
    var isFirstAlert = false
    
    var webViewHud : MBProgressHUD?
    var webViewHudTimer : Timer?
    var webViewHudTimerInterval : TimeInterval = 3
    
    var firstChain : Chain?
    var subParameterRequest : URLRequest?
	
	//OJKIM ADDED START - 당겼다나 놓으면 페이지 리프레시처리
	var refreshControl: UIRefreshControl!
	//OJKIM ADDED END
    
    // Child 창
    // COMMAND_CALL_HYBRID_POPUP 일때,  모달창의 파라미터
    var subParameters : [ String : Any ]? {
        didSet {
            
            if let subParameters = subParameters {
                
                let urlString = subParameters["url"] as! String
                var url : URL!
                // http 포함한 full  URL 일 경우  serverDomainUse 을 false 로 해서, 전체 경로를 사용함.
                if urlString.hasPrefix("http") {
                    url = NetworkInterface.buildURL(serverDomainUse: false, appendString: urlString)
                } else {
                    //  urlString 에 "http" 문자가 포함되지  URL 일 경우  serverDomainUse 을 true 로 해서, NetworkInterface 의 SERVER_URL 기준으로 url 조합
                    url = NetworkInterface.buildURL(serverDomainUse: true, appendString: urlString)
                    url = URL(string: NetworkInterface.SERVER_URL)
                    url.appendPathComponent(urlString)
                }
                
                
                var  urlRequest = URLRequest(url: url)
                
                if let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL )!) {
                    var cookieHeaders = HTTPCookie.requestHeaderFields(with: cookies)
                    if let value = cookieHeaders["Cookie"] {
                        urlRequest.setValue(value, forHTTPHeaderField: "Cookie")
                    }
                }
                    
                
                if let webView = self.webView {
                    webView.load(urlRequest)
                } else {
                    subParameterRequest = urlRequest
                }
            }// if subParameters
        } //didSet
    }//subParameters
    
    var delegate : CerealMainWebViewControllerDelegate?
    
    var urlString:String! {
        didSet {
            
            var url : URL!
            if urlString.hasPrefix("http") {
                url = NetworkInterface.buildURL(serverDomainUse: false, appendString: urlString)
            } else {
                url = NetworkInterface.buildURL(serverDomainUse: true, appendString: urlString)
            }
            
            var  urlRequest = URLRequest(url: url)
            
            // 쿠키 정보 셋팅
            
            if let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL )!) {
                
                var cookieHeaders = HTTPCookie.requestHeaderFields(with: cookies)
                
                if let value = cookieHeaders["Cookie"] {
                    //urlRequest.addValue(value, forHTTPHeaderField: "Cookie")
                    urlRequest.setValue(value, forHTTPHeaderField: "Cookie")
                    
                    log?.debug("cookie value:\(value)")
                }
            }
            
            delay(0.1){
                self.webView.load(urlRequest)
            }

            
        }
    }
    
    @objc override func mainWebViewChangeWebUrl(_ urlString:String) {
        self.urlString = urlString
        
    }
    
    var subWebViewCallBack : ( ()->Void)?
    

    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    func didEnterBackground() {
        self.webView.evaluateJavaScript("onPause()") { ( anyData , error) in
            log?.debug("onPause anyData:\(anyData as Any)")
            log?.debug("onPause error:\(error as Any)")
        }
    }
    
    func willEnterForeground() {
        self.webView.evaluateJavaScript("onResume();") { ( anyData , error) in
            log?.debug("onResume anyData:\(anyData as Any)")
            log?.debug("onResume error:\(error as Any)")
        }
    }
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        ApplicationShare.mainEntered = true
        
        self.slidingViewController()?.mainNavigationController = self.navigationController
        
        self.navigationController?.view.addGestureRecognizer((self.slidingViewController()?.screenEdgePanGesture)!)
        
        self.slidingViewController()?.screenEdgePanGesture.delegate = self
        
        let contentController = WKUserContentController()
        contentController.removeAllUserScripts()
        
        //let cookies = HTTPCookieStorage.shared.cookies
        let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL)!)
        let script = getJSCookiesString(cookies: cookies)
        
        let cookieScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: false)
        contentController.addUserScript(cookieScript)
//
        
//        let cookieScript2 = WKUserScript(source: "cookieScript('aaa');", injectionTime: .atDocumentEnd, forMainFrameOnly: false)
//        contentController.addUserScript(cookieScript2)
        
//
        let updateScript = WKUserScript(source: "window.webkit.messageHandlers.updateCookies.postMessage(document.cookie);", injectionTime: .atDocumentStart, forMainFrameOnly: false)
        contentController.addUserScript(updateScript)
        
        
        contentController.add(OKScriptMessageHandler(delegate: self), name: SCRIPT_HANDLER_NAME)
        contentController.add(OKScriptMessageHandler(delegate: self), name: "updateCookies")
        
        
        let config = WKWebViewConfiguration()
        //config.preferences.javaScriptCanOpenWindowsAutomatically = true;
        config.userContentController = contentController
        
        
        
        
        webView = WKWebView(frame: CGRect.zero, configuration: config)
        webView.backgroundColor = UIColor.red
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.scrollView.showsHorizontalScrollIndicator = false
        
        if let slidingViewController = self.slidingViewController() {
            webView.scrollView.panGestureRecognizer.require(toFail: (slidingViewController.screenEdgePanGesture))
        }
        
        
        view.addSubview(webView)

		//OJKIM ADDED START - 당겼다나 놓으면 페이지 리프레시처리
		refreshControl = UIRefreshControl()
		refreshControl.bounds = CGRect(refreshControl.bounds.origin.x,
									   30,
										refreshControl.bounds.size.width,
										refreshControl.bounds.size.height)
		refreshControl.addTarget(self, action:#selector(CerealMainWebViewController.refresh(_:)),
								 for: UIControlEvents.valueChanged)
		webView.scrollView.addSubview(refreshControl)
		//OJKIM ADDED END
		 
        webView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        
        if #available(iOS 11, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.view.backgroundColor = UIColor.white
        
        self.webView.isOpaque = false
        self.webView.backgroundColor = UIColor.clear
        self.webView.scrollView.backgroundColor = UIColor.clear

        
        if isMaster {
            
            // Main WebView 에 대해서 cerealPushLinkURLString, cerealAppLinkURLString ,isCerealMemberJoin 를 처리 한다.
            // 즉 isMaster 아닌 경우는  모달팝업 웹뷰 이므로, 처리 하지 않음.
            if let cerealPushLinkURLString =  ApplicationShare.cerealPushLinkURLString {
                urlString = cerealPushLinkURLString
                ApplicationShare.cerealPushLinkURLString = nil
            } else if let cerealAppLinkURLString =  ApplicationShare.cerealAppLinkURLString {
                urlString = cerealAppLinkURLString
                ApplicationShare.cerealAppLinkURLString = nil
            } else if  let isCerealMemberJoin = ApplicationShare.isCerealMemberJoin , isCerealMemberJoin == true {
                urlString = MenuInfo.menuURL(from: .memIntro)
                ApplicationShare.isCerealMemberJoin = false
            }else {
                urlString = defaultURLString
            }
            
            registerChain()
        }
        
        
        Notification.Name.ActionLogin.addObserver(self, selector: #selector(actionLogin(_:)))
        Notification.Name.ActionLogout.addObserver(self, selector: #selector(actionLogout(_:)))
        Notification.Name.ForgroundPushLink.addObserver(self, selector: #selector(forgroundPushLink(_:)))
        Notification.Name.ForgroundURLLink.addObserver(self, selector: #selector(forgroundUrlLink(_:)))
        Notification.Name.MenuSlide.addObserver(self, selector: #selector(menuSlide(_:)))
        Notification.Name.UIApplicationWillResignActive.addObserver(self, selector: #selector(backgroundEnter(_:)))        
    }
	
	//ojk
	@objc func refresh(_ refreshControl: UIRefreshControl) {
		refreshControl.endRefreshing()
		webView.reload()
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // MainWwebView 일때
        if isMaster {
            
            if !isMasterEvent {
                isMasterEvent = true
                processEvent()
            }
            
            processChain()
        }
        else {
            // callHybridPopup 인터페이스 통해서 자식창으로 띄워져 있을때
            if let subParameterRequest = subParameterRequest {
                webView.load(subParameterRequest)
                self.subParameterRequest = nil
            }
        }
    }
    
    deinit {
        log?.debug("WKEBViewController deinit")
        
        let ucc = self.webView.configuration.userContentController
        ucc.removeAllUserScripts()
        ucc.removeScriptMessageHandler(forName:SCRIPT_HANDLER_NAME)
        
    }
    
    func processEvent() {
        
        if let eventJSON = MenuInfo.filterEventJSON  {
            
            if !eventJSON.arrayValue.isEmpty {
                let popupViewController = UIStoryboard.storyboard(.main).instantiateViewController() as SystemPopupViewController
                popupViewController.modalPresentationStyle = .overFullScreen
                popupViewController.delegate = self
                present(popupViewController, animated: true, completion: nil)
            }
        }
    }
    
    
    @objc func actionLogin(_ notification:Notification) {
        
        let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL )!)
        let script = getJSCookiesString(cookies: cookies)
        
        
//        self.webView.evaluateJavaScript(script) { ( anyData , error) in
//
//            log?.debug("anyData:\(anyData as Any)")
//            log?.debug("error:\(error as Any)")
//
//            log?.debug("script:\(script)")
        
            
            if let userInfo = notification.userInfo {
                if let webURL = userInfo["webURL"] {
                    delay(0.1) {
                        self.mainWebViewChangeWebUrl(webURL as! String)
                    }
                    
                } else if let hybridShowLogin = userInfo["hybridShowLogin"] {
                    log?.debug("hybridShowLogin 통해 온 경우 ")
                    
                    self.webView.evaluateJavaScript(script) { (anyData, error) in
                        log?.debug("anyData\(anyData as Any)")
                        log?.debug("error:\(error as Any)")

                        log?.debug("script:\(script)")
                    }
                }
                
            } else {
                // userInfo 없는 경우는,  Native 메뉴에서 사용자가 로그인 버튼을 명시적으로 눌러서 , 로그인 하는 케이스, 현재페이지 reload 한다.
                //self.webView.reload()
                delay(0.1) {
                    self.mainWebViewChangeWebUrl(self.webView.url!.absoluteString as! String)
                }
                
                
                
            }
//        } // self.webView.evaluateJavaScript
        
        
        
    }
    
    @objc func actionLogout(_ notification:Notification) {
        
        let url = MenuInfo.menuURL(from: .main)!
        
        urlString = url
        
    }
    
    // Push Launch
    @objc func forgroundPushLink(_ notification:Notification) {
        
        if let cerealForgroundPushLinkURLString = ApplicationShare.cerealForgroundPushLinkURLString {
            
            if let slidingViewController = slidingViewController() {
                slidingViewController.menuHide(true)
            }
            
            if let _  = presentedViewController {
                dismiss(animated: false) {
                    self.urlString = cerealForgroundPushLinkURLString
                    
                    ApplicationShare.cerealForgroundPushLinkURLString = nil
                }
            } else {
                self.urlString = cerealForgroundPushLinkURLString
                
                ApplicationShare.cerealForgroundPushLinkURLString = nil
            }
        }
        
    }
    
    // App2App  OpenURL
    @objc func forgroundUrlLink(_ notification:Notification) {
        
        if let cerealForgroundAppLinkURLString = ApplicationShare.cerealForgroundAppLinkURLString {
            
            if let slidingViewController = slidingViewController() {
                slidingViewController.menuHide(true)
            }
            
            if let _  = presentedViewController {
                dismiss(animated: false) {
                    self.urlString = cerealForgroundAppLinkURLString
                    
                    ApplicationShare.cerealForgroundAppLinkURLString = nil
                }
            } else {
                self.urlString = cerealForgroundAppLinkURLString
                
                ApplicationShare.cerealForgroundAppLinkURLString = nil
            }
        }
        
    }
    
    @objc func menuSlide(_ notification:Notification) {
        hideHudView()
    }
    
    
    @objc func backgroundEnter(_ notification:Notification) {
       hideHudView()
    }
    
    
    func registerChain() {
    }
    
    func processChain() {
        
        if isMaster {
            firstChain?.doJob()
        }
        
    }
    
    @objc func refreshButtonAction(_ sender : Any) {
        captureAction(self)
    }
    
    
    // Cookie 스트링 뽑기
    // WKWebView 에서 Javascript inject  통해서 session 유지 목적  :  AJAX 에서는 자동으로 Cookie 안 해주므로
    func getJSCookiesString(cookies : [HTTPCookie]? ) -> String {
        
        var result = ""
        
        
     
//
//        if let cookies = cookies {
//            for cookie in cookies {
//                var tempResult = "document.cookie='"
//                tempResult += "\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path);"
//
//                if let expiresDate = cookie.expiresDate {
//                    log?.debug("cookie.expiresDate : \(expiresDate)")
//                }
//
//                if cookie.isSecure {
//                    tempResult += "secure;"
//                }
//
//                tempResult += "';"
//
//                result += tempResult
//            }
//        }
        var source = String()
        cookies?.forEach { cookie in
            source.append("document.cookie = '")
            source.append("\(cookie.name)=\(cookie.value); path=\(cookie.path); domain=\(cookie.domain);'\n")
        }
        
        log?.debug("getJSCookiesString:\(source)")
        
        return source
        
    }
    
    
    // 테스트용
    @IBAction func captureAction(_ sender : Any) {
        log?.debug("captureAction called")
        
        captureWebView()
    }
    
    // 테스트용
    func captureWebView() {
        let currentSize = webView.frame.size
        let currentOffset = webView.scrollView.contentOffset
        
        webView.frame.size = webView.scrollView.contentSize
        webView.scrollView.setContentOffset(CGPoint.zero, animated: false)
        
        let rect = CGRect(x: 0, y: 0, width: webView.bounds.size.width, height: webView.bounds.size.height)
        
        
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        // iOS7
        webView.drawHierarchy(in: rect, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        webView.frame.size = currentSize
        webView.scrollView.setContentOffset(currentOffset, animated: false)
        
        
        guard image != nil else {
            return
        }
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)),nil)
    }
    
    // 테스트용
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    
    var createWebView: WKWebView!


    // 테스트용
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        self.slidingViewController()?.menuShow(true) {
            log?.debug("menuButtonTapped completed called")
        }
    }
    
    
    // Loading 바 보여주기, 감추기
    func showHudView() {
        
        if  webViewHud == nil {
            webViewHud = MBProgressHUD.showCerealCHUDAdded(to: self.view, animated: true)
        }
        
        
        //if let webViewHudTimer = webViewHudTimer {
        //    webViewHudTimer.invalidate()
        //}
        
        //webViewHudTimer = Timer.scheduledTimer(timeInterval: webViewHudTimerInterval, target: self, selector: #selector(timerTrigger), userInfo: nil, repeats: false)
 
    }
    
    @objc  func timerTrigger () {
       hideHudView()
    }
    
    // Hybrid 에서 실수 또는 네트워크 오류로  Loading 바 숨기기 안 되는것 방지 하기 위해서, webViewHudTimerInterval 이상 지나면 자동으로 닫게 한다.
    func hideHudView() {
        
        if let webViewHudTimer = webViewHudTimer {
            webViewHudTimer.invalidate()
        }
        
        webViewHud?.hide(animated: false)
        webViewHud = nil
    }
    
}


//MARK: - WKScriptMessageHandler  하이브리드 스크립트

extension CerealMainWebViewController : WKScriptMessageHandler {
    
    
    // WKWebView 자바스크립트 인터페이스
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        log?.debug("wkwebview run javascript name = \(message.name) body = \(message.body)")
        log?.debug(type(of: message.body))
        
       /*
        let cookies = message.body
        NSArray<NSString *> *cookies = [message.body componentsSeparatedByString:@"; "];
        for (NSString *cookie in cookies) {
            // Get this cookie's name and value
            NSArray<NSString *> *comps = [cookie componentsSeparatedByString:@"="];
            if (comps.count < 2) {
                continue;
            }
            
            // Get the cookie in shared storage with that name
            NSHTTPCookie *localCookie = nil;
            for (NSHTTPCookie *c in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:self.wk_webView.URL]) {
                if ([c.name isEqualToString:comps[0]]) {
                    localCookie = c;
                    break;
                }
            }
            
            // If there is a cookie with a stale value, update it now.
            if (localCookie) {
                NSMutableDictionary *props = [localCookie.properties mutableCopy];
                props[NSHTTPCookieValue] = comps[1];
                NSHTTPCookie *updatedCookie = [NSHTTPCookie cookieWithProperties:props];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:updatedCookie];
            }
        }
        */
        
        if message.name == "updateCookies" {
            log?.debug("updateCookies:\(message.body)")
            
            
            let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL )!)
            let script = getJSCookiesString(cookies: cookies)
            
            
            self.webView.evaluateJavaScript(script) { ( anyData , error) in
    
                log?.debug("updateCookies anyData:\(anyData as Any)")
                log?.debug("updateCookies error:\(error as Any)")
    
                log?.debug("updateCookies script:\(script)")
            
            }
            
            return
        }
        
        
        
        // message 는 3개로 나누어짐. [0] : 콜백 배열 , CallbackArray , [1] : action명 , [2] : 파라미터 배열, Arguments Array
        if let objArray = message.body as? Array<Any> , objArray.count == 3 {
            // action명으로 Switch 구분
            let action = objArray[1] as! String
            switch action {
            case ShareActivityInterface.COMMAND_SHARE_SNS :
                    shareShow(objArray) // SNS 공유
            case SecurePadInterface.COMMAND_SECURE_KEY_SHOW :
                    keypadShow(objArray) // 보안키패드
            case SMSSendInterface.COMMAND_SEND_SMS :
                    sendSMS(objArray) // SMS 문자 공유
            case CameraPhotoInterface.COMMAND_CAPTURE_AND_SAVE_OMAGE ,
                 CameraPhotoInterface.COMMAND_GETIMAGE_FROM_GALLERY :
                    showCameraAndPhoto(objArray)
            case LoadingBarInterface.COMMAND_SHOW ,
                 LoadingBarInterface.COMMAND_HIDE :
                    showHideLoadingBar(objArray)
            case WebViewInterface.COMMAND_CALL_HYBRID_POPUP ,
                 WebViewInterface.COMMAND_CONFIRM_POPUP ,
                 WebViewInterface.COMMAND_RECEIVE_POPUP     ,
                 WebViewInterface.COMMAND_CLEAR_CACHE ,
                 WebViewInterface.COMMAND_CANCEL_POPUP ,
                 WebViewInterface.COMMAND_SHOW_WEBVIEW_POPUP,
//OJKIM ADDED START - 추가팝업창띄우기
				 WebViewInterface.COMMAND_HYBRID_KIND_POPUP,
//OJKIM ADDED END
//JJBAE ADDED START
                WebViewInterface.COMMAND_HYBRID_LEVEL_POPUP:
//JJBAE ADDED END
                    showWebView(objArray)
            case LoginInterface.COMMAND_CHECK_LOGIN ,
                 LoginInterface.COMMAND_GET_LOGIN_INFO ,
                 LoginInterface.COMMAND_CALL_LOGIN  ,
                 LoginInterface.COMMAND_AUTO_LOGIN_SETTING ,
                 LoginInterface.COMMAND_SET_LOGOUT ,
                 LoginInterface.COMMAND_SET_NICK_NAME ,
                 LoginInterface.COMMAND_SET_PROFILE_DEFAULT_IMAGE ,
                 LoginInterface.COMMAND_SET_ALARM_COUNT ,
                 LoginInterface.COMMAND_SAVE_TOKEN ,
                 LoginInterface.COMMAND_CALL_LOGIN_DIRECT :
                    loginCall(objArray)
            case UtilityInterface.COMMAND_SET_PREFERENCE    , // 네이트브 저장소에 저장
                 UtilityInterface.COMMAND_GET_PREFERENCE    , // 네이트브 저장소 부터 값 조회
                 UtilityInterface.COMMAND_OPEN_MENU         ,
                 UtilityInterface.COMMAND_GET_PICKER_INFO   ,
                 UtilityInterface.COMMAND_GET_DATE_PICKER   ,
                 UtilityInterface.COMMAND_UTIL_TEST         ,
                 UtilityInterface.COMMAND_SEND_GA_EVENT     ,
                 UtilityInterface.COMMAND_GET_STATUSBAR_HEIGHT ,
                 UtilityInterface.COMMAND_COPY_PASTEBOARD   ,
                 UtilityInterface.COMMAND_GET_OS_TYPE       , // OS
                 UtilityInterface.COMMAND_GET_DEVICE_NAME   , // 디바이스이름
                 UtilityInterface.COMMAND_GET_APP_VERSION   , // 앱 버전정보
                 UtilityInterface.COMMAND_GET_PUSH_TOKEN    , // 푸시 토큰 (FCM) 가져오기
                 UtilityInterface.COMMAND_GET_PUSH_SETTING  , // 푸시 설정 여부
                 UtilityInterface.COMMAND_MOVE_PUSH_SETTING , // 앱설정 이동 푸시제어 용도
                 UtilityInterface.COMMAND_INSTALLED_KAKAOTALK , // 카카오톡 설치 여부
                 UtilityInterface.COMMAND_APP_UPDATE        : // 앱 업데이트
                    utilityCall(objArray)
            case ContactInterface.COMMAND_GET_CONTACT_INFO :
                    showContact(objArray)
            case SNSInterface.COMMAND_SHARE_KAKAOTALK : // 카카오톡 링크 공유
                    showSNS(objArray)
            default : ()
            }
      
        }
        
    }
    
    // 콜백이 있을 경우 . 역으로 Native 에서 Javascript 콜백 호출 . cereal_hybrid.js callbackFromNative 메소트 통해서 콜백 호출
    func callback(callbackId: String, retMessage : Any , isJson : Bool = false ) {
        
        var retString = "iOSPluginJSNI.callbackFromNative('\(callbackId)'"
        
        if !isJson {
            if let retMessage = retMessage as? Array<String>  {
                
                for item in retMessage {
                    retString += ",'\(item)'"
                }
            } else if let retMessage = retMessage as? String  {
                retString += ",'\(retMessage)'"
            } else {
                retString += ",'\(retMessage)'"
            }
            
            retString += ")"
            
        }
        else {
            retString += ",\(retMessage))"
        }
        
        log?.debug("retString:\(retString)")
      
        
        webView.evaluateJavaScript(retString) { ( anyData , error) in
            log?.debug(anyData as Any)
            log?.debug(error as Any)
        }
    }
    
  
    
    // 로그인 관련 LoginInterface
    func loginCall(_ message:  Array<Any>  ) {
        
        hybridInterface = LoginInterface(viewController: self, command: message) { (hybridResult) in
            var callbackArray = message[0] as! Array<String>
            if callbackArray.count > 0 {
                switch(hybridResult) {
                case .success(let retMessage) :
                    self.callback(callbackId: callbackArray[0], retMessage: retMessage  )
                default:
                    ()
                }
            }
        }
    }
    
      // 유티리리티  관련 UtilityInterface
    func utilityCall(_ message:  Array<Any>  ) {
        
        
        let action = message[1] as! String
        
            
        hybridInterface = UtilityInterface(viewController: self, command: message) { (hybridResult) in
            var callbackArray = message[0] as! Array<String>
            if callbackArray.count > 0 {
                switch(hybridResult) {
                case .success(let retMessage) :
                    
                    if action == UtilityInterface.COMMAND_GET_APP_VERSION {
                        self.callback(callbackId: callbackArray[0], retMessage: retMessage , isJson: true )
                    }
                    else {
                        self.callback(callbackId: callbackArray[0], retMessage: retMessage  )
                    }
                    
                    
                default:
                    ()
                }
            }
        }
    }
    
    /// keypadShow( message )
    /// 보안키패드 보여주기
    /// - Parameters:
    ///   - message:  userContentController(_ , didReceive ) 함수에서 받은 메시지 message
    func keypadShow(_ message:  Array<Any>  ) {

        //2018. 11. 28 보안키패드 하단 영역에 소프트키보드 보여지는 현상 안보이도록 수정
        self.view.endEditing(true);
        
        hybridInterface = SecurePadInterface(viewController: self, command: message) { (hybridResult) in

            var callbackArray = message[0] as! Array<String>
            
            switch(hybridResult) {
            case .progress(let retMessage) :
                self.callback(callbackId: callbackArray[0], retMessage: retMessage as! String)
            case .success(let retMessage) :
                self.callback(callbackId: callbackArray[1], retMessage: retMessage as! String , isJson: true)
            case .fail(let error, let errorMessage) :
                self.callback(callbackId: callbackArray[2], retMessage: errorMessage as! String )
          
            default:
                ()
            }
        }
        
    }
    
    func showHideLoadingBar(_ message:  Array<Any>  ) {
        
        let action = message[1] as! String
        
        if action == LoadingBarInterface.COMMAND_SHOW {
            showHudView()
        } else if action == LoadingBarInterface.COMMAND_HIDE {
           hideHudView()
        }
        
    }
      // WebView   관련 WebViewInterface
    func showWebView(_ message:  Array<Any>  ) {
        
        if let objArray = message as? Array<Any>{
            
            let action = objArray[1] as! String
//JJBAE ADDED START
            if  action == WebViewInterface.COMMAND_HYBRID_LEVEL_POPUP {
                
                hybridInterface = WebViewInterface(viewController: self, command: message, result:   { (hybridResult) in
                    
                    var callbackArray = message[0] as! Array<String>
                    switch(hybridResult) {
                    case .success(let retMessage) :
                        self.callback(callbackId: callbackArray[0], retMessage: retMessage, isJson: true)
                    default:
                        ()
                    }
                })
            }
//JJBAE ADDED END
            else if  action == WebViewInterface.COMMAND_RECEIVE_POPUP {
                
                let byPass = subParameters!["jsonArg"]
                
                hybridInterface = WebViewInterface(viewController: self, command: message,  byPass:byPass! ,   result:   { (hybridResult) in
                    
                    var callbackArray = message[0] as! Array<String>
                    switch(hybridResult) {
                    case .success(let retMessage) :
                        self.callback(callbackId: callbackArray[0], retMessage: retMessage, isJson: true)
                    default:
                        ()
                    }
                })
            } else {
                hybridInterface = WebViewInterface(viewController: self, command: message, result:   { (hybridResult) in
                    
                    var callbackArray = message[0] as! Array<String>
                    switch(hybridResult) {
                    case .success(let retMessage) :
                        self.callback(callbackId: callbackArray[0], retMessage: retMessage, isJson: true)
                    default:
                        ()
                    }
                })
            }
            
        }
        
        // parentWebViewInterface  변수로 담기 위해
        if let objArray = message as? Array<Any>{
            
            let action = objArray[1] as! String
           
            if  action == WebViewInterface.COMMAND_CALL_HYBRID_POPUP {
                parentWebViewInterface = hybridInterface as! WebViewInterface
            }
        }
    }
    
    //  SNS 공유 관련 ShareActivityInterface  (  UIActivityViewController  통해 호출 )
    func shareShow(_ message:  Array<Any>  ) {
        
        hybridInterface = ShareActivityInterface(viewController: self, command: message, result:   { (hybridResult) in
            log?.debug(self)
            log?.debug(hybridResult)
        })
    }
    
    // App2App SNS 카카오톡 SNSInterface
    func showSNS(_ message:  Array<Any>  ) {
        
        hybridInterface = SNSInterface(viewController: self, command: message, result:   { (hybridResult) in
            log?.debug(self)
            log?.debug(hybridResult)
        })
    }
    
    // 카메라 ,사진 관련  CameraPhotoInterface
    func showCameraAndPhoto(_ message:  Array<Any>  ) {
        
        hybridInterface = CameraPhotoInterface(viewController: self, command: message, result:   { (hybridResult) in
            log?.debug(self)
            log?.debug(hybridResult)
            
            switch(hybridResult) {
            case .success(let retMessage) :
                var callbackArray = message[0] as! Array<String>
                self.callback(callbackId: callbackArray[0], retMessage: retMessage)
            default:
                ()
                
            }
        })
    }
    
    
    
    
    // SMS 문자 보내기   SMSSendInterface
    func sendSMS(_ message:  Array<Any>  ) {
        
        hybridInterface = SMSSendInterface(viewController: self, command: message, result: { (hybridResult) in
            log?.debug(hybridResult)
        })
    }
    
    // 연락처    ContactInterface
    func showContact(_ message:  Array<Any>  ) {
        
        var callbackArray = message[0] as! Array<String>
        
        hybridInterface = ContactInterface(viewController: self, command: message, result: { (hybridResult) in
            
            switch(hybridResult) {
            case .success(let retMessage) :
                self.callback(callbackId: callbackArray[0], retMessage: retMessage)
            default:
                ()
                
            }
            
        })
    }
    
    
    
}


// MARK://WKUIDelegate  alert,confirm 제어

extension CerealMainWebViewController : WKUIDelegate {
    
    
    func webView(_ webView: WKWebView,
                 runJavaScriptAlertPanelWithMessage message: String,
                 initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        let host = frame.request.url?.host
        let alert = DOAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(DOAlertAction(title: "확인", style: .default) { _ in
            completionHandler()
        })
        self.present(alert, animated:true)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        let alertController = DOAlertController(title: "", message: message, preferredStyle: .alert)
    
        alertController.addAction(DOAlertAction(title: "취소", style: .cancel, handler: { (action) in
            completionHandler(false)
        }))
        
        alertController.addAction(DOAlertAction(title: "확인", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "취소", style: .cancel, handler: { (action) in
            completionHandler(nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        log?.debug("no thanks, I'd rather not")
     
        
        self.webView.load(navigationAction.request)
        return nil
    }
    
 
}

// MARK:// WKNavigationDelegate
extension CerealMainWebViewController : WKNavigationDelegate {
    
    /*
        WebView 의 ShouldStart 동일
     */
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        log?.debug("decidePolicyFornavigationAction navigationType: \(navigationAction.navigationType)")
        //log?.debug("decidePolicyFornavigationAction url: \(String(describing: webView.url))")
        
        //log?.debug("navigationAction.targetFrame : \(String(describing: navigationAction.targetFrame))")
        //log?.debug("decidePolicyFornavigationAction navigationAction.request.url: \(String(describing: navigationAction.request.url))")

        
        //let cookies = HTTPCookieStorage.shared.cookies(for: URL(string: NetworkInterface.SERVER_URL )!)
        //let script = getJSCookiesString(cookies: cookies)
        //log?.debug("decidePolicyFornavigationAction script: \(script)")
        
        if let absoluteString = navigationAction.request.url?.absoluteString,absoluteString.contains("postcode.do"){
            log?.debug("abc")
            decisionHandler(.allow)
            return
        }else{
            
            if navigationAction.navigationType != .linkActivated {
                if let absoluteString = navigationAction.request.url?.absoluteString,absoluteString.contains("https://web.facebook.com/"){
                    log?.debug("def")
                    hideHudView()
                    decisionHandler(.allow)
                    
                    return
                }else{
					log?.debug("ghi")
					//OJKIM DELETED START - 주소검색시 로딩뷰 사라지지 않는 문제 수정
					//showHudView()
					//OJKIM DELETED END
                }
                
            } //else if let absoluteString = navigationAction.request.url?.absoluteString , absoluteString.hasSuffix("#") == false {
            else if let absoluteString = navigationAction.request.url?.absoluteString {
            
            // let range = absoluteString.range(of: "#") ,  range.isEmpty == false {
                if let range = absoluteString.range(of: "#") {
                    if range.isEmpty {
                        log?.debug("jkl")
                        showHudView()
                    }
                } else {
                    log?.debug("mno")
                    showHudView()
                }
            
            } else {
                log?.debug("pqr")
                log?.debug("webViewHud not called")
            }
        
    //
            if isMaster {
    //            self.webView.evaluateJavaScript(script) { (anyData, error) in
    //                log?.debug(anyData as Any)
    //                log?.debug(error as Any)
    //            }
            }

            decisionHandler(.allow)
        }
    }
    
    /*
     Decides whether to allow or cancel a navigation after its response is known.
     Your delegate can either call the block immediately or save the block and call it asynchronously at a later time.
    */
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
//        log?.debug("decidePolicyFornavigationResponse : \(navigationResponse)")
//
//        // 2018.08.27 주석 처리
//        if let urlResponse = navigationResponse.response as? HTTPURLResponse,
//            let url = urlResponse.url,
//            let allHeaderFields = urlResponse.allHeaderFields as? [String : String] {
//            let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: url)
//            HTTPCookieStorage.shared.setCookies(cookies , for: urlResponse.url!, mainDocumentURL: nil)
//
//        }
//
//        decisionHandler(.allow)
        
        
        //https://code.i-harness.com/ko-kr/q/1957951
//        guard
//            let response = navigationResponse.response as? HTTPURLResponse,
//            let url = navigationResponse.response.url
//            else {
//                decisionHandler(.cancel)
//                return
//        }
//
//        if let headerFields = response.allHeaderFields as? [String: String] {
//            let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
//            cookies.forEach { (cookie) in
//                HTTPCookieStorage.shared.setCookie(cookie)
//            }
//        }
        
        decisionHandler(.allow)
        
    }

    // Called when web content begins to load in a web view.
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        log?.debug("didStartProvisionalNavigation : \(navigation)")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        log?.debug("didReceiveServerRedirectForProvisionalNavigation : \(navigation)")
    }
 
    /*
    Called when an error occurs while the web view is loading content.
    */
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        // KB 에서는 1001-1009 처리함.
        //  Code=-1002 "unsupported URL" 
        log?.debug("didFailProvisionalNavigation: \(navigation) error: \(error)");
        
        hideHudView()
    }
    
    
    /*
     Called when the web view begins to receive web content.
    */
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        log?.debug("didCommitNavigation \(navigation) ")
    }
    
    // Called when the navigation is complete.
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        log?.debug("didFinishNavigation \(navigation) ")
        
        hideHudView()
		
//OJKIM TEST START - 추가팝업창띄우기테스트
//if (webView.url!.absoluteString == "https://m.mycereal.co.kr:8443/matcs/inv/invSubDetail.do") {
//	var testScript = "CerealPluginObj.prototype.callHybridKindPopup = function (url, param, callback) {"
//	testScript.append("if(OS_TYPE===OS_AND){")
//	testScript.append("CerealPluginJSNI.callHybridKindPopup(url, param, CerealRegisterCallback(callback));")
//	testScript.append("}else if(OS_TYPE===OS_IOS){")
//	testScript.append("var callbackArray = [callback];")
//	testScript.append("var args = [url, param];")
//	testScript.append("iOSPluginJSNI.exec(callbackArray, 'callHybridKindPopup', args)")
//	testScript.append("}")
//	testScript.append("};")
//	testScript.append("function testCallback() {")
//	testScript.append("alert('callback callback');")
//	testScript.append("}")
//	self.webView.evaluateJavaScript(testScript) { (anyData, Error) in
//		var ts = ""
//		ts.append("function callHybridKindpopup(url, params, callback) {\n")
//		ts.append("    CerealPlugin.callHybridKindPopup(url, params, callback);\n")
//		ts.append("}\n")
//		ts.append("var tParam = {};\n")
//		ts.append("var returnParameter = {};\n")
//		ts.append("returnParameter.title = \"상품설명\";\n")
//		ts.append("tParam.parameter = returnParameter;\n")
//		ts.append("tParam.titleType = \"1\";\n")
//		ts.append("tParam.callType = \"3\";\n")
//		ts.append("callHybridKindpopup(\"http://www.daum.net\", JSON.stringify(tParam));\n")
////		"CerealPlugin.callHybridKindPopup(\"http://www.daum.net\", '{\"titleType\":\"0\",\"parameter\":{\"title\":\"TestTitle\"},\"callType\":\"2\"}', \"testCallback\")"
//		self.webView.evaluateJavaScript(ts) { (anyData, Error) in
//		}
//	}
//}
        if (webView.url!.absoluteString == "https://dm.mycereal.co.kr:8443/matcs/inv/invSubDetail.do") {
            var testScript = "CerealPluginObj.prototype.callHybridKindPopup = function (url, param, callback) {"
            testScript.append("if(OS_TYPE===OS_AND){")
            testScript.append("CerealPluginJSNI.callHybridKindPopup(url, param, CerealRegisterCallback(callback));")
            testScript.append("}else if(OS_TYPE===OS_IOS){")
            testScript.append("var callbackArray = [callback];")
            testScript.append("var args = [url, param];")
            testScript.append("iOSPluginJSNI.exec(callbackArray, 'callHybridKindPopup', args)")
            testScript.append("}")
            testScript.append("};")
            testScript.append("function testCallback() {")
            testScript.append("alert('callback callback');")
            testScript.append("}")
            self.webView.evaluateJavaScript(testScript) { (anyData, Error) in
                var ts = ""
                ts.append("var message = \"Lv.66가 되셨습니다.\\n축하합니다.\";\n")
                ts.append("var level = \"66\";\n")
                ts.append("var colour = 6;\n")
                ts.append("var args = [message,level,colour];\n"); ts.append("iOSPluginJSNI.exec(null,'showHybridLevelPopup',args);\n")
                self.webView.evaluateJavaScript(ts) { (anyData, Error) in
                }
            }
        }
//OJKIM TEST END
    }
        
   
    
    // Called when an error occurs during navigation.
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        log?.debug("didFailNavigation \(navigation) error:\(error)")
        
        hideHudView()
        
    }
  
}

extension CerealMainWebViewController : SystemPopupViewControllerDelegate {
    func systemPopupViewControllerDidFinished(_ viewController: SystemPopupViewController, action: SystemPopupAction) {
        viewController.dismiss(animated: true, completion: nil)
        switch action {
        case let .urlClose(urlString):
            self.urlString = urlString
        default:
            ()
        }
    }
}

extension CerealMainWebViewController :  UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if( otherGestureRecognizer.view == webView.scrollView) {
//            webView.scrollView.isScrollEnabled = false
            return true
        }
        
        return true
    }
}

extension CerealMainWebViewController : CerealMainWebViewControllerDelegate {
    func okSubWebViewController(_ : UIViewController ,callback : [String:Any]? ) {
        self.dismiss(animated: true, completion: nil)
        if let callback = callback {
            parentWebViewInterface?.afterNotify(callback)
        }
        
    }
}




extension CerealMainWebViewController : ChainContext {
   
    
    func finishChain() {
        
    }
    
    
    func isByPass(_ jobType: ChainJobType) -> Bool {
        switch jobType {
        default:
            return true
        }
    }
    
    func notifyBegin(_ jobType: ChainJobType) {
        switch jobType {
        case .permissionInitialCheck:
            ()
      
        default:
            ()
        }
    }
    
    func notifySuccess(_ jobType: ChainJobType) {
        switch jobType {
        case .permissionInitialCheck:
            ()
        default:
            ()
        }
    }
    
    func notifyError(_ jobType: ChainJobType) {
        
        switch jobType {
        case .permissionInitialCheck:
            ()
        default:
            ()
        }
    }
    
    
    func contextAction(_ jobType: ChainJobType, chain: Chain?) {
        ()
    }
    
    
    
}


extension  WKNavigationType : CustomStringConvertible {
    
    public var description: String {
        switch self {
        case .linkActivated: return "linkActivated"
        case .formSubmitted: return "formSubmitted"
        case .backForward: return "backForward"
        case .reload: return "reload"
        case .formResubmitted: return "formResubmitted"
        case .other: return "other"
            
        }
    }
}



