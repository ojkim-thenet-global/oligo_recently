//
//  OKSlidingInteractiveTransition.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class OKSlidingInteractiveTransition : OKPercentDrivenInteractiveTransition {

    var positiveLeftToRight : Bool = false
    var fullWidth : Float = 0
    var currentPercentage : Float?
    
    
    fileprivate var slidingViewController : OKSlidingViewController
    
    init(slidingViewController : OKSlidingViewController ) {
        self.slidingViewController = slidingViewController
        
        super.init()
    }
    
    override func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        log?.debug("startInteractiveTransition called")
        super.startInteractiveTransition(transitionContext)
        
        let moveViewController =  transitionContext.viewController(forKey: .move)!
        let finalMinEdge =  transitionContext.finalFrame(for: moveViewController).minX
        let initialMinEdge =  transitionContext.initialFrame(for: moveViewController).minX
        
        log?.debug("startInteractiveTransition finalMinEdge:\(finalMinEdge) initialMinEdge:\(initialMinEdge)")
        
        
        positiveLeftToRight = initialMinEdge < finalMinEdge;
        fullWidth           = fabsf(Float(finalMinEdge - initialMinEdge));
        currentPercentage   = 0;
      
        
        log?.debug("startInteractiveTransition called-2 fullWidth:\(fullWidth)")
        

        
    }
    
    func updateTopViewHorizontalCenterWithRecognizer(_ recognizer: UIPanGestureRecognizer)  {
        var translationX = recognizer.translation(in: self.slidingViewController.view).x
        let velocityX = recognizer.velocity(in: self.slidingViewController.view).x
        
//        log?.debug("translationX:\(translationX)")
//        log?.debug("velocityX:\(velocityX)")
        
        let isMovingRight = velocityX > 0
        
        switch recognizer.state {
        case .began:
            
            log?.debug("began isMovingRight:\(isMovingRight)")
            
            if slidingViewController.slidingMenuOption == .menuSlide {
                if slidingViewController.menuPosition == .right  {
                    
                    // hidden 상태에서 , 오른쪽에서 왼쪽으로 이동할 경우
                    if slidingViewController.menuSlidePosition == .hidden && !isMovingRight {
                        log?.debug("right hidden => Show")
                        slidingViewController.menuShow(true)
                    } else if slidingViewController.menuSlidePosition == .show && isMovingRight {
                        log?.debug("right show => hidden")
                        slidingViewController.menuHide(true)
                    }
                    
                }
            }
            
        case .changed:
            if !positiveLeftToRight {
                translationX *= -1
            }
            
            var  percentComplete = ( Float(translationX) / fullWidth)
            
            if percentComplete < 0 { percentComplete = 0}
            if percentComplete > 100 { percentComplete = 100}
            
            updateInteractiveTransition(CGFloat(percentComplete))
            
        case .ended, .cancelled :
            let isPanningRight = velocityX > 0
            
            
            if isPanningRight && positiveLeftToRight {
                finishInteractiveTransition()
            } else if !isPanningRight && positiveLeftToRight {
                cancelInteractiveTransition()
            } else if isPanningRight && !positiveLeftToRight {
                cancelInteractiveTransition()
            } else if !isPanningRight && !positiveLeftToRight {
                finishInteractiveTransition()
            }
        default:
            ()
        }
    }
    
    
}

