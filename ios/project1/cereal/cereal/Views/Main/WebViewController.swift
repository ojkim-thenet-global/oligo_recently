//
//  WebViewController.swift
//  cereal
//
//  Created by dubhe on 2018. 11. 26..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    
    var url : String?
    var webView : WKWebView?
    @IBOutlet var webViewContainer : UIView?
    @IBOutlet var statusHeight : NSLayoutConstraint?
    @IBOutlet var buttonTop : NSLayoutConstraint?
	var timer: Timer!
	
	var progressHUD: MBProgressHUD!

    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436, 2688, 1792:
                statusHeight?.constant = 35;
                buttonTop?.constant = 45;
                break
            default:
                break
            }
        };
        let request = URLRequest.init(url: URL.init(string: self.url!)!)
        self.webView = self.createWebView()
		webView?.navigationDelegate = self
        let scrollView = self.webView?.scrollView
        scrollView?.pinchGestureRecognizer?.isEnabled = true
        self.webViewContainer?.addSubview(self.webView!)
        self.webView?.translatesAutoresizingMaskIntoConstraints = false;
        
        self.webViewContainer?.addConstraint(NSLayoutConstraint.init(item: self.webView!, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.webViewContainer, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant:35))
        
        self.webViewContainer?.addConstraint(NSLayoutConstraint.init(item: self.webView!, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.webViewContainer, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant:0))
        
        self.webViewContainer?.addConstraint(NSLayoutConstraint.init(item: self.webView!, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.webViewContainer, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant:0))
        
        self.webViewContainer?.addConstraint(NSLayoutConstraint.init(item: self.webView!, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.webViewContainer, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant:0))
		
		progressHUD = MBProgressHUD.showCerealCHUDAdded(to: self.view, animated: true)
		timer = Timer.scheduledTimer(timeInterval: 10,
									 target: self,
									 selector: #selector(timerAction),
									 userInfo: nil,
									 repeats: false)
		
        self.webView?.load(request)
    }
	
	@objc func timerAction() {
		self.progressHUD.hide(animated: true)
		self.timer = nil
	}
	
    @IBAction func didTouchupInsideClose(_ sender: Any) {
        self.dismiss(animated: true) {
        }
    }
    
    func createWebView() -> WKWebView {
        let configuration = WKWebViewConfiguration.init();
        return WKWebView.init(frame: CGRect.zero, configuration: configuration);
    }
	
	func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
		if (timer != nil && timer.isValid) {
			timer.invalidate()
			timer = nil
		}
		progressHUD.hide(animated: true)
	}
	
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		if (timer != nil && timer.isValid) {
			timer.invalidate()
			timer = nil
		}
		progressHUD.hide(animated: true)
	}
}
