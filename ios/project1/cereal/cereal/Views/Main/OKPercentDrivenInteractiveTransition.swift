//
//  OKPercentDrivenInteractiveTransition.swift
//  OKInvestment
//
//  Created by sungyong Kang on 2018. 7. 12..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class OKPercentDrivenInteractiveTransition: NSObject , UIViewControllerInteractiveTransitioning{
    
    var animationController : UIViewControllerAnimatedTransitioning!
    weak var transitionContext : UIViewControllerContextTransitioning!
    var isActive : Bool = false
    
    public private(set) var percentComplete : Float = 0.0
    
    func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        isActive = true
        self.transitionContext = transitionContext
        
        let containerLayer = transitionContext.containerView.layer
        self.removeAnimationsRecursively(layer: containerLayer)
        
        animationController.animateTransition(using: transitionContext)
        
        self.updateInteractiveTransition(0)
        
    }
    
    func updateInteractiveTransition(_ percentComplete: CGFloat) {
        guard isActive else {return}
        
        transitionContext.updateInteractiveTransition(percentComplete)
        
        var boundedPercentage : Float!
        
        if percentComplete > 1.0 {
            boundedPercentage = 1.0
        } else if percentComplete < 0.0 {
            boundedPercentage = 0.0
        } else {
            boundedPercentage = Float(percentComplete)
        }
        
        self.percentComplete = boundedPercentage
        
        let layer = transitionContext.containerView.layer
        let pausedTime = animationController.transitionDuration(using: transitionContext) * TimeInterval(self.percentComplete)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
        
//        log?.debug("updateInteractiveTransition percentComplete:\(percentComplete) pausedTime:\(pausedTime)")
        
        
    }
    
    func cancelInteractiveTransition() {
        
        log?.debug("cancelInteractiveTransition called-1")
        guard isActive else {return}
        log?.debug("cancelInteractiveTransition called-2")
        transitionContext.cancelInteractiveTransition()
        
        let displayLink = CADisplayLink(target: self, selector: #selector(reversePausedAnimation(_:)))
        displayLink.add(to: RunLoop.main, forMode: .defaultRunLoopMode)
    }
    
   
    
    func finishInteractiveTransition() {
        
        log?.debug("finishInteractiveTransition called-1")
        
         guard isActive else {return}
        
        isActive = false
        
        transitionContext.finishInteractiveTransition()
        
        let layer = transitionContext.containerView.layer
        let pausedTime = layer.timeOffset
        layer.speed = 1.0;
        layer.timeOffset = 0.0;
        layer.beginTime = 0.0;
        let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        
        log?.debug("finishInteractiveTransition timeSincePause:\(timeSincePause)")
        
        layer.beginTime = timeSincePause
        
    }
    
 
    
    private func removeAnimationsRecursively(layer : CALayer) {
        
        if let sublayers = layer.sublayers , sublayers.count > 0 {
            layer.sublayers?.forEach({ (sublayer) in
                sublayer.removeAllAnimations()
                self.removeAnimationsRecursively(layer: sublayer)
            })
        }
    }
    
    @objc func reversePausedAnimation(_ displayLink:CADisplayLink) {
        
        log?.debug("reversePausedAnimation called")
        let percentInterval = displayLink.duration / animationController.transitionDuration(using: transitionContext)
        
        percentComplete -= Float(percentInterval)
        
        if percentComplete <= 0.0 {
            log?.debug("reversePausedAnimation  invalidate percentComplete:\(percentComplete)")
            percentComplete = 0.0
            displayLink .invalidate()
            log?.debug("reversePausedAnimation displayLink invalidate")
        }
        
        updateInteractiveTransition(CGFloat(percentComplete))
        
        if percentComplete == 0.0 {
            
            log?.debug("reversePausedAnimation percentComplete == 0.0 ")
            
            isActive = false
            let layer = transitionContext.containerView.layer
            layer.removeAllAnimations()
            layer.speed = 1.0
        }
    }

    

    
}
