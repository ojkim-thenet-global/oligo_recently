//
//  OKSlidingViewController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

// 슬라이딩 방법 : 메인이 슬라이딩 할지(mainSlide), 메뉴가 슬라이딩 할지 (menuSlide)
enum SlidingMenuOption {
    case mainSlide
    case menuSlide
}

// SlidingMenuOption 이 mainSlide( 메인이 슬라이딩 일 경우) ,
// 메인 슬라이딩 위치
// anchoredLeft     ( 메인이 왼쪽)
// anchoredRight    ( 메인이 오른쪽)
// centered         ( 메인이 센터)
enum MainSlidePosition {
    case anchoredLeft
    case anchoredRight
    case centered
}


// SlidingMenuOption 이 menuSlide( 메뉴가 슬라이딩 일 경우) ,
// 메뉴  위치
// left     ( 메뉴가 왼쪽)
// right    ( 메뉴가 오른쪽)
enum MenuPosition {
    case left
    case right
}

// SlidingMenuOption 이 menuSlide( 메뉴가 슬라이딩 일 경우) ,
// 메뉴 슬라이딩 위치 ( left, right 둘다 해당됨)
// hidden  ( 메뉴가 숨김  MenuPostion 이 left 일 경우,  왼쪽에 숨겨진 상태  right 일 경우,  오른쪽에 숨겨진 상태 )
// show    ( 메뉴가 보임  MenuPostion 이 left 일 경우,  왼쪽으로 부터 나온상태  right 일 경우,  오른쪽부터 나온상태 )
enum MenuSlidePosition {
    case hidden
    case show
}

// 매뉴의 동작방식  (Operation)  InteractiveTransition 과 관련됨
@objc  enum MenuOperation : Int  {
    
    case none // 기본 상태
    case mainAnchorLeft     // 슬라이딩 방법 : 메인이 슬라이딩 (mainSlide) 일 경우, 메인이 왼쪽으로 움직여라
    case mainAnchorRight    // 슬라이딩 방법 : 메인이 슬라이딩 (mainSlide) 일 경우, 메인이 오른쪽으로 움직여라
    case mainResetFromLeft  // 슬라이딩 방법 : 메인이 슬라이딩 (mainSlide) 일 경우, 왼쪽에서 가운데로 Rest
    case mainResetFromRight // 슬라이딩 방법 : 메인이 슬라이딩 (mainSlide) 일 경우, 오른쪽에서 가운데로 Rest
    case menuLeftShow       // 슬라이딩 방법 : 메뉴가 슬라이딩 (menuSlide) 이고, 메뉴위치 MenuPosition 가 Left 인경우,  메뉴가 보여줘라 ( 메뉴가 왼쪽에서 가운데로 보임)
    case menuLeftHidden    // 슬라이딩 방법 : 메뉴가 슬라이딩 (menuSlide) 이고, 메뉴위치 MenuPosition 가 Left 인경우,  메뉴를 숨겨라 ( 메뉴가  가운데에서 왼쪽으로 숨김)
    case menuRightShow     // 슬라이딩 방법 : 메뉴가 슬라이딩 (menuSlide) 이고, 메뉴위치 MenuPosition 가 Right 인경우,  메뉴가 보여줘라 ( 메뉴가 오른쪽에서 가운데로 보임)
    case menuRightHidden   // 슬라이딩 방법 : 메뉴가 슬라이딩 (menuSlide) 이고, 메뉴위치 MenuPosition 가 Right 인경우,  메뉴를 숨겨라 ( 메뉴가  가운데에서 오른쪽으로 숨김)
}


// 애니메이팅 (UIViewControllerAnimatedTransitioning) , 인터액티브 (UIViewControllerInteractiveTransitioning) 구현 할 Delegate 프로토콜
@objc protocol OKSlidingViewControllerDelegate   {
    // 슬라이딩 애니메이티드 트래지션닝 리턴
    @objc optional func slidingViewController(_ slidingViewController : OKSlidingViewController,
                                              animationControllerFor  operation : MenuOperation ,  topViewController : UIViewController ) -> UIViewControllerAnimatedTransitioning
    
    // 슬라이딩 인터액티브 트래지션닝 리턴
    @objc optional func slidingViewController(_ slidingViewController : OKSlidingViewController,
                                              interactionControllerFor  animationController : UIViewControllerAnimatedTransitioning  ) -> UIViewControllerInteractiveTransitioning

}


class OKSlidingViewController: UIViewController {

    // OKSlidingViewController 의 delegate
    var delegate: OKSlidingViewControllerDelegate?
    
    // topViewController 가 NavigationController 일때 ,   NavgiationController 을 OKSlidingViewController 에서 제어 하기 위해서
    var mainNavigationController : UINavigationController? {
        didSet {
            // PopGesture
            mainNavigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    }
    
    // 스토리 보드에서 동적으로 셋팅 할 수 있도록  IBInspectable 로 선언함.
    // Main.storybard 에서
    // SlidingViewController 에서
    // User Defined Runtime Attributes 에 지정함.
    
    //  topViewControllerStoryboardId 를 OKMainTopNavigationViewController 로 지정  ( CerealMainWebViewController 메인 웹뷰 를   Root 로 하는 NavigationController 임
    @IBInspectable var topViewControllerStoryboardId : String?
    //  leftViewControllerStoryboardId  설정 안함. 올리고에서 사용 안 하므로
    @IBInspectable var leftViewControllerStoryboardId : String?
    // rightViewControllerStoryboardId 를 OKMenuViewController (메뉴화면) 으로 지정
    @IBInspectable var rightViewControllerStoryboardId : String?
    
    // dimView 메뉴가 열렸을때, 터치 통해서 닫기 위한 용도. (메뉴 아닌 다른 부분 터치 했을 때, TapGestureRecognizer 통해서 메뉴 닫음)
    var dimView : UIView!
    
    // animationComplete 는 animation 동작이 끝났을 때, 호출 하는 Receiver 가 등록 한 animationComplete 클로저를 호출
    var animationComplete : VoidClousre?
    
    // currentAnimationPercentage 는 Interactive  동작의  퍼센티지
    var currentAnimationPercentage : Float = 0.0
    
    // 아이폰 4.7 인치는 넓이가 375,  아이폰 5.5 인치는 넓이가 414 이기 때문에 메뉴 넓이 335 를 할 수 있음
    let MAXIMUM_WIDTH : CGFloat = 335;
    
    // anchorPeekAmount 메뉴가 나왔을때, 메뉴를 제외한 크기
    lazy var  anchorPeekAmount : Int = {
        
        var width  = UIScreen.main.bounds.size.width
        
        // 아이폰 4' 인치는 넓이가 320 이므로 메뉴 넓이 MAXIMUM_WIDTH (335) 보다 작기 때문에, 리사이즈 하는 로직 필요. 320*0.1 = 32  320-32 = 288 이 메뉴의 넓이가 됨.
        if  ApplicationShare.shared.screenSize.smallScreen {
            return Int( width * 0.1)
        }
        else {
            return Int ( width  -  MAXIMUM_WIDTH)
        }
        
//         return Int ( width * (40/100) )
    }()
    
    // 메뉴가 슬라이딩 (menuSlide) 을 사용함 (참고로 mainSlide 는 메인이 슬라이딩됨)
    var slidingMenuOption : SlidingMenuOption = .menuSlide
    
    //  mainSlidingPosition :  슬라이드 옵션이  mainSlide 일때 의미가 있음
    var mainSlidingPosition : MainSlidePosition = .centered
    
    
    //  menuPosition , menuSlidePosition : 슬라이드 옵션이  menuSlide 일때 의미가 있음
    var menuPosition : MenuPosition = .right // 메뉴 위치는 오른쪽
    var menuSlidePosition : MenuSlidePosition = .hidden
    
    var currentOperation : MenuOperation = .none
    
    // transitionInProgress  Interactive 상태에서 animation 동작(Operation)을 더 추가 못하게 하는 용도
    var transitionInProgress = false
    var isInteractive = false
    var isAnimated = false
    // transitionWasCancelled  Interactive 상태에서 취소 된 상태 ( 취소시, 원래상태로 돌아감. 메뉴가 숨김상태에서 보여줌상태로 옮긴 상태에서, 취소 할 경우 숨김상태로 Rollback
    var transitionWasCancelled = false
    

    
    // topViewController 가 메인임. (즉 메뉴가 아님)
    var topViewController : UIViewController? {
        didSet {
            // topViewController 가 교체시 didSet Obeserver 가 호출 됨.
            // 기존에 topViewController 가 있을 경우, 해당 view 를 제거함.
            if let oldViewController = oldValue {
                oldViewController.view.removeFromSuperview()
                oldViewController.willMove(toParentViewController: nil)
                oldViewController.beginAppearanceTransition(false, animated: false)
                oldViewController.removeFromParentViewController()
                oldViewController.endAppearanceTransition()
            }
            
            // 신규 topViewController 가 nil 이 아닐 경우,  slidngViewcontroller 의  view 및 viewcontroller 에 add
            if let topViewController = topViewController {
                self.addChildViewController(topViewController)
                topViewController.didMove(toParentViewController: self)
                
                if self.isViewLoaded {
                    topViewController.beginAppearanceTransition(true, animated: false)
                    self.view.addSubview(topViewController.view)
                    topViewController.endAppearanceTransition()
                }
                
            }
        }
    }
    
    // leftViewController는 왼쪽 메뉴임. 사용하지 않음
    var leftViewController : UIViewController? {
        didSet {
            if let oldViewController = oldValue {
                oldViewController.view.removeFromSuperview()
                oldViewController.willMove(toParentViewController: nil)
                oldViewController.beginAppearanceTransition(false, animated: false)
                oldViewController.removeFromParentViewController()
                oldViewController.endAppearanceTransition()
            }
            
            if let leftViewController = leftViewController {
                self.addChildViewController(leftViewController)
                leftViewController.didMove(toParentViewController: self)
            }
           
        }
    }
    
    // rightViewController는 오른쪽 메뉴임. 사용함.
    // didSet 로직은  topViewController 의 로직 설명 참고
    var rightViewController : UIViewController? {
        didSet {
            if let oldViewController = oldValue {
                oldViewController.view.removeFromSuperview()
                oldViewController.willMove(toParentViewController: nil)
                oldViewController.beginAppearanceTransition(false, animated: false)
                oldViewController.removeFromParentViewController()
                oldViewController.endAppearanceTransition()
            }
            
            if let rightViewController = rightViewController {
                self.addChildViewController(rightViewController)
                rightViewController.didMove(toParentViewController: self)
                let contentView = rightViewController.view!
             
                // layer 의 shadow 추가 함.
                contentView.layer.cornerRadius = 30
                contentView.clipsToBounds = true
                contentView.layer.shadowColor = UIColor.black.cgColor
                contentView.layer.shadowOpacity = 0.5
                contentView.layer.shadowOffset = CGSize(width:0, height: 0)
                contentView.layer.shadowRadius = 3
                contentView.layer.masksToBounds = false
               
            }
            
        }
    }
    
    // 슬라이딩 메뉴의 현재  AnimatedTransitioning
    var currentAnimationController   : UIViewControllerAnimatedTransitioning?
    // 슬라이딩 메뉴의 현재  InteractiveTransitioning
    var currentInteractiveTransition : UIViewControllerInteractiveTransitioning?
    
    // Delegate 설정이 없을 경우 default AnimationController를 생성함.
    lazy var defaultAnimationController : OKSlidingAnimationController? = {
        var defaultAnimationController = OKSlidingAnimationController(slidingMenuOption: slidingMenuOption, menuPosition: menuPosition)
        return defaultAnimationController
    }()
    
    
    // Delegate 설정이 없을 경우 default InteractiveTransition를 생성함.
    lazy var defaultInteractiveTransition : OKSlidingInteractiveTransition?  = {
        var  defaultInteractiveTransition = OKSlidingInteractiveTransition(slidingViewController: self)
        defaultInteractiveTransition.animationController = defaultAnimationController
        return defaultInteractiveTransition;
    }()
    
    
    // 메뉴 열렸을때, tap 하면 menuHide 메소드로  메뉴 숨김
    lazy var resetTapGesture :  UITapGestureRecognizer = {
        let resetTapGesture = UITapGestureRecognizer(target: self, action: #selector(resetTopViewAnimated(_:)))
        resetTapGesture.delegate = self
        return resetTapGesture
    }()
    
    // 메뉴를 Pan(드래그) 통해서 메뉴 위치.
     // Pan 제스처 : 메뉴 위치는 InteractiveTransition 에서 처리 함.
    lazy var panGesture :  UIPanGestureRecognizer = {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(detectPanGestureRecognizer(_:)))
        panGesture.delegate = self
        return panGesture
    }()
    
    // 스크린 엣지 ( edges = .right Right 부터 )
    // 메뉴를 열기 위한 용도
    // Pan 제스처 : 메뉴 위치는 InteractiveTransition 에서 처리 함.
    lazy var screenEdgePanGesture :  UIScreenEdgePanGestureRecognizer = {
        let screenEdgePanGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(detectPanGestureRecognizer(_:)))
        screenEdgePanGesture.edges = .right
        screenEdgePanGesture.delegate = self
        return screenEdgePanGesture
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    typealias ShowLoginClousre = (Bool, Error?) -> Void // hybridShowLoginCompletion 멤버 타입  (success,error) 으로 콜백
    var hybridShowLoginCompletion :  ShowLoginClousre? // hybridShowLogin 함수에서 escaping  저장. 나중에 콜백할 때 호출
    
    // init 했을때 초기화 작업
    func setup() {
    }
    
    
    // Main.storyboard 통해서 OKSlidingViewController 인스턴스가 생기면,
    // 스토리보드 안에서 OKSlidingViewController 의 usef defined 속성을 가져와 topViewController, rightViewController 를 생성한다.
    override func awakeFromNib() {
        
        if let topViewControllerStoryboardId =   topViewControllerStoryboardId {
            self.topViewController =  UIStoryboard.storyboard(.main).instantiateViewController(withIdentifier: topViewControllerStoryboardId)
        }
        
        if let leftViewControllerStoryboardId =   leftViewControllerStoryboardId {
            self.leftViewController =  UIStoryboard.storyboard(.main).instantiateViewController(withIdentifier: leftViewControllerStoryboardId)
        }
        
        if let rightViewControllerStoryboardId =   rightViewControllerStoryboardId {
            self.rightViewController =  UIStoryboard.storyboard(.main).instantiateViewController(withIdentifier: rightViewControllerStoryboardId)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Notification Observer 등록
        addObserver()
        
        // 메인슬라이드가 아닌 메뉴 슬라이드 일 경우, rightViewController , leftViewController 의 view frame 을 계산한다.
        if slidingMenuOption != .mainSlide {
        
            topViewController!.view.frame = topViewCalculatedFrameForPosition(.centered)
            
            if let rightViewController =  rightViewController {
                rightViewController.view.frame = rightViewCalculatedFrameForPosition(.hidden)
            }
            
            if let leftViewController =  leftViewController {
                leftViewController.view.frame = leftViewCalculatedFrameForPosition(.hidden)
            }

        }
        
        view.addSubview(self.topViewController!.view)
        view.addGestureRecognizer(resetTapGesture)
        view.addGestureRecognizer(panGesture)
        
        // dimView 메뉴가 열렸을때, 터치 통해서 닫기 위한 용도. (메뉴 아닌 다른 부분 터치 했을 때, TapGestureRecognizer 통해서 메뉴 닫음)
        dimView = UIView(frame: self.topViewController!.view.frame)
        dimView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
        dimView.isUserInteractionEnabled = false
        dimView.isHidden = true
        view.addSubview(dimView)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    
    func addObserver() {
        
        Notification.Name.ActionLogin               .addObserver(self, selector: #selector(actionLogin(_:))) // 로그인 성공 Noti Observer
        Notification.Name.ShowSecurityKeyboard      .addObserver(self, selector: #selector(showSecurityKeyboard(_:))) // 보안키패드 띄우기 Noti Observer 테스트 용도임. 사용하지 않음
        Notification.Name.ShowLogin                 .addObserver(self, selector: #selector(showLogin(_:))) // 로그인창 띄우기 Noti Observer
        Notification.Name.ShowProfileSetting        .addObserver(self, selector: #selector(showProfileSetting(_:)))// 프로파일 화면 이동 Noti Observer 테스트 용도임. 사용하지 않음
        Notification.Name.ShowPermission            .addObserver(self, selector: #selector(showPermission(_:)))// 권한체크  화면 이동 Noti Observer 테스트 용도임. 사용하지 않음
        Notification.Name.ShowAnalytics             .addObserver(self, selector: #selector(showAnalytics(_:)))// GA 테스트  화면 이동 Noti Observer 테스트 용도임. 사용하지 않음
        Notification.Name.OpenMenu                  .addObserver(self, selector: #selector(openMenu(_:))) // 메뉴 띄우기 요청  Noti Observer
        Notification.Name.GetPicker                 .addObserver(self, selector: #selector(getPicker(_:))) // 날짜 선택 띄우기 화면 요청  Noti Observer 테스트 용도임. 사용하지 않음
        Notification.Name.ChangeWebUrl              .addObserver(self, selector: #selector(changeWebUrl(_:))) // 웹뷰 URL change Noti Observer
        Notification.Name.ShowKakaoTest             .addObserver(self, selector: #selector(showKakaoTest(_:))) // 카카오톡링크 화면 띄우기  Noti Observer 테스트 용도임. 사용하지 않음
        
    }
    
    // // 로그인 성공 Noti Observer
    @objc func actionLogin(_ notification:Notification) {
        menuHide(true)
    }
    
    // 메뉴 띄우기 요청  Noti Observer
    @objc func openMenu(_ notification:Notification) {
        menuShow(true)
    }
    
     // 날짜 선택 띄우기 화면 요청  Noti Observer 테스트 용도임. 사용하지 않음
    @objc func getPicker(_ notification:Notification) {
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.addon).instantiateViewController() as PickerTestViewController
            self.mainNavigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // 웹뷰 URL change Noti Observer
    @objc func changeWebUrl(_ notification:Notification) {
        
        menuHide(true) {
            if let mainNavigationController = self.mainNavigationController , mainNavigationController.viewControllers.count > 1 {
                mainNavigationController.popToRootViewController(animated: true)
            }
            if let userInfo = notification.userInfo {
                let webURL = userInfo["webURL"]
                if webURL != nil && (webURL as! String) == "https://pf.kakao.com/_zxdeDj" {
                    UIApplication.shared.openURL(URL(string: webURL as! String)!)
                } else {
                    self.mainNavigationController?.viewControllers[0].mainWebViewChangeWebUrl(webURL as! String)
                }
            }
        }
    }
    
    
    // 로그인 하이브리드 인터페이스 통해서 호출됨. 메뉴 열렸으면 메뉴 닫고, 로그인창 띄움.
    // 로그인 Delegate 에서 콜백 처리 하기 위해, hybridShowLoginCompletion 을 escaping 하고
    // 로그인 Delegate 에서 처리 결과를 hybridShowLoginCompletion 클로저로 처리
    func hybridShowLogin(_ hybridDirect:[String:String]? = nil,completion: @escaping  ShowLoginClousre ) {
        
        menuHide(true) {
            self.hybridShowLoginCompletion = completion
            let controller = UIStoryboard.storyboard(.login).instantiateViewController() as LoginViewController
            controller.info =  hybridDirect
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    // Native 메뉴에서 로그인창 띄우기 Noti 함.
    // 로그인창을 present 한다.
    @objc func showLogin(_ notification:Notification) {
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.login).instantiateViewController() as LoginViewController
            
            if let userInfo = notification.userInfo {
                controller.info = userInfo
            }
            
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    // 카카오톡 링크 페이지 이동 - 테스트 용도
    @objc func showKakaoTest(_ notification:Notification) {
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.kakao).instantiateViewController() as KakaoViewController
            self.mainNavigationController?.pushViewController(controller, animated: true)
            
        }
    }
    
    // 보안키패드 테스트 페이지 이동 - 테스트 용도
    @objc func showSecurityKeyboard(_ notification:Notification) {
        
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.addon).instantiateViewController() as ViewController
            self.mainNavigationController?.secondPushViewController(controller)
        }
    }
    
    // 프로필 이미지 설정 테스트 페이지 이동 - 테스트 용도
    @objc func showProfileSetting(_ notification:Notification) {
        
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.addon).instantiateViewController() as ProfileSettingTestViewController
            self.mainNavigationController?.secondPushViewController(controller)
        }
    }
    
    // Google GA 테스트 페이지 이동 - 테스트 용도
    @objc func showAnalytics(_ notification:Notification) {
        
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.addon).instantiateViewController() as AnalyticsViewController
            self.mainNavigationController?.secondPushViewController(controller)
        }
    }
    
    // 권한 설정  테스트 페이지 이동 - 테스트 용도
    @objc func showPermission(_ notification:Notification) {
        
        menuHide(true) {
            let controller = UIStoryboard.storyboard(.addon).instantiateViewController() as PermissionCheckViewController
            self.mainNavigationController?.secondPushViewController(controller)
            
        }
    }
    
    // 슬라이딩 포지션에 따라 frame 을 계산한다.
    func topViewCalculatedFrameForPosition(_ position : MainSlidePosition)-> CGRect {
    
        var containerViewFrame = self.view.bounds
        
        if !(topViewController!.edgesForExtendedLayout.contains(.top)) {
            let topLayoutGuideLength = self.topLayoutGuide.length
            containerViewFrame.origin.y = topLayoutGuideLength
            containerViewFrame.size.height -= topLayoutGuideLength
        }
        
        if !(topViewController!.edgesForExtendedLayout.contains(.bottom)) {
            let bottomLayoutGuideLength = self.bottomLayoutGuide.length
            containerViewFrame.size.height -= bottomLayoutGuideLength
        }
        
        switch position {
        case .centered:
            return containerViewFrame
        case .anchoredLeft:
            containerViewFrame.origin.x -= CGFloat(anchorPeekAmount)
            return containerViewFrame
        case .anchoredRight:
            containerViewFrame.origin.x += CGFloat(anchorPeekAmount)
            return containerViewFrame
        }
        
    }
    
    // 오른쪽 메뉴 뷰컨트롤러의 show, hide  일때의 frame 계산
    func rightViewCalculatedFrameForPosition(_ position : MenuSlidePosition)-> CGRect {
        
        var containerViewFrame = self.view.bounds
        
        if !(rightViewController!.edgesForExtendedLayout.contains(.top)) {
            let topLayoutGuideLength = self.topLayoutGuide.length
            containerViewFrame.origin.y = topLayoutGuideLength
            containerViewFrame.size.height -= topLayoutGuideLength
        }
        
        if !(rightViewController!.edgesForExtendedLayout.contains(.bottom)) {
            let bottomLayoutGuideLength = self.bottomLayoutGuide.length
            containerViewFrame.size.height -= bottomLayoutGuideLength
        }
        
        switch position {
        case .show:
            containerViewFrame.origin.x = CGFloat(anchorPeekAmount)
            return containerViewFrame
        case .hidden:
            containerViewFrame.origin.x = containerViewFrame.size.width

             return containerViewFrame
        }
        
    }
    
    // 왼쪽 메뉴 뷰컨트롤러의 show, hide  일때의 frame 계산 - 올리고에서는 왼쪽 메뉴가 없음
    func leftViewCalculatedFrameForPosition(_ position : MenuSlidePosition)-> CGRect {
        
        var containerViewFrame = self.view.bounds
        
        if !(leftViewController!.edgesForExtendedLayout.contains(.top)) {
            let topLayoutGuideLength = self.topLayoutGuide.length
            containerViewFrame.origin.y = topLayoutGuideLength
            containerViewFrame.size.height -= topLayoutGuideLength
        }
        
        if !(leftViewController!.edgesForExtendedLayout.contains(.bottom)) {
            let bottomLayoutGuideLength = self.bottomLayoutGuide.length
            containerViewFrame.size.height -= bottomLayoutGuideLength
        }

        switch position {
        case .show:
            containerViewFrame.origin.x = CGFloat(anchorPeekAmount)
            containerViewFrame.size.width -= CGFloat(anchorPeekAmount)
            return containerViewFrame
        case .hidden:
            containerViewFrame.origin.x = containerViewFrame.size.width
            containerViewFrame.size.width -= CGFloat(anchorPeekAmount)
            return containerViewFrame
        }
        
    }
    
   
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // 탭을 눌렀을 경우 메뉴를 숨긴다.
    @objc func resetTopViewAnimated(_ recognizer: UITapGestureRecognizer) {
        menuHide(true)
    }
    
    // Pan 제스처 : 메뉴 위치는 InteractiveTransition 에서 처리 함.
    @objc func detectPanGestureRecognizer(_ recognizer: UIPanGestureRecognizer) {
        
        if recognizer.state == .began {
            self.view.endEditing(true)
            isInteractive = true
            Notification.Name.MenuSlide.post()
        }
        
        self.defaultInteractiveTransition?.updateTopViewHorizontalCenterWithRecognizer(recognizer)
        isInteractive = false;
    }
    
   
    // 메뉴 보여줌
    func menuShow(_ animated : Bool) {
        menuShow(animated, completion: nil)
    }
    
    func menuShow(_ animated : Bool , completion: VoidClousre? = nil) {
        
        menuMovePosition(menuPosition, slidePoistion: .show, animated: animated, completion: completion)
    }
    
    // 메뉴 숨김
    func menuHide(_ animated : Bool) {
        menuHide(animated, completion: nil)
    }
    
    func menuHide(_ animated : Bool , completion: VoidClousre? = nil) {
        
        // 로딩바가 떠 있으면 닫게 한다.
        if let loadingBarHud = LoadingBarInterface.loadingBarHud {
            loadingBarHud.hide(animated: true)
        }
        
        LoadingBarInterface.loadingBarHud?.layer.zPosition = 0;
        
        
        menuMovePosition(menuPosition, slidePoistion: .hidden, animated: animated, completion: completion)
    }
    
    // 메뉴가 움직임.
    func menuMovePosition(_ menuPoistion : MenuPosition ,  slidePoistion : MenuSlidePosition, animated : Bool,  completion: VoidClousre? ) {
        
        self.isAnimated = animated
        self.animationComplete = completion
        
        self.view.endEditing(true)
        
        // 메뉴의 진행방향에 따라 operation 이 결정
        let operation = operationFromSlidePosition(menuPoistion, fromPosition: menuSlidePosition, toPoistion: slidePoistion)
        
        //  operation(menuRightShow ,menuRightHidden) 을 결정
        if operation != .none {
            self.animateOperation(operation)
        } else {
            // operation 이 none 이라는 애기는, 아무런 action 이 없거나, operation 이 이미 완료(complete 또는 cancel) 이 된 상태임.
            
            // animationComplete 클로저가 있을 경우 완료되었다고 호출
            if let animationComplete = animationComplete {
                animationComplete()
            }
        }
        
    }
    
    func operationFromSlidePosition(_ menuPoistion : MenuPosition , fromPosition : MenuSlidePosition , toPoistion : MenuSlidePosition ) -> MenuOperation {
       
        // 메뉴가 오른쪽인 경우
        if(menuPosition == .right) {
            // from, to 에 따라서 operation 을 결정
            if(fromPosition == .hidden && toPoistion == .show) {
                return .menuRightShow
            } else if(fromPosition == .show && toPoistion == .hidden) {
                return .menuRightHidden
            }
        } else if(menuPosition == .left) {
            // 메뉴가 왼쪽인 경우
            
            // from, to 에 따라서 operation 을 결정
            if(fromPosition == .hidden && toPoistion == .show) {
                return .menuLeftShow
            } else if(fromPosition == .show && toPoistion == .hidden) {
                return .menuLeftHidden
            }
        }
        return .none
    }
    
    // 애니메이션  동작
    func animateOperation(_  menuOperation : MenuOperation) {
        // transitionInProgress 트랜지션이 진행중이면, return 한다.
        guard !self.transitionInProgress else {
            return
        }
        
        view.isUserInteractionEnabled = false
        transitionInProgress = true
        
        currentOperation = menuOperation
        
        // delegate 가 있을 경우 delegate 로 부터 애니메이션컨트롤러, 인터액티브트랜지셔닝을 가져온다.
        if let delegate = self.delegate {
            if  let currentAnimationController = delegate.slidingViewController?(self, animationControllerFor: menuOperation, topViewController: topViewController!) {
                self.currentAnimationController = currentAnimationController
                
                if let currentAnimationController = self.currentAnimationController ,
                    let currentInteractiveTransition = delegate.slidingViewController?(self, interactionControllerFor: currentAnimationController) {
                    
                    self.currentInteractiveTransition = currentInteractiveTransition
                } else {
                    self.currentAnimationController = nil;
                }
            } else {
                self.currentAnimationController = nil;
            }
        } else {
            log?.debug("delegate nil")
        }
        
      
        //  delegate 가 없어서 delegate 로 부터 애니메이션컨트롤러, 인터액티브트랜지셔닝을 못 가져 올 경우, default 를 생성해서 할당한다.
        if self.currentAnimationController != nil {
            if self.currentInteractiveTransition != nil {
//                self.isInteractive = true
            }
            else {
                self.defaultInteractiveTransition?.animationController = self.currentAnimationController;
                self.currentInteractiveTransition = self.defaultInteractiveTransition;
            }
        } else {
            self.currentAnimationController = self.defaultAnimationController
            
            log?.debug("self.currentAnimationController: \(String(describing: self.currentAnimationController)) ")
            
            self.defaultInteractiveTransition?.animationController = self.currentAnimationController
            self.currentInteractiveTransition = self.defaultInteractiveTransition
        }
        
        //  인터액티브 트랜지시닝일경우 (PanGesture 통해서),
        if self.isInteractive {
            self.currentInteractiveTransition?.startInteractiveTransition(self)
        }else {
            // 인터액티브가 아닌경우
            self.currentAnimationController?.animateTransition(using: self)
        }
    }
    
    func percentComplete() -> Float {
        return currentAnimationPercentage
    }
    
}


// MARK:// UIViewControllerContextTransitioning

extension OKSlidingViewController :  UIViewControllerContextTransitioning {
    
 
    var containerView: UIView {
        return self.view
    }
    
    var presentationStyle: UIModalPresentationStyle {
        return .custom
    }
    
    func updateInteractiveTransition(_ percentComplete: CGFloat) {
        currentAnimationPercentage = Float(percentComplete)
    }
    
    func finishInteractiveTransition() {
        transitionWasCancelled = false
    }
    
    func cancelInteractiveTransition() {
        transitionWasCancelled = true
    }
    
    func pauseInteractiveTransition() {
        
    }
    
    
    func completeTransition(_ didComplete: Bool) {
        
        guard currentOperation != .none else { return }
        
        if transitionWasCancelled {
            
            if slidingMenuOption == .menuSlide {
                
                if menuPosition == .right {
                    if currentOperation == .menuRightShow  && menuSlidePosition == .hidden {
                       menuSlidePosition = .hidden
                    } else if currentOperation == .menuRightHidden  && menuSlidePosition == .show {
                        menuSlidePosition = .show
                    }
                }
            }
            
        } else {
            // transitionWasCancelled 아닌 경우
            
            if slidingMenuOption == .menuSlide {
                
                if menuPosition == .right {
                    if currentOperation == .menuRightShow  && menuSlidePosition == .hidden {
                        menuSlidePosition = .show
                    } else if currentOperation == .menuRightHidden  && menuSlidePosition == .show {
                        menuSlidePosition = .hidden
                    }
                  
                    
                }
            }
            
        }
        
        if let animationComplete = animationComplete {
            animationComplete()
        }
        
        
        if slidingMenuOption == .menuSlide {
            
            if menuPosition == .right {
                if menuSlidePosition == .show {
                    dimView.isHidden = false
                    view.insertSubview(dimView, belowSubview: rightViewController!.view)
                    topViewController?.view.isUserInteractionEnabled = false
                } else {
                   dimView.isHidden = true
                    topViewController?.view.isUserInteractionEnabled = true
                }
            } 
        }
        
        animationComplete = nil
        
        transitionWasCancelled = false
        isInteractive = false
        currentOperation = .none
        currentAnimationPercentage = 0.0
        transitionInProgress = false
        
        self.view.isUserInteractionEnabled = true;
    }
    
    func viewController(forKey key: UITransitionContextViewControllerKey) -> UIViewController? {
        
        if key == .top {
            return self.topViewController
        } else {
            if self.currentOperation == .menuRightShow {
                if key == .from {
                    return self.topViewController
                } else if key == .to {
                    return self.rightViewController
                } else if key == .move {
                    return self.rightViewController
                }
            } else  if self.currentOperation == .menuRightHidden {
                if key == .from {
                    return self.rightViewController
                } else if key == .to {
                    return self.topViewController
                } else if key == .move {
                    return self.rightViewController
                }
            }
        }
        return nil
    }
    
    func view(forKey key: UITransitionContextViewKey) -> UIView? {
        return nil
    }
    
    var targetTransform: CGAffineTransform {
        return CGAffineTransform.identity
    }
    
    func initialFrame(for vc: UIViewController) -> CGRect {
        
        if self.currentOperation == .menuRightShow {
            if vc == self.topViewController {
                return topViewCalculatedFrameForPosition(.centered)
            } else if vc == self.rightViewController {
                return rightViewCalculatedFrameForPosition(.hidden)
            }
        } else if self.currentOperation == .menuRightHidden {
            if vc == self.topViewController {
                return topViewCalculatedFrameForPosition(.centered)
            } else if vc == self.rightViewController {
                return rightViewCalculatedFrameForPosition(.show)
            }
        }
       
        
        return CGRect.zero
    }
    
    func finalFrame(for vc: UIViewController) -> CGRect {
        
        if self.currentOperation == .menuRightShow {
            if vc == self.topViewController {
                return topViewCalculatedFrameForPosition(.centered)
            } else if vc == self.rightViewController {
                return rightViewCalculatedFrameForPosition(.show)
            }
        } else if self.currentOperation == .menuRightHidden {
            if vc == self.topViewController {
                return topViewCalculatedFrameForPosition(.centered)
            } else if vc == self.rightViewController {
                return rightViewCalculatedFrameForPosition(.hidden)
            }
        }
        
        return CGRect.zero
    }
    
}


// MARK:// UIGestureRecognizerDelegate

extension OKSlidingViewController :  UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if panGesture == gestureRecognizer || resetTapGesture == gestureRecognizer {

            if slidingMenuOption == .menuSlide {

                if menuPosition == .right {
                    if  menuSlidePosition == .hidden {
                       return false
                    } else if  menuSlidePosition == .show {

                        if panGesture == gestureRecognizer {
                            return true
                        } else {
                            // 오른쪽 메뉴가 열려 있을 때, 오른쪽 메뉴 영역의 탭은 비활성화
                            if  let   rightViewBounds = rightViewController?.view.bounds,   rightViewBounds.contains(touch.location(in: rightViewController?.view)) {
                                return false
                            } else {
                                return true
                            }
                        }
                    }
                }
            }
        }

        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {

        if screenEdgePanGesture == gestureRecognizer {

            if slidingMenuOption == .menuSlide {
                if menuPosition == .right {
                    if  menuSlidePosition == .hidden {
                        return true
                    } else if  menuSlidePosition == .show {
                        return false
                    }
                }
            }
        } else if panGesture == gestureRecognizer {

            if slidingMenuOption == .menuSlide {
                if menuPosition == .right {
                    if  menuSlidePosition == .hidden {
                        return false
                    } else if  menuSlidePosition == .show {
                        return true
                    }
                }
            }
        } else if resetTapGesture == gestureRecognizer {
            
            if slidingMenuOption == .menuSlide {
                if menuPosition == .right {
                    if  menuSlidePosition == .hidden {
                        return false
                    } else if  menuSlidePosition == .show {
                        return true
                    }
                }
            }
        }

        return true
    }
}

// MARK:// LoginViewControllerDelegate
extension OKSlidingViewController : LoginViewControllerDelegate  {
    
    func loginViewController(_ controller: LoginViewController, action: DelegateButtonAction, info: Any?) {
        switch action {
            
        // 닫기 버튼
        case .close:
            
            
            
            // 하이브리드 통해서 로그인창 띄우고, close 버튼 누르면, false 로 callback 한다.
            if let hybridShowLoginCompletion = self.hybridShowLoginCompletion {
                
                
                if let info = info as? [String:String] {
                    
                    if let failURL = info["failURL"]
                        , failURL != "" {
                        let userInfo = ["webURL" :failURL]
                        Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo)
                    } else {
                        hybridShowLoginCompletion(false,nil)
                    }
                    
                    delay(0.1) {
                        self.dismiss(animated: true, completion:nil)
                    }
                    
                    
                } else {
                    hybridShowLoginCompletion(false,nil)
                    self.dismiss(animated: true, completion:nil)
                }
                
            } else {
                
                delay(1.5) {
                    self.dismiss(animated: true, completion:nil)
                }
                
                self.dismiss(animated: true, completion: nil)
            }
            
            
            
           
            
         // 로그인 성공
        case .login:
            // 로그인 모달창 닫기
            
            
            // hybridShowLoginCompletion 가 있다는것은 하이브리드 통해서 로그인 창 띄운 케이스
            // 2018-10-05 수정
            if let hybridShowLoginCompletion = self.hybridShowLoginCompletion {
                
                
                Notification.Name.ActionLogin.post(object: nil, userInfo:  ["hybridShowLogin":" Y"])
                
                if let info = info as? [String:String] {
                    
                    if let successURL = info["successURL"]
                        , successURL != "" {
                         let userInfo = ["webURL" :successURL]
                        Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo)
                    } else {
                        hybridShowLoginCompletion(true,nil)
                    }
                    
                    delay(0.1) {
                        self.dismiss(animated: true, completion:nil)
                    }
                    
                    
                } else {
                    hybridShowLoginCompletion(true,nil)
                    
                    delay(1.5) {
                        self.dismiss(animated: true, completion:nil)
                    }
                    
                }
                
            } else {
                self.dismiss(animated: true, completion: nil)
                // 메뉴 통해서 로그인 하는 경우
                if let info = info as? [String:Any] {
                    if let url = info["webURL"] as? String {
                        let userInfo = ["webURL" :url]
                        Notification.Name.ActionLogin.post(object: nil, userInfo: userInfo)
                    }
                } else {
                    // 메뉴 없이 로그인 하는 경우
                    Notification.Name.ActionLogin.post()
                }
            }
            
            
        // 회원가입 버튼 눌렀을때
        case .memberJoin:
            self.dismiss(animated: true, completion: nil)
            
            let url = MenuInfo.menuURL(from: .memIntro)
            let userInfo = ["webURL" :url!]
            Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
            
        // 아이디찾기  버튼 눌렀을때
        case .changeID:
            self.dismiss(animated: true, completion: nil)
            
            let url = MenuInfo.menuURL(from: .logFindId)
            let userInfo = ["webURL" :url!]
            Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
      
            
         // 비밀번호 변경은  로그인 실패시 응답값에 URL 내려줌
         //
        case .change90Pw:
            self.dismiss(animated: true, completion: nil)
            
            if let info = info as? [String:String] ,
                let url = info["webURL"] {
                let userInfo = ["webURL" :url]
                Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
            }
        // 비밀번호 찾기
        case .findPw:
            self.dismiss(animated: true, completion: nil)
            
            let url = MenuInfo.menuURL(from: .logFindPw)
            let userInfo = ["webURL" :url!]
            Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
        // 로그인 실패 1004 번 에러 코드 ( 로그인 토큰 정보가 없거나 토큰이 불일치)
        case .invalidToken:
            self.dismiss(animated: true, completion: nil)
            
            let url = MenuInfo.menuURL(from: .logFindId)
            var urlflag = url!
            urlflag += "?flag=2"
            let userInfo = ["webURL" :urlflag]
            Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
            
            
        default:
            ()
        }
        
        self.hybridShowLoginCompletion = nil
    }
}
