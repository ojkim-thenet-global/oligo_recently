//
//  CerealErrorPresenter.swift
//  cereal
//
//  Created by srkang on 2018. 8. 29..
//  Copyright © 2018년 srkang. All rights reserved.
//

import Foundation

protocol CerealErrorPresenterDelegate : class {
    func cerealErrorViewControllerFinished(  _ presenter : CerealErrorPresenter) -> Void
}

class CerealErrorPresenter {
    weak var delegate : CerealErrorPresenterDelegate? = nil
    
    var viewController : UIViewController!
    
    func presentError(_ viewController : UIViewController, titleMessage : String , subMessage : String )  {
        self.viewController = viewController
        
         let errorViewController = UIStoryboard.storyboard(.main) .instantiateViewController() as CerealErrorViewController
        errorViewController.delegate = self
        
        viewController.present(errorViewController, animated: true, completion: nil)
        
    }
    
}

extension CerealErrorPresenter : CerealErrorViewControllerDelegate {
    func cerealErrorViewControllerFinished(_ controller: CerealErrorViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        if let delegate = delegate {
            delegate.cerealErrorViewControllerFinished(self)
        }
    }
    
    
}
