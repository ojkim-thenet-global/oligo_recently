//
//  OKMenuViewController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Alamofire
import AlamofireImage

class OKMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var iconViewTopLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var badgeWidthConstraint:        NSLayoutConstraint!
    @IBOutlet weak var topViewLeadingConstraint:    NSLayoutConstraint!
    @IBOutlet weak var topViewTrailingConstraint:   NSLayoutConstraint!
    @IBOutlet weak var tableViewLeadingConstraint:  NSLayoutConstraint!
    @IBOutlet weak var tableViewTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var refreshButton:   UIButton!

    // IconView
    
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var badgeCountLabel:         UILabel! // 알림 뱃지 카운트 레이블
    
    // ProfileView
    
    @IBOutlet weak var profileImageView:        UIImageView! // 프로파일 이미지뷰
    @IBOutlet weak var profileMedalImageView:   UIImageView! // 프로파일 메달 이미지뷰
    
    @IBOutlet weak var profileLabel1: UILabel! // 닉네임, 투자왕 김프로
    @IBOutlet weak var profileLabel2: UILabel! // 투자성향명. 고독한 프리랜서
    @IBOutlet weak var myInfoButton: UIButton! // 내정보 버튼
    
    // BadgeView
    
    @IBOutlet weak var badgeImageView1: UIImageView! // 나의 뱃지 1 레벨 이미지뷰
    @IBOutlet weak var badgeImageView2: UIImageView! // 나의 뱃지 2 레벨 이미지뷰
    @IBOutlet weak var badgeImageView3: UIImageView! // 나의 뱃지 3 레벨 이미지뷰
    
    // EventView
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    // MyWalletView
    // 로그인전  배너
    @IBOutlet weak var bannerImageView:     UIImageView!
    @IBOutlet weak var bannerClearButton:   UIButton!
    
    // 로그인 이후 자산정보 StackView
    
     @IBOutlet weak var summaryView:    UIView!
    @IBOutlet weak var assetAmtLabel:       UILabel!
    @IBOutlet weak var balanceLabel:        UILabel!
    
    // 맨하단 로그인 , 아웃 레이블
    @IBOutlet weak var logInOutLabel:       UILabel!
    
    let MyIdentifier = "MyIdentifier"
    
    let modelKey = "label"
    
    let smallScreenPaddingSize : CGFloat = 15
    
    var tableModels : [ [String:Any] ]?
    
    var isQueryparentTopGuideLayout = false
    
    lazy var labelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognized(_:)))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiControls()

        
        tableModels = [ [modelKey:"공지사항",      "menu_id" : MenuInfo.MenuEnum.notice],
                        [modelKey:"이벤트",       "menu_id"  : MenuInfo.MenuEnum.event],
                        [modelKey:"올리고포인트 안내",   "menu_id"  : MenuInfo.MenuEnum.point],
                        [modelKey:"모바일쿠폰",     "menu_id"  : MenuInfo.MenuEnum.giftycon],
                        [modelKey:"FAQ",        "menu_id"  : MenuInfo.MenuEnum.faq],
//OJKIM MODIFIED START - 1:1문의를 카카오톡문의로 변경
						[modelKey:"카카오톡문의하기",    "menu_id"  : MenuInfo.MenuEnum.qna] ]
//                        [modelKey:"1:1 문의",    "menu_id"  : MenuInfo.MenuEnum.qna] ]
//OJKIM MODIFIED END

        
        
        Notification.Name.ActionLogin       .addObserver(self, selector: #selector(loginNotification(_:)))
        Notification.Name.ActionLogout      .addObserver(self, selector: #selector(logoutNotification(_:)))
        Notification.Name.PhotoChange       .addObserver(self, selector: #selector(photoChangeNotification(_:)))
        Notification.Name.NickNameChange    .addObserver(self, selector: #selector(nickNameChangeNotification(_:)))
        Notification.Name.InvestTypeChange  .addObserver(self, selector: #selector(investTypeChangeNotification(_:)))
        Notification.Name.DefaultImageChange.addObserver(self, selector: #selector(defaultImageChangeNotification(_:)))
        Notification.Name.BadgeCount        .addObserver(self, selector: #selector(badgeCountNotification(_:)))
        
        
        dispalyLoginOut()
    }
    
  
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // 딱 1번만 실행하기 위해서 isQueryparentTopGuideLayout Boolean 으로 처리함.
        // TopLayoutGuide, BottomLayoutGuide 의 length 값은  ViewDidLoad() 에서 잡을 수 없으므로,
        // viewDidLayoutSubviews 안에서 처리 했고, viewDidLayoutSubviews 메소드는 여러번 콜 되므로, 1번만 처리하기 위해서, Boolean 사용.
        if  !isQueryparentTopGuideLayout {
            isQueryparentTopGuideLayout = true
            
            // iOS 11 이후 부터는 TopLayoutGuide, BottomLayoutGuide 보다는 SafeArea Guide 사용.
            // if 문 안에는 없고, else 절은 iOS 10 이하 로직,
            // #available 구문은 not 구문이랑 같이 사용 할 수 없으므로, 부득이하게 , if 빈껍데기  else iOS10 이하 로직으로 처리함.
            if #available(iOS 11, *) {
                
            } else {
                // iOS 10 이하는  SafeArea  사용하지 못하고, TopLayoutGuide, BottomLayoutGuide 를 사용한다.
                
                //
                if let iconViewTopLayoutConstraint = iconViewTopLayoutConstraint {
                    NSLayoutConstraint.deactivate([iconViewTopLayoutConstraint])
                }
            
                if let  parentTopLayoutGuide = self.parent?.topLayoutGuide {
                    
                    // Menu 뷰는 Main 뷰가 아니기 때문에 topLayoutGuide의 bottomAnchor 길이가 상태바 크기가 잡히지 않고, 0 으로 잡힘.
                    // 메인뷰 컨트롤러 부터 length 를 질의해서, 그값을  constraint의 constant 로 셋팅한다.
                    
                    //FIXME://
                    
                    
                  //  NSLayoutConstraint.activate([self.iconView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: parentTopLayoutGuide.length)] )
                    
                    NSLayoutConstraint.activate([NSLayoutConstraint(item: self.iconView, attribute: .top, relatedBy: .equal, toItem: self.topLayoutGuide, attribute: .bottom, multiplier: 1.0, constant: parentTopLayoutGuide.length)])
//                      NSLayoutConstraint.activate([self.iconView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: parentTopLayoutGuide.length)] )
                    
                }
            
            }
            
            // iPhone 5,5s , SE 같은 4인치를  smallScreen 으로 정의함.
            // 메뉴 넓이가 335 이나, 4인치는 단말기 넓이가 320 이기 때문에,  335를  초과하므로,
            // 320 보다 작게. 320 * 0.9 = 288 로 잡고,
            // 메뉴 컨텐츠 좌우 폭 Default 가 25 (StoryBoard 에서 설정) 로 하기에, 컨텐츠 표현하는것이 모잘라서,
            // 15으로 줄임.
            if ApplicationShare.shared.screenSize.smallScreen {
                self.topViewLeadingConstraint.constant      = smallScreenPaddingSize
                self.topViewTrailingConstraint.constant     = smallScreenPaddingSize
                self.tableViewLeadingConstraint.constant    = smallScreenPaddingSize
                self.tableViewTrailingConstraint.constant   = smallScreenPaddingSize
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uiControls() {
        profileLabel1.isUserInteractionEnabled = true
        profileLabel1.addGestureRecognizer(labelTapGestureRecognizer)
    }
    
    //MARK:// Login Logout 처리
    
    func dispalyLoginOut() {
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if loginInfo.isLogin {
            // 로그인  상태
            refreshButton.isHidden = false
            // Alarm 카운트
            if let push_count = loginInfo.push_count , let iPushCount = Int(push_count) {
                alarmBadgeCnt(iPushCount)
            } else {
                alarmBadgeCnt(0)
            }
            
            
            // 프로파일 이미지 가져오기
            displayProfileImage()
            
            profileMedalImageView.isHidden =  false
            myInfoButton.isHidden = false // 내정보 버튼 숨김
            
            var badgeCnt = 0
            var currentIndex = 0
            
            let viewImageNameArray = [["badge_01_n","badge_01_o"] ,
                                      ["badge_02_n","badge_02_o"] ,
                                      ["badge_03_n","badge_03_o"]]
            
            
            for item in loginInfo.badge_list {
                
                let  viewImageNameItem = viewImageNameArray[currentIndex]
                
                if let item = item as? [String:Any] , let has_yn = item["has_yn"] as? String{
                    if has_yn.caseInsensitiveCompare("Y") == .orderedSame {
                        
                        if( currentIndex == 0) {
                            badgeImageView1.image = UIImage(named: viewImageNameItem[0])
                        } else if( currentIndex == 1) {
                            badgeImageView2.image = UIImage(named: viewImageNameItem[0])
                        } else if( currentIndex == 2) {
                            badgeImageView3.image = UIImage(named: viewImageNameItem[0])
                        }
                        badgeCnt += 1
                    } else { 
                        if( currentIndex == 0) {
                            badgeImageView1.image = UIImage(named: viewImageNameItem[1])
                        } else if( currentIndex == 1) {
                            badgeImageView2.image = UIImage(named: viewImageNameItem[1])
                        } else if( currentIndex == 2) {
                            badgeImageView3.image = UIImage(named: viewImageNameItem[1])
                        }
                    }
                }
                currentIndex += 1
            }
            
            if badgeCnt == 0 {
                profileMedalImageView.isHidden = true
            } else {
                profileMedalImageView.isHidden = false
            }
            
            switch badgeCnt {
            case 3:
                profileMedalImageView.image = UIImage(named: "profile_badge_03")
            case 2:
                profileMedalImageView.image = UIImage(named: "profile_badge_02")
            case 1:
                profileMedalImageView.image = UIImage(named: "profile_badge_01")
            default :
                ()
            }
            
            eventTitleLabel.text = "친구 초대하고 포인트 받아가세요!"
            
            if let asset_amt = loginInfo.asset_amt {
                self.assetAmtLabel.text = String(CerealUtils.stringFromStringWithDigits(asset_amt));
            } else  {
                self.assetAmtLabel.text = "0"
            }
            
            
            if let balance = loginInfo.balance {
                self.balanceLabel.text = String(CerealUtils.stringFromStringWithDigits(balance));
            } else {
                self.balanceLabel.text = "0"
            }
            
            bannerImageView.isHidden = true
            bannerClearButton.isHidden = true
            summaryView.isHidden = false
            
           loginProfileSetting()
           
            
            
            
            logInOutLabel.text = "로그아웃"
        } else {
            // 로그아웃 상태
            refreshButton.isHidden = true
            // Alarm 카운트
            alarmBadgeCnt(0)
            
            // 프로파일
            profileImageView.image = UIImage(named: "profile_nonmember")
            profileMedalImageView.isHidden = true
            myInfoButton.isHidden = false
            
            
            // 뱃지를 디폴트로 채움.
            badgeImageView1.image = UIImage(named: "badge_01_o")
            badgeImageView2.image = UIImage(named: "badge_02_o")
            badgeImageView3.image = UIImage(named: "badge_03_o")
            
            // 총자산 숨기고,  배너이미지 노출
            bannerImageView.isHidden = false
            bannerClearButton.isHidden = false
            summaryView.isHidden = true
            
            
            eventTitleLabel.text = "투자성향 분석하고 맞춤투자 추천받자!"
            
            //FIXME:// 배너 이미지 명은 하드코딩 된것을 Intro
            bannerImageView.image = UIImage(named: "img_banner_01")
            
            
            profileLabel1.text = "로그인"
            profileLabel2.text = "로그인 하세요"
            
            logInOutLabel.text = "로그인"
            
            
        }
    }
    
    func loginProfileSetting() {
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if let user_level = loginInfo.user_level , let nickname =  loginInfo.nickname{
            let s1 = "Lv.\(user_level)"
            let s2 = nickname + " "  + s1
            
            let content2 = NSMutableAttributedString(string:s2)
            
            let r1 = s2.range(of: s1)
            
            content2.addAttributes([
                .foregroundColor: UIColor(hexString: "8bcb2f"),
                ], range: s2.nsRange(from: r1!) )
            
            profileLabel1.attributedText = content2
        }
        
        if let simul_nm = loginInfo.simul_nm  , simul_nm != "없음" ,  simul_nm != "" {
            profileLabel2.text = simul_nm
        } else {
            profileLabel2.text = "투자성향 분석하기>"
        }
    }
    
    func displayProfileImage(_ url: String? = nil) {
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        var user_img_url : String?
        if let url = url {
            user_img_url = url
        } else {
            user_img_url = loginInfo.user_img_url
        }
        
        if let user_img_url = user_img_url {
            
            let imageURL = NetworkInterface.buildURL(serverDomainUse: false, appendString: user_img_url)
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                
                let fileManager = FileManager.default
                var documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent("temp")
                
                var isDir : ObjCBool = false
                if fileManager.fileExists(atPath: documentsURL.path, isDirectory: &isDir) {
                    if isDir.boolValue {
                        
                    } else {
                        
                    }
                } else {
                    // file does not exist
                    
                    do {
                        try fileManager.createDirectory(at: documentsURL, withIntermediateDirectories: true, attributes: nil)
                    } catch {
                        
                    }
                }
                
                
                documentsURL.appendPathComponent("profile.jpg")
                
                
                return (documentsURL, [.removePreviousFile])
            }
            
            Alamofire.download(imageURL, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
                log?.debug("Progress: \(progress.fractionCompleted)")
                } .validate().responseData { ( response ) in
                    
                    if let destinationURL = response.destinationURL {
                        log?.debug(destinationURL.lastPathComponent)
                        if let data = try? Data(contentsOf: destinationURL) {
                            self.profileImageView.image = UIImage(data: data)
                        }
                        
                    }
                    
            }
        } else {
            profileImageView.image = UIImage(named: "profile_nonmember")
        }
        
    }
    
    func alarmBadgeCnt(_ cnt : Int) {
        
        if cnt > 0 {
            self.badgeCountLabel.isHidden = false
            
            var cntLabel = "99+"
            if cnt < 100 {
                cntLabel = String(cnt)
            }
            self.badgeCountLabel.text = cntLabel
            
            let contentSize = self.badgeCountLabel.intrinsicContentSize
            badgeWidthConstraint.constant = contentSize.width + 10
            
        } else {
            self.badgeCountLabel.isHidden = true
        }
    }
    
    //MARK:// IBAction
    
    // 홈버튼 아이콘
    @IBAction func btnHomeAction(_ sender: Any) {
        changeUrl(from : .main)
    }
    
    // 홈 Refresh 아이콘
    @IBAction func btnRefreshAction(_ sender: Any) {
        log?.debug("btnRefreshAction called")
        
        
        let progressHUD = MBProgressHUD.showCerealCHUDAdded(to: self.view, animated: true)
        
        
        NetworkInterface.transferServer(viewController: self, operation: .user_asset_info ) { [weak self]  (error, response) in
            
            guard let strongSelf = self else {
                return
            }
            
            progressHUD.hide(animated: true)
            
            
            if error == nil {
                
                // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                    if let data = response["data"] as? [String:Any]   {
                        //                                let last_login_time = data["Last_login_time"]
                        
                        log?.debug(data)
                        
                        /*
                         {
                         "msg" : "정상적으로 처리되었습니다.",
                         "data" : {
                         "user_level" : 2,
                         "simul_nm" : "",
                         "nickname" : "0XDKHPS8",
                         "push_count" : 3,
                         "badge_list" : [
                         {
                         "has_yn" : "N",
                         "order" : "1",
                         "badge_nm" : "MISSION1",
                         "badge_cd" : "1"
                         },
                         {
                         "has_yn" : "N",
                         "order" : "2",
                         "badge_nm" : "MISSION2",
                         "badge_cd" : "2"
                         },
                         {
                         "has_yn" : "N",
                         "order" : "3",
                         "badge_nm" : "MISSION3",
                         "badge_cd" : "3"
                         }
                         ],
                         "asset_info" : {
                         "balance" : 110000,
                         "asset_amt" : 110000
                         },
                         "user_img_url" : null
                         },
                         "code" : "0000"
                         }                        */
                        
                        if let asset_info = data["asset_info"] as? [String:Any] {
                            let asset_amt  = asset_info["asset_amt"] as! Int
                            let balance    = asset_info["balance"] as! Int
                            
                            strongSelf.assetAmtLabel.text = String(CerealUtils.stringFromNumberWithDigits(asset_amt));
                            strongSelf.balanceLabel.text = String(CerealUtils.stringFromNumberWithDigits(balance));
                            
                        }
                     
                        
                        if let user_level = data["user_level"] as? String {
                            ApplicationShare.shared.loginInfo.user_level = user_level
                        } else if let user_level = data["user_level"] as? Int {
                            ApplicationShare.shared.loginInfo.user_level = String(user_level)
                        }
                        
                        if let nickname = data["nickname"] as? String {
                            ApplicationShare.shared.loginInfo.nickname = nickname
                        }
                        
                        if let simul_nm = data["simul_nm"] as? String {
                            ApplicationShare.shared.loginInfo.simul_nm = simul_nm
                        }
                        
                        strongSelf.loginProfileSetting()
                        
                        if let push_count = data["push_count"] as? Int {
                           strongSelf.alarmBadgeCnt( push_count )
                        }
                        
                        if let user_img_url = data["user_img_url"] as? String , user_img_url != "" {
                            strongSelf.displayProfileImage(user_img_url)
                        }
                        
                       
                      
                        
                        
                    }
                } else if let response =  response,  let msg = response["msg"] as? String {
                    // code 가 Success 가 아닌 경우
                    let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                    alertController.addAction(alertAction)
                    strongSelf.present(alertController, animated: true, completion: nil)
                    
                } else {
                    // response 없는 경우
                }
            } else {
                //  AlamoFire 오류
                
            }
            
        }
    }
    
    
    // 알림버튼 아이콘
    @IBAction func btnAlaramAction(_ sender: Any) {
        log?.debug("btnAlaramAction called")
        
        changeUrl(from : .alarm)
        
    }
    
    // 환경설정버튼 아이콘
    @IBAction func btnSettingAction(_ sender: Any) {
        log?.debug("btnSettingAction called")
        changeUrl(from : .setting)
    }
    
    // 사용안함.
    @IBAction func profileImageSectionAction(_ sender: Any) {
        
    }
    
    // 프로파일 닉네임 이동
    @IBAction func profile1SelectAction(_ sender: Any) {
        log?.debug("profile1SelectAction called")
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if !loginInfo.isLogin {
            // 로그인 안했을 경우 로그인 화면.  로그인전 일경우, 텍스트가 로그인 상태
            logInOutAction(sender)
        }
    }
    
    
    // 프로파일 투자성향 이동
    @IBAction func profile2SelectAction(_ sender: Any) {
        log?.debug("profile2SelectAction called")
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if !loginInfo.isLogin {
            // 로그인 안했을 경우 로그인 화면. 로그인전 일경우, 텍스트가 로그인 상태
            logInOutAction(sender)
        } else {
            // 로그인 하고 투자성향이 없을 경우  투자성향 분석으로 이동. 버튼 텍스트명은 투자성향 분석하세요 >
            if let simul_nm = loginInfo.simul_nm  , simul_nm != "없음" ,  simul_nm != "" {
//                changeUrl(from : .invAna)
            } else {
                changeUrl(from : .invAna)
            }
            
        }
       
    }
    
    // 내정보 버튼
    @IBAction func myInfoSelectAction(_ sender: Any) {
        log?.debug("myInfoSelectAction called")
        changeUrl(from : .myInfo)
    }
    
    // 뱃지 획득방법
    @IBAction func badgeGainSelectAction(_ sender: Any) {
        log?.debug("badgeGainSelectAction called")
        changeUrl(from : .badgeInfo)
    }
    
    
    
    //  이벤트 레이블
    @IBAction func eventSelectAction(_ sender: Any) {
        log?.debug("eventSelectAction called")
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if loginInfo.isLogin {
            // 로그인 했을 경우 친구추천
            changeUrl(from : .recommendUser)
        } else {
            // 로그인 안 했을 경우 투자성향분석
            changeUrl(from : .invAna)
        }
        
        
    }
    
    
    // 로그인전 배너 이미지
    @IBAction func bannerEventAction(_ sender: Any) {
        log?.debug("bannerEventAction called")
     
        // 회원가입 페이지 이동대신, 로그인 화면으로 이동. 기획자 의도는 사용자가 회원가입 되어있는지 모르기 때문에 로그인 페이지로 이동 하라는 요청
        Notification.Name.ShowLogin.post()
//        changeUrl(from : .memIntro)
      
    }
    
    // Tap 1 - 간편 투자
    @IBAction func btnTap1Action(_ sender: Any) {
        
        changeUrl(from : .inveset)
    }
    
    // Tap 2 - 내지갑
    @IBAction func btnTap2Action(_ sender: Any) {
        
        changeUrl(from : .wallet)
        
    }
    
    // Tap 3 - 리얼투자
    @IBAction func btnTap3Action(_ sender: Any) {
        
        
        changeUrl(from : .community)
    }
    
    // 로그인 후 총자산 Refresh
    @IBAction func btnAssetRefreshAction(_ sender: Any) {
        log?.debug("btnAssetRefreshAction called")
        
        
        let progressHUD = MBProgressHUD.showCerealCHUDAdded(to: self.view, animated: true)
        
        
        NetworkInterface.transferServer(viewController: self, operation: .user_asset_info ) { [weak self]  (error, response) in
            
            guard let strongSelf = self else {
                return
            }
            
            progressHUD.hide(animated: true)
            
            
            if error == nil {
                
                // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                    if let data = response["data"] as? [String:Any]   {
                        //                                let last_login_time = data["Last_login_time"]
                        
                        log?.debug(data)
                        
                        if let asset_info = data["asset_info"] as? [String:Any] {
                            let asset_amt  = asset_info["asset_amt"] as! Int
                            let balance    = asset_info["balance"] as! Int
                            
                            strongSelf.assetAmtLabel.text = String(CerealUtils.stringFromNumberWithDigits(asset_amt));
                            strongSelf.balanceLabel.text = String(CerealUtils.stringFromNumberWithDigits(balance));
                            
                        } else {
                            
                        }
                        
                        
                    }
                } else if let response =  response,  let msg = response["msg"] as? String {
                    // code 가 Success 가 아닌 경우
                    let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                    alertController.addAction(alertAction)
                    strongSelf.present(alertController, animated: true, completion: nil)
                    
                } else {
                    // response 없는 경우
                }
            } else {
                //  AlamoFire 오류
                
            }
            
        }
    }
    
   
    
    func changeUrl(from : MenuInfo.MenuEnum) {
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if !loginInfo.isLogin {
            
            let loginYn = MenuInfo.menuLoginYn(from: from)
            
            if loginYn {
                
                let url = MenuInfo.menuURL(from: from)!

                Notification.Name.ShowLogin.post(object: nil, userInfo: ["webURL" : url  ])
                
                return
            }
        }
        
        let url = MenuInfo.menuURL(from: from)
        let userInfo = ["webURL" :url!]

        Notification.Name.ChangeWebUrl.post(object: nil, userInfo: userInfo )
    }
    
    
    // 로그인, 로그아웃
    @IBAction func logInOutAction(_ sender: Any) {
        
        let loginInfo = ApplicationShare.shared.loginInfo
        
        if loginInfo.isLogin {
          
            logoutProcess()
            
        } else {
            // 로그인전, 로그인 화면으로 이동
            
             Notification.Name.ShowLogin.post()
        }
    }
    
    func logoutProcess() {
        // 로그인 상태, 로그아웃 Alert 이후 로그아웃 처리
        
        let alertController = UIAlertController(title: "", message: "로그아웃 하시겠습니까?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        
        let logoutAction = UIAlertAction(title: "확인", style: .default, handler: { _ in
            
            NetworkInterface.transferServer(viewController: self, operation: .logout ) { [weak self]  (error, response) in
                
                guard let strongSelf = self else {
                    return
                }
                
                if error == nil {
                    
                    /*
                     {
                     "msg" : "로그인 핀번호 복호화 오류",
                     "data" : {
                     "aResultCode" : "50006",
                     "aResultMessage" : "[DB fail]-[Does not exist, can not be select, deleted or updated to] "
                     },
                     "code" : "1001"
                     }
                     */
                    
                    // 로그인 응답존재 하고,  Succes (response["code"]  == "0000" ) 인 경우
                    if let response =  response,  let code = response["code"] as? String , code == CerealConstrants.NetworkServerResponse.success {
                        if let data = response["data"] as? [String:Any]   {
                            //                                let last_login_time = data["Last_login_time"]
                            
                            
                            ApplicationShare.shared.loginInfo.resetLogout()
                            Notification.Name.ActionLogout.post()
                            
                             let keyChainWrapper = ApplicationShare.shared.keychainWrapper
                             let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
                            custItem.auto_login = false
                            keyChainWrapper.set(custItem, forKey: Configuration.keyChainKey)
                            
                            delay(0.1) {
                                if let slidingViewController = self?.slidingViewController() {
                                    slidingViewController.menuHide(true)
                                }
                            }
                            
                            
                            
                        }
                    } else if let response =  response,  let msg = response["msg"] as? String {
                        // code 가 Success 가 아닌 경우
                        let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                        alertController.addAction(alertAction)
                        strongSelf.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        // response 없는 경우
                    }
                } else {
                    //  AlamoFire 오류
                    
                }
                
            }
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(logoutAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    
  
    
    // MARK:// TapGestureRecognizer
    
    @objc func tapGestureRecognized(_ tapGestureRecognizer : UITapGestureRecognizer) {
        profile1SelectAction(tapGestureRecognizer)
    }
    
    // MARK:// Notification
    
    @objc func loginNotification(_ notification : Notification) {
        dispalyLoginOut()
    }
    
    @objc func logoutNotification(_ notification : Notification) {
        dispalyLoginOut()
    }

    @objc func photoChangeNotification(_ notification : Notification) {
        
        if let userInfo = notification.userInfo {
            if let url = userInfo["url"] as? String  {
                displayProfileImage(url)
            }
        }
        
    }
    
    @objc func nickNameChangeNotification(_ notification : Notification) {
        
        if let userInfo = notification.userInfo {
            if let nickName = userInfo["NickName"] as? String  {
                
                ApplicationShare.shared.loginInfo.nickname = nickName
                
                loginProfileSetting()
            }
        }
        
    }
    
    @objc func investTypeChangeNotification(_ notification : Notification) {
        
        if let userInfo = notification.userInfo {
            if let investType = userInfo["InvestType"] as? String  {
                
                ApplicationShare.shared.loginInfo.simul_nm = investType
                
                loginProfileSetting()
            }
        }
        
    }
    
    
    
    
    
    @objc func defaultImageChangeNotification(_ notification : Notification) {
        
        ApplicationShare.shared.loginInfo.user_img_url = nil
        displayProfileImage()
        
    }
    
    @objc func badgeCountNotification(_ notification : Notification) {
        
        if let userInfo = notification.userInfo ,
            let badgeCount = userInfo["badgeCount"] as? Int {
            alarmBadgeCnt( badgeCount )
        }
        
    }
    
    
 
    
    
}

// MARK:// UITableViewDataSource, UITableViewDelegate

extension OKMenuViewController : UITableViewDataSource {
    

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (tableModels?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyIdentifier, for: indexPath) as! RootTableViewCell
        
        let row = indexPath.row
        cell.info = tableModels![row]
        
        return cell
    }
    
}



extension OKMenuViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let row = indexPath.row
        
        let info = tableModels![row]
        
        changeUrl(from: info["menu_id"] as! MenuInfo.MenuEnum)
        
        
        
    }
       
}


