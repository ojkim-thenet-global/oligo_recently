//
//  OKScriptMessageHandler.swift
//  cereal
//
//  Created by srkang on 2018. 7. 26..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import WebKit

class OKScriptMessageHandler : NSObject, WKScriptMessageHandler {
    
    weak var delegate : WKScriptMessageHandler?
    init(delegate:WKScriptMessageHandler) {
        self.delegate = delegate
        super.init()
    }
    func userContentController(_ ucc: WKUserContentController,
                               didReceive message: WKScriptMessage) {
        self.delegate?.userContentController(ucc, didReceive: message)
    }
    
    deinit {
        log?.debug("OKScriptMessageHandler deinit")
    }
}
