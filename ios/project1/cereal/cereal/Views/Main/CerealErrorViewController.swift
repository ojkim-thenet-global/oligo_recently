//
//  CerealErrorViewController.swift
//  cereal
//
//  Created by srkang on 2018. 8. 29..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

protocol CerealErrorViewControllerDelegate : class {
     func cerealErrorViewControllerFinished(  _ controller : CerealErrorViewController) -> Void
}

class CerealErrorViewController: UIViewController {

    weak var delegate : CerealErrorViewControllerDelegate? = nil
    
    @IBOutlet weak var previousButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let buttonTitle = "이전페이지로 가기"
        let attributeTitle = buttonTitle.underline
        previousButton.setAttributedTitle(attributeTitle, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnPreviousAction(_ sender: Any) {
        
        if let delegate = delegate {
            delegate.cerealErrorViewControllerFinished(self)
        }
    }
    
}
