//
//  OKUIWebViewController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SnapKit


class OKUIWebViewController: UIViewController {
    
    var webView : UIWebView!
    
    let SCRIPT_HANDLER_NAME = "hybridscript"
    
    var hybridInterface : HybridInterface?
    var parentWebViewInterface : WebViewInterface?
    
    var isMaster = true
    
    var subParameters : [ String : Any ]? {
        didSet {
            
            if let subParameters = subParameters {
                var urlString = subParameters["url"] as! String
                var url : URL!
                if urlString.hasPrefix("http") {
                    
                    url = NetworkInterface.buildURL(serverDomainUse: false, appendString: urlString)
                    
                } else {
                    url = NetworkInterface.buildURL(serverDomainUse: true, appendString: urlString)
                    
                    //FIXME://
                    
                    url = URL(string: defaultURLString)
                    url.appendPathComponent(urlString)
                }
                
                let  urlRequest = URLRequest(url: url)
                webView.loadRequest(urlRequest)
            }
           
        }
    }
    
    var delegate : CerealMainWebViewControllerDelegate?
    
    var urlString:String! {
        didSet {
            
            var url : URL!
            if urlString.hasPrefix("http") {
                url = NetworkInterface.buildURL(serverDomainUse: false, appendString: urlString)
            } else {
                url = NetworkInterface.buildURL(serverDomainUse: true, appendString: urlString)
            }
            
            let  urlRequest = URLRequest(url: url)
//            urlRequest.httpMethod = "POST"
            webView.loadRequest(urlRequest)
        }
    }
    
    @objc override func mainWebViewChangeWebUrl(_ urlString:String) {
        self.urlString = urlString
        
    }
    
    var subWebViewCallBack : ( ()->Void)?
    
//    urlString = "https://blooming-shelf-99889.herokuapp.com"
//    urlString = "http://localhost:5000"

    let defaultURLString = "main.do"
//    let defaultURLString = "https://blooming-shelf-99889.herokuapp.com"
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()

        self.slidingViewController()?.mainNavigationController = self.navigationController
        
        self.navigationController?.view.addGestureRecognizer((self.slidingViewController()?.screenEdgePanGesture)!)
        
//        self.slidingViewController()?.screenEdgePanGesture.delegate = self

        
        webView = UIWebView(frame: CGRect.zero)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = UIColor.red
        webView.delegate = self
        webView.scrollView.showsHorizontalScrollIndicator = false
        
        if let slidingViewController = self.slidingViewController() {
            webView.scrollView.panGestureRecognizer.require(toFail: (slidingViewController.screenEdgePanGesture))
        }
        
      
        
        view.addSubview(webView)
        
        
//        NSLayoutConstraint.activate([
//            webView.topAnchor.constraint(equalTo: view.topAnchor),
//            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
//            ])
        
        
        
        if #available(iOS 11, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.view.backgroundColor = UIColor.white
        
        self.webView.isOpaque = false
        self.webView.backgroundColor = UIColor.clear
        self.webView.scrollView.backgroundColor = UIColor.clear
        
       // webView.loadHTMLString(s, baseURL: nil)
//        webView.load(URLRequest(url: URL(string: "http://localhost:5000")!))
        
        
        let button = UIButton(type: .custom)
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(self, action: #selector( refreshButtonAction(_:) ), for: .touchUpInside)
        
        view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-50)
        }
        
        
        if isMaster {
             urlString = defaultURLString
        }
        
        
    }
    
    
    
    @objc func refreshButtonAction(_ sender : Any) {
        webView.reload()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
//         self.view.backgroundColor = UIColor.magenta
    }
    
    deinit {
        print("OKUIWebViewController deinit")
    }
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        self.slidingViewController()?.menuShow(true) {
            print("menuButtonTapped completed called")
        }
    }
}

extension OKUIWebViewController : UIWebViewDelegate {
    
}

