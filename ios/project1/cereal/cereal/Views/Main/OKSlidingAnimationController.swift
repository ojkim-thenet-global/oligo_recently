//
//  OKSlidingAnimationController.swift
//  OKInvestment
//
//  Created by srkang on 2018. 6. 19..
//  Copyright © 2018년 srkang. All rights reserved.
//


import UIKit

class OKSlidingAnimationController: NSObject {
    var defaultTransitionDuration : TimeInterval = 0.0
    
    var slidingMenuOption : SlidingMenuOption
    var menuPosition : MenuPosition
    
    
    init(slidingMenuOption : SlidingMenuOption , menuPosition: MenuPosition ) {
        
        self.slidingMenuOption = slidingMenuOption
        self.menuPosition = menuPosition
        
        super.init()
    }
    
        
}

extension OKSlidingAnimationController : UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let topViewController = transitionContext.viewController(forKey: .top)!
        let fromViewController  = transitionContext.viewController(forKey: .from)!
        let toViewController  = transitionContext.viewController(forKey: .to)!
        
        
        let containerView       = transitionContext.containerView
        
        
        let fromViewInitialFrame = transitionContext.initialFrame(for: fromViewController)
        let fromViewFinalFrame   = transitionContext.finalFrame(for: fromViewController)
        
        let toViewInitialFrame = transitionContext.initialFrame(for: toViewController)
        let toViewFinalFrame   = transitionContext.finalFrame(for: toViewController)
        
        
        if topViewController != toViewController {
            containerView.insertSubview(toViewController.view, aboveSubview: topViewController.view)
        }
        
        
        toViewController.view.frame = toViewInitialFrame
        
        
        
        let duration = self.transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, animations: {
            UIView.setAnimationCurve(.easeInOut)
            
            fromViewController.view.frame = fromViewFinalFrame
            toViewController.view.frame = toViewFinalFrame
            
        }) { (finished) in
            
            let cancelled = transitionContext.transitionWasCancelled
            
            log?.debug("animateTransition finished:\(finished) cancelled:\(cancelled)")
            
            if cancelled {
                
                fromViewController.view.frame = fromViewInitialFrame
                toViewController.view.frame = toViewInitialFrame
                
            }
            
            transitionContext.completeTransition(finished)
           
            
            
        }
        
    }
    
    
}
