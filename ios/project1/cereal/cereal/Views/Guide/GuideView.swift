//
//  GuideView.swift
//  cereal
//
//  Created by srkang on 2018. 8. 24..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

protocol GuideViewDelegate : class {
    func guidViewClick(_ view : GuideView ,  at index: Int)
}

class GuideView: UIView {

    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var bottomLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var bottomButton: UIButton!
    
   var imageViewConstraint: NSLayoutConstraint!
    
    weak var delegate : GuideViewDelegate?
    
    var pageNum = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let view = Bundle.main.loadNibNamed("GuideView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        
        if ApplicationShare.shared.screenSize.smallScreen {
            
            imageViewConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 0.56, constant:0.0)
            self.addConstraint(imageViewConstraint)
        }  else if ApplicationShare.shared.screenSize.middleScreen {
            
            imageViewConstraint = NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .lessThanOrEqual, toItem: self, attribute: .width, multiplier: 0.62, constant:0.0)
            self.addConstraint(imageViewConstraint)
        }
        
    }
    
    func displayView(pageNum : Int , topText : String , bottomText : String , imageName : String , buttonHidden : Bool , buttonName : String? = nil) {
        
        self.pageNum = pageNum
        topLabel.text = topText
        bottomLabel.text = bottomText
        imageView.image = UIImage(named: imageName)
        bottomButton.isHidden = buttonHidden
        bottomButton.setTitle(buttonName, for: .normal)
    }
    
    
    @IBAction func buttonClickAction(_ sender: Any) {
        
        if let delegate = delegate {
            delegate.guidViewClick(self, at: pageNum)
        }
    }
}
