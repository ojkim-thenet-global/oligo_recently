//
//  GuideViewController.swift
//  cereal
//
//  Created by srkang on 2018. 8. 24..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

enum GuideAction {
    case skip
    case finish
}

protocol GuideViewControllerDelegate : class {
    func guideViewControllerFinished(_ viewController : GuideViewController ,  action: GuideAction)
}


class GuideViewController: UIViewController {

    @IBOutlet weak var skipButton: UIButton!
    
    var currentPage = 0
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var isFirst = false
    
     weak var delegate : GuideViewControllerDelegate?
    
    var observation : NSKeyValueObservation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
//        let dateObservation = configurationManager.configuration.observe(\.updatedAt, options: [.new]) {[weak self] (observed, changed) in
//            self?.label.text = self?.configurationManager.dateFormatter.string(from: changed.newValue!)
//        }
        
        
        observation = pageControl.observe(\.currentPage, options: [.new]) { (observed, changed) in
          
            if let newValue = changed.newValue  {
                
                if self.currentPage != newValue {
                    if newValue == 4 {
                        self.skipButton.setTitle("나중에하기", for: .normal)
                    } else {
                        self.skipButton.setTitle("SKIP", for: .normal)
                    }
                }
                
                self.currentPage = newValue
                
            }
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pageControlChanged(_ sender: UIPageControl) {
        let currentPage = sender.currentPage
        scrollView.currentPage = currentPage
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(!isFirst) {
            isFirst = true
            
            var topLength       : CGFloat!
            var bottomLength    : CGFloat!
            
            if #available(iOS 11.0, *) {
                //            let window = UIApplication.shared.keyWindow
                topLength = view?.safeAreaInsets.top
                bottomLength = view?.safeAreaInsets.bottom
                
            } else {
                topLength = self.topLayoutGuide.length
                bottomLength = self.bottomLayoutGuide.length
            }
            
            let frame = scrollView.bounds
            
            
            
            let guideView1 = GuideView()
            let guideView2 = GuideView()
            let guideView3 = GuideView()
            let guideView4 = GuideView()
            let guideView5 = GuideView()
            
            guideView5.delegate = self
            
            
            guideView1.displayView(pageNum:0, topText: "다양한 상품정보와\n투자를 한번에",
                                   bottomText: "다양한 대안투자 상품 정보를\n한번에 보고 바로 투자할 수 있어요.", imageName: "guide_01", buttonHidden: true)
            guideView2.displayView(pageNum:1, topText: "투자 자산관리를\n편리하게",
                                   bottomText: "나의 지갑 서비스로 투자하신\n업체의 투자 내역과 현황을 관리 할 수 있어요.", imageName: "guide_02", buttonHidden: true)
            guideView3.displayView(pageNum:2, topText: "계좌를 몰라도\n송금을 쉽게",
                                   bottomText: "공인인증서,계좌번호 없이 연락처만으로\n송금을 할 수 있어요.", imageName: "guide_03", buttonHidden: true)
            guideView4.displayView(pageNum:3, topText: "리얼한 금융정보로\n투자를 스마트하게",
                                   bottomText: "실제 투자자들과 리얼한 투자후기를\n공유해보세요.", imageName: "guide_04", buttonHidden: true)
            guideView5.displayView(pageNum:4, topText: "올리고는 회원가입도\n한결 가볍게",
                                   bottomText: "간편가입으로 다양한 회원혜택을\n경험해보세요.", imageName: "guide_05", buttonHidden: false, buttonName: "시작하기")
            
            scrollContentView.addSubview(guideView1)
            scrollContentView.addSubview(guideView2)
            scrollContentView.addSubview(guideView3)
            scrollContentView.addSubview(guideView4)
            scrollContentView.addSubview(guideView5)
            
            //let height  = frame.height - bottomLength
            let height  = frame.height
            
            guideView1.frame = CGRect(0 * frame.width,0, frame.width, height)
            guideView2.frame = CGRect(1 * frame.width,0, frame.width, height)
            guideView3.frame = CGRect(2 * frame.width,0, frame.width, height)
            guideView4.frame = CGRect(3 * frame.width,0, frame.width, height)
            guideView5.frame = CGRect(4 * frame.width,0, frame.width, height)
        }
        
    }
    
    
    @IBAction func btnGuideSkipAction(_ sender: Any) {
        delegate?.guideViewControllerFinished(self, action: .skip)
    }
    

    
}

extension GuideViewController : GuideViewDelegate {
    func guidViewClick(_ view: GuideView, at index: Int) {
        if index == 4 {
            if let delegate = delegate {
                delegate.guideViewControllerFinished(self, action: .finish)
            }
        }
    }
}

extension GuideViewController : UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            let currentPage = scrollView.currentPage
            pageControl.currentPage = currentPage
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = scrollView.currentPage
        pageControl.currentPage = currentPage
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.currentPage
        pageControl.currentPage = currentPage
    }
}
