//
//  CerealUtils.swift
//  cereal
//
//  Created by srkang on 2018. 8. 9..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class CerealUtils {

    static func stringFromNumberWithDigits(_ number: Int , digits : Int? = 0) -> String {
        let formatCurrency = NumberFormatter()
        formatCurrency.numberStyle = .decimal
        
        if let digits  = digits {
            formatCurrency.maximumFractionDigits = digits
            formatCurrency.minimumFractionDigits = digits
        }
        
        let formatString = formatCurrency.string(from: (number as NSNumber )) ?? ""
        
        return formatString
    }
    
    static func stringFromStringWithDigits(_ string: String , digits : Int? = 0) -> String {
        
        if let number = Int(string) {
            return stringFromNumberWithDigits(number, digits: digits)
        } else {
            return ""
        }
        
    }

}
