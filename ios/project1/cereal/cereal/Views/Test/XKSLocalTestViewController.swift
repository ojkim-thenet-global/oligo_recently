//
//  XKSLocalTestViewController.swift

//
//  Created by srkang on 2018. 6. 7..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SnapKit
import AVFoundation

class XKSLocalTestViewController: UIViewController {

    
//    XKTextField * mXKTextField2;
//    XKTextField * mXKTextField3;

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var xkKeypadType : XKeypadType?
    var xkKeypadViewType : XKeypadViewType?
    
    var mTextFieldCollection : XKTextFieldCollection!
  
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: XKTextField!
    @IBOutlet weak var textField3: XKTextField!
    
    @IBOutlet weak var textField4: UITextField!
    
    
    @IBOutlet weak var xkTextField4: XKTextField!
    
    func visibleViewController() -> UIViewController {
        return self;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
    
        label.snp.makeConstraints { (make) in
            make.centerX.equalTo(view).offset(0)
            make.bottom.equalTo(view).offset(-50)
            
//            make.edges.equalTo(view).inset(300).constraint
        }

        // Do any additional setup after loading the view.
        
        xkTextField4 = XKTextField(frame: CGRect.zero)
        self.view.addSubview(xkTextField4)
        
        mTextFieldCollection = XKTextFieldCollection()
        
        textField1.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        
        textField2.returnDelegate = self
        textField2.xkeypadType = xkKeypadType!
        textField2.xkeypadViewType = xkKeypadViewType!
        textField2.xkeypadPreViewType =  XKeypadPreViewType.off
        
        let bundle = Bundle.main.path(forResource: "backgroundSample", ofType: nil)
//        let image = UIImage(contentsOfFile: bundle!)
        
        let image = UIImage(named: "backgroundSample")
 
        textField2.setBackgroundImage(image, alpha: 0.4)
        textField2.subTitle = "비밀번호"
        textField2 .setUseInputButton(true)
        mTextFieldCollection .addTextField(textField2)
        
        
        
        textField3.returnDelegate = self
        textField3.xkeypadType = xkKeypadType!
        textField3.xkeypadViewType = xkKeypadViewType!
        textField3.subTitle = "비밀번호"
        mTextFieldCollection .addTextField(textField3)
        
        
        
        xkTextField4.returnDelegate = self
        xkTextField4.xkeypadType = xkKeypadType!
        xkTextField4.xkeypadViewType = xkKeypadViewType!
        
        
    }
    
    deinit {
        log?.debug("XKSLocalTestViewController deinit called")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func hideKeypadAction(_ sender: Any) {
        textField4.resignFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension XKSLocalTestViewController : UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        log?.debug("textFieldShouldBeginEditing called")
        
        if(textField == textField4) {
            
            xkTextField4.becomeFirstResponder()
            
            return false
        }
        
        return true
    }
    
//
//    optional public func textFieldDidBeginEditing(_ textField: UITextField) // became first responder
//
//
//    optional public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
//
//
//    optional public func textFieldDidEndEditing(_ textField: UITextField)
//
//
//    optional public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason)  textFieldDidEndEditing:
//
//
//
//    optional public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//
//
//    optional public func textFieldShouldClear(_ textField: UITextField) -> Bool
//
//
//    optional public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    
    
}

extension XKSLocalTestViewController : XKTextFieldDelegate {
//    - (void) keypadInputCompleted:(NSInteger)aCount;
//
//    - (void) keypadInputCompleted:(XKTextField *) textField
//    count:(NSInteger) count;
//
//    - (void) keypadE2EInputCompleted:(NSString *)aSessionID
//    token:(NSString *)aToken
//    indexCount:(NSInteger)aCount;
//
//    - (void) keypadE2EInputCompleted:(XKTextField *) textField
//    sessionID:(NSString *) sessionID
//    token:(NSString *) token
//    indexCount:(NSInteger) count;
//
//    - (void) keypadCanceled;
//
//    - (void) keypadCreateFailed:(NSInteger) errorCode;
//
//    - (BOOL)textField:(XKTextField *)textField shouldChangeLength:(NSUInteger)length;
//    - (BOOL)textFieldShouldDeleteCharacter:(XKTextField *)textField;
//    - (void)textFieldSessionTimeOut:(XKTextField *)textField;
    
    func keypadInputCompleted(_ aCount: Int) {
        log?.debug("ABC keypadInputCompleted aCount \(aCount)")
        
        
        log?.debug("ABC keypadInputCompleted getData \(xkTextField4.getData())")
        
        
        
        
    }
    
    func keypadInputCompleted(_ textField: XKTextField!, count: Int) {
        log?.debug("ABC keypadInputCompleted textField \(textField) count \(count)")
    }
    
    func keypadE2EInputCompleted(_ aSessionID: String!, token aToken: String!, indexCount aCount: Int) {
        log?.debug("ABC keypadE2EInputCompleted aSessionID \(aSessionID) aToken \(aToken) aCount \(aCount)")
    }
    
    func keypadE2EInputCompleted(_ textField: XKTextField!, sessionID: String!, token: String!, indexCount count: Int) {
        log?.debug("ABC keypadE2EInputCompleted sessionID \(sessionID) token \(token) count \(count)")
    }
    
    func keypadCanceled() {
        log?.debug("ABC keypadCanceled ")
    }
    
    func textField(_ textField: XKTextField!, shouldChangeLength length: UInt) -> Bool {
        log?.debug("ABC shouldChangeLength  \(length) ")
        
        if(textField == xkTextField4) {
            
            var mutableString = ""
            for v in   0 ..< length {
                log?.debug("v\(v)")
                mutableString += "1"
                textField4.text = mutableString
            }
            
        }
        return true
    }
    
    func textFieldShouldDeleteCharacter(_ textField: XKTextField!) -> Bool {
        log?.debug("ABC textFieldShouldDeleteCharacter ")
        return true
    }
    
    func textFieldSessionTimeOut(_ textField: XKTextField!) {
        UIAlertController.showMessage("보안 세션이 만료되었습니다.\n다시 실행해 주세요.");
        log?.debug("ABC textFieldSessionTimeOut ")
        textField.cancelKeypad();
    }
    
   
    
   
}





