//
//  PickerTestViewController.swift
//  cereal
//
//  Created by srkang on 2018. 7. 20..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class PickerTestViewController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        datePicker.backgroundColor = .black
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
        datePicker.setValue(0.8, forKeyPath: "alpha")
        
        for subview in datePicker.subviews {
            
            if subview.frame.height <= 5 {
                
                subview.backgroundColor = UIColor.white
                subview.tintColor = UIColor.white
                subview.layer.borderColor = UIColor.white.cgColor
                subview.layer.borderWidth = 0.5            }
        }
        
        if let pickerView = datePicker.subviews.first {
            
            for subview in pickerView.subviews {
                
                if subview.frame.height <= 5 {
                    
                    subview.backgroundColor = UIColor.white
                    subview.tintColor = UIColor.white
                    subview.layer.borderColor = UIColor.white.cgColor
                    subview.layer.borderWidth = 0.5
                }
            }
            self.datePicker.setValue(UIColor.white, forKey: "textColor")
        }
        
//        UIDatePicker.appearance().tintColor
        
//        let pickerView = UIPickerView(frame: .zero)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
