//
//  ProfileSettingTestViewController.swift
//  cereal
//
//  Created by srkang on 2018. 7. 9..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

class ProfileSettingTestViewController: UIViewController {

    
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func btnSelectImageAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "카메라 이미지 선택", message: "프로파일에 선택할 이미지 선택하세요", preferredStyle:  .actionSheet)
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let cameraAction = UIAlertAction(title: "카메라", style: .default) { (alertAction) in
                
                log?.debug("cameraAction called")
                
                if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                    
                    permissionScope.statusCamera(completion: { (status) in
                        
                        
                        permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                            
                            log?.debug("btnCameraRequestAction onAuthChange called-1")
                        }
                        
                        if status == .unknown {
                            
                            permissionScope.requestPermission(type: .camera, permissionResult: { (finished, permissionResult,error) in
                                
                                if error == nil && permissionResult?.status == .authorized {
                                    self.showImagePicker(type: .camera)
                                }
                                
                            })
                        }
                        else if status == .authorized {
                            self.showImagePicker(type: .camera)
                        }
                        
                        else if status == .unauthorized {
                            permissionScope.showDeniedAlert(viewController: self, permission: .camera)
                        }
                        
                    })
                }
                
                
            }
            alertController.addAction(cameraAction)
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imageAction = UIAlertAction(title: "이미지", style: .default) { (alertAction) in
                log?.debug("imageAction called")
                
                if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                    
                    permissionScope.statusPhotos(completion: { (status) in
                        
                        
                        permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                            
                            log?.debug("photo onAuthChange called-1")
                        }
                        
                        if status == .unknown {
                            
                            permissionScope.requestPermission(type: .photos, permissionResult: { (finished, permissionResult,error) in
                                
                                if error == nil && permissionResult?.status == .authorized {
                                    self.showImagePicker(type: .photoLibrary)
                                }
                                
                            })
                        }
                        else if status == .authorized {
                            self.showImagePicker(type: .photoLibrary)
                        }
                            
                        else if status == .unauthorized {
                            permissionScope.showDeniedAlert(viewController: self, permission: .photos)
                        }
                        
                    })
                }
            }
            alertController.addAction(imageAction)
        }
        
        if alertController.actions.count > 0 {
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    
    @IBAction func btnSendServerAction(_ sender: Any) {
        
        
        if let image = self.profileImageView.image {
            
            
            guard let imageData = UIImageJPEGRepresentation(image, 0.5) else {
                log?.debug("Could not get JPEG representation of UIImage")
                return
            }
            
            let keyChainWrapper = ApplicationShare.shared.keychainWrapper
            let custItem = keyChainWrapper.object(forKey: Configuration.keyChainKey) as! KeyChainCustItem
            
            
            NetworkInterface.upload(.registerProfile, multipartFormData: { (multipartFormData) in
                multipartFormData.append(imageData,  withName: "file", fileName: "fileName.jpg", mimeType: "image/jpeg")
                multipartFormData.append(custItem.user_no!.data(using: .utf8)!, withName: "user_no")
                
                
//                multipartFormData.append(, withName: <#T##String#>, mimeType: String)
                        },
                encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress { progress in
                        
                        log?.debug(progress.fractionCompleted)
                        //progressCompletion(Float(progress.fractionCompleted))
                    }
                    
                    upload.log()
                   
                    
                    upload.response(completionHandler: { (defaultData) in
                        if let data =  defaultData.data {
                            var responseData = String(data: data, encoding: .utf8)
                            
                            log?.debug(defaultData.response?.statusCode as Any)
                            log?.debug(responseData as Any)
                        }
                    })
                    
                    upload.responseJSON { response in
                        guard response.result.isSuccess,
                            let value = response.result.value else {
                                log?.debug("Error while uploading file: \(String(describing: response.result.error))")
                                //completion(nil, nil)
                                return
                        }
                        

                    }
                    
                    
                case .failure(let encodingError):
                    log?.debug(encodingError)
                }
            })
        }
        
    }
    
    
    func showImagePicker(type : UIImagePickerControllerSourceType) {
    
        let picker = UIImagePickerController()
        picker.sourceType = type
        picker.mediaTypes = [ kUTTypeImage as String]
    
        picker.delegate = self
    
        picker.allowsEditing = true // try true
    
    
        self.present(picker, animated: true)
       
    }
    
    func showImage(_ image:UIImage) {
        
        self.profileImageView.image = image
    }
    

}


extension ProfileSettingTestViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated:true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let url = info[UIImagePickerControllerMediaURL] as? URL
        var image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = editedImage
        }
        // iOS9.1  UIImagePickerControllerLivePhoto
//        if let live = info[UIImagePickerControllerLivePhoto] {
//            log?.debug("I got a live photo!") // nope
//            _ = live
//        }
        // iOS11 UIImagePickerControllerImageURL
//        let imurl = info[UIImagePickerControllerImageURL] as? URL
//        log?.debug(imurl as Any)
        
        
        let m = info[UIImagePickerControllerMediaMetadata] as? NSDictionary
//        log?.debug(m as Any)
        
        self.dismiss(animated:true) {
            let mediaType = info[UIImagePickerControllerMediaType]
            guard let type = mediaType as? NSString else {return}
            switch type as CFString {
            case kUTTypeImage:
                self.showImage(image!)
            default:
                ()
            }
            
        }
    }
}
