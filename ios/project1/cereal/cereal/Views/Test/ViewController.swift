//
//  ViewController.swift

//
//  Created by srkang on 2018. 6. 5..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import SnapKit


class ViewController: UIViewController {
    
    let  XKSLocalPushSegue  = "XKSLocalPushSegue"
    @IBOutlet weak var keyboardTypeSegment: UISegmentedControl!
    //
    //    CGPoint originalCenter
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
    var mXKeypadType : XKeypadType?
    var mXKeypadViewType :  XKeypadViewType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        mXKeypadType = XKeypadType.qwerty
        mXKeypadViewType = XKeypadViewType.fullView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            mXKeypadType = XKeypadType.qwerty
        default:
            mXKeypadType = XKeypadType.number
        }
    }
    
    
    @IBAction func segmentKeypadViewTypeChanged(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            mXKeypadViewType = XKeypadViewType.fullView
        default:
            mXKeypadViewType = XKeypadViewType.normalView
        }

        
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == XKSLocalPushSegue {
            let controller = segue.destination as! XKSLocalTestViewController
            
            controller.xkKeypadType = mXKeypadType
            controller.xkKeypadViewType = mXKeypadViewType
        }
        
    }

 
}


extension ViewController  {
    // new functionality to add to SomeType goes here
    
 
}


