//
//  MyTableViewController.swift

//
//  Created by srkang on 2018. 6. 11..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Firebase


class MyTableViewController: UITableViewController {

    //var tableModels = [String:String]()
    //var tableModels : [String:String]?
    
    let MyIdentifier = "MyIdentifier"
    
    let modelKey = "label"
    
    var tableModels : [ [String:Any] ]?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        title = "메인"
        tableModels = [ [modelKey:"보안키패드"], [modelKey:"로그인"] ,  [modelKey:"하이브리드"] ,  [modelKey:"하이브리드 서버연동"]  ,   [modelKey:"Google Anaytics"] ,
          [modelKey:"권한체크"] ,   [modelKey:"프로파일 셋팅"] ]
        
        //
        
        Analytics.logEvent(AnalyticsEventLogin, parameters: nil)
        
//        notificationCheck()
    }
    
//    func notificationCheck() {
//        
//         if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
//            
//            permissionScope.statusNotifications(status: { (status) in
//                
//                if status ==  .authorized {
//                    
//                    
////                    ApplicationShare.shared.registerFCMAndRemote()
//                    (UIApplication.shared.delegate as! AppDelegate).fcmRegister()
//
//                    
//                }
//            })
//
//            
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  (tableModels?.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyIdentifier, for: indexPath) as! RootTableViewCell
        
        let row = indexPath.row
        cell.info = tableModels![row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let row = indexPath.row
        
        
        switch row {
        case 0:
            performSegue(withIdentifier: "SecuritySegue", sender: nil)
        case 1:
            
            
            
            let controller = UIStoryboard.storyboard(.login).instantiateViewController() as LoginViewController
            
            controller.delegate = self
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
//            self.present(controller, animated: true, completion: nil)
            
            
            
        case 2:
            performSegue(withIdentifier: "HybridSegue2", sender: nil)
        case 3:
            performSegue(withIdentifier: "HybridSegue3", sender: nil)
        case 4:
            performSegue(withIdentifier: "AnalyticsSegue", sender: nil)
        case 5:
            performSegue(withIdentifier: "PermissionSegue", sender: nil)
        case 6:
            performSegue(withIdentifier: "ProfileSettingSegue", sender: nil)
        default:
            performSegue(withIdentifier: "HybridSegue", sender: nil)
        }
        
        
        
        
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        SecuritySegue
//    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyTableViewController : LoginViewControllerDelegate  {
    func loginViewController(_ controller: LoginViewController, action: DelegateButtonAction, info: Any?) {
        switch action {
        case .close:
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: false)
        default:
            ()
        }
    }
}
