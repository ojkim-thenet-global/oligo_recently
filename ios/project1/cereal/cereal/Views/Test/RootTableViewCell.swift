//
//  RootTableViewCell.swift

//
//  Created by srkang on 2018. 6. 11..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class RootTableViewCell: UITableViewCell {

    var info :  [String:Any]? {
        didSet(newValue) {
            label.text = info?["label"] as? String
        }
    }
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
