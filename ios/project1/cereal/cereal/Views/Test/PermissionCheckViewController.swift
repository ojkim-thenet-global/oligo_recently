//
//  PermissionCheckViewController.swift
//  cereal
//
//  Created by srkang on 2018. 7. 6..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

class PermissionCheckViewController: UIViewController {
    
    
    @IBOutlet weak var pushStatusLabel: UILabel!
    
    @IBOutlet weak var cameraStatusLabel: UILabel!
    
    @IBOutlet weak var photoStatusLabel: UILabel!
    
    @IBOutlet weak var contactStatusLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnPushRequestAction(_ sender: Any) {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusNotifications(completion: { (status) in
                
                delay(0.0) {
                    if status == .unknown {
                        
                        //                    ( Bool, [PermissionResult] ) -> Void
                        
                        permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                            //self.pushResult(status: permissionStatus)
                        }
                        
                        permissionScope.requestPermission(type: .notifications, permissionResult: { (finished, permissionResult,error) in
                            
                            log?.debug("permissionResult:\(String(describing: permissionResult))")
                            if !finished {
                                log?.debug("finished 가 완료되지 않는 상태 \(finished)")
                            }
                            else {
                                if error != nil {
                                    log?.debug("error  가 발생된 상태 \(String(describing: error))")
                                }
                                else {
                                    if let permissionStatus = permissionResult?.status {
                                        self.pushResult(status: permissionStatus)
                                    }
                                }
                            }
                            
                        })
                    }
                    else {
                        self.pushResult(status: status)
                    }
                    
                }
               
                
            })
            
            
        }
      
    }
    
    func pushResult(status : PermissionStatus , final : Bool = false) {
        
        self.pushStatusLabel.text = "\(status)"
        
        
        if !final {
            if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                
                if status == .unauthorized {
                    permissionScope.showDeniedAlert(viewController: self, permission: .notifications)
                }
                else if status == .authorized {
                    (UIApplication.shared.delegate as! AppDelegate).fcmRegister()
                }
                
                /*
                 struct   PermissionResult : CustomStringConvertible {
                 public let type: PermissionType
                 public let status: PermissionStatus
                */
                permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                    
                    let result = permissionResults.first {$0.type == .notifications}
                    
                    
                    self.pushResult(status: result!.status, final: true)
                    
                }
                
            }
        }
      
    }
    
    
    @IBAction func btnCameraRequestAction(_ sender: Any) {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusCamera(completion: { (status) in
                
                if status == .unknown {
                    
                    permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                        
                        log?.debug("btnCameraRequestAction onAuthChange called-1")
                    }
                    
                    permissionScope.requestPermission(type: .camera, permissionResult: { (finished, permissionResult,error) in
                        
                        log?.debug("permissionResult:\(String(describing: permissionResult))")
                        if !finished {
                            log?.debug("finished 가 완료되지 않는 상태 \(finished)")
                        }
                        else {
                            if error != nil {
                                log?.debug("error  가 발생된 상태 \(String(describing: error))")
                            }
                            else {
                                if let permissionStatus = permissionResult?.status {
                                    self.camerResult(status: permissionStatus)
                                }
                            }
                        }
                        
                    })
                }
                else {
                    self.camerResult(status: status)
                }
                
                
            })
            
            
        }
    }
    
    func camerResult(status : PermissionStatus,  final : Bool = false) {
        
        self.cameraStatusLabel.text = "\(status)"
        
        if !final {
            
            if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                
                if status == .unauthorized {
                    permissionScope.showDeniedAlert(viewController: self, permission: .camera)
                }
                else if status == .authorized {
                    
                }
          
                permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                    
                    let result = permissionResults.first {$0.type == .camera}
                    
                    
                    self.camerResult(status: result!.status, final: true)
                    
                }
                
            }
        }
        
        
    }
    
    func photoResult(status : PermissionStatus ,  final : Bool = false) {
        
        self.photoStatusLabel.text = "\(status)"
        
        if !final {
            
            if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                
                if status == .unauthorized {
                    permissionScope.showDeniedAlert(viewController: self, permission: .photos)
                }
                else if status == .authorized {
                    log?.debug("authorized")
                }
                
                permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                    
                    let result = permissionResults.first {$0.type == .photos}
                    
                    self.photoResult(status: result!.status, final: true)
                    
                }
                
            }
        }
        
    }
    
    
    func contactResult(status : PermissionStatus ,  final : Bool = false) {
        
        self.contactStatusLabel.text = "\(status)"
        
        if !final {
            
            if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
                
                if status == .unauthorized {
                    permissionScope.showDeniedAlert(viewController: self, permission: .contacts)
                }
                else if status == .authorized {
                    
                }
                
                permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                    
                    let result = permissionResults.first {$0.type == .contacts}
                    
                    
                    self.contactResult(status: result!.status, final: true)
                    
                }
                
            }
        }
        
    }
    
    
    
    
    
    
    @IBAction func btnPhotoRequestAction(_ sender: Any) {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusPhotos(completion: { (status) in
                
                if status == .unknown {
                    
                    permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                        
                        log?.debug("btnPhotoRequestAction onAuthChange called-1")
                    }
                    
                    permissionScope.requestPermission(type: .photos, permissionResult: { (finished, permissionResult,error) in
                        
                        log?.debug("permissionResult:\(String(describing: permissionResult))")
                        if !finished {
                            log?.debug("finished 가 완료되지 않는 상태 \(finished)")
                        }
                        else {
                            if error != nil {
                                log?.debug("error  가 발생된 상태 \(String(describing: error))")
                            }
                            else {
                                if let permissionStatus = permissionResult?.status {
                                    self.photoResult(status: permissionStatus)
                                }
                            }
                        }
                        
                    })
                }
                else {
                    self.photoResult(status: status)
                }
                
                
            })
            
            
        }
    }
    
    @IBAction func btnContactRequestAction(_ sender: Any) {
        
        if let permissionScope = ApplicationShare.shared.permissionInfo.permissionScope {
            
            permissionScope.statusContacts(completion: { (status) in
                
                if status == .unknown {
                    
                    permissionScope.onAuthChange = { (allSuccess, permissionResults ) in
                        
                        log?.debug("btnPhotoRequestAction onAuthChange called-1")
                    }
                    
                    permissionScope.requestPermission(type: .contacts, permissionResult: { (finished, permissionResult,error) in
                        
                        log?.debug("permissionResult:\(String(describing: permissionResult))")
                        if !finished {
                            log?.debug("finished 가 완료되지 않는 상태 \(finished)")
                        }
                        else {
                            if error != nil {
                                log?.debug("error  가 발생된 상태 \(String(describing: error))")
                            }
                            else {
                                if let permissionStatus = permissionResult?.status {
                                    self.contactResult(status: permissionStatus)
                                }
                            }
                        }
                        
                    })
                }
                else {
                    self.contactResult(status: status)
                }
                
                
            })
            
            
        }
    }
    
}
