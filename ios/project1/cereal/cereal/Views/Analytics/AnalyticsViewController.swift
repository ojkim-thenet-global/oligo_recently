//
//  AnalyticsViewController.swift
//  pushTutorial
//
//  Created by srkang on 2018. 6. 25..
//  Copyright © 2018년 srkang. All rights reserved.


/*
 *
 *
 
        테스트 용도로 사용함.
 *
 *
 *
 */

import UIKit
import Firebase

class AnalyticsViewController: UIViewController {

    override func viewDidLoad() {
        
        title = "GA 테스트페이지"
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonPressed(_ sender: Any) {
        Analytics.logEvent("press_button", parameters: nil)
        
        //kFIREventSelectContent NS_SWIFT_NAME(AnalyticsEventSelectContent) =
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: "id-\(title!)",
            AnalyticsParameterItemName: title!,
            AnalyticsParameterContentType: "cont"
            ])
        
    }
    
    
    @IBAction func switchChanged(_ sender: Any) {
        //Analytics.logEvent("flip_switch", parameters: nil)
        
        let test_val = "iOS  Switch \((sender as! UISwitch).isOn)"
        //Analytics.logEvent("flip_switch", parameters: ["test_val":  test_val])
        Analytics.logEvent("begin_checkout", parameters: ["test_val":  test_val])
        
        
    }
    
    
    @IBAction func sliderChanged(_ sender: Any) {
        
        let test_val = "iOS  Slider \((sender as! UISlider).value)"
        
      //  Analytics.logEvent("adjust_slider", parameters: ["nevValue": (sender as! UISlider) .value])
        
        Analytics.logEvent("begin_checkout", parameters: ["test_val":  test_val])
        
        
    }
    
    deinit {
        log?.debug("AnalyticsViewController deinit")
    }

}
