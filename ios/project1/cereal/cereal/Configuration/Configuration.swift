//
//  Configuration.swift
//  cereal
//
//  Created by srkang on 2018. 7. 3..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit

enum ServerHost : String {
    case debug
    case real
    
    func isReal() -> Bool {
        switch self {
        case .real:
            return true
        default:
            return false
        }
    }
}

// 애플리케이션 환경 정보 , plist 로 부터 정보 가져옴
class Configuration  {

    static let shared = Configuration()
    static var serverHost : ServerHost = .real
    
    static let bundleIdentifier = {
        return Bundle.main.bundleIdentifier
        //    return [[[NSBundle mainBundle] infoDictionary] objectForKey: @“CFBundleIdentifier"];
        //  NSString *appID = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleIdentifierKey];
    }()
    
    static let appVersion : String = {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    }()
        
    
    static var serverVersion : String?
    
    
    static let keyChainKey : String =  {
        
        if Configuration.appStoreScheme {
            return "keyChainAppStoreKey"
        } else {
            return "keyChainKey"
        }

    }()
    
  
    
    /// Property ServerHost
    /// 서버 주소 
    /// info.plist 의 사용자 키 serverHost 값을 읽는다.
    /// 개발 : debug , 운영 real , UAT : uat
    lazy var serverHost : ServerHost = {
        var serverHostString = Bundle.main.object(forInfoDictionaryKey: "serverHost")
        var serverHost = ServerHost(rawValue: serverHostString as! String)
        
        Configuration.serverHost = serverHost!
        
        return serverHost!
    }()
    
    /// Property timeOutForResource
    /// 세션 Configuration 에 대입 용도.  요청부터 시작해서, 응답 메시지의 마지막 부분 까지 시간. timeOutForResource 초과 되면 Fail
    /// info.plist 의 사용자 키 timeOutForResource 값을 읽는다.
    /// 디폴트 : 30초
    lazy var timeOutForResource : Float = {
        var timeOutForResource = Bundle.main.object(forInfoDictionaryKey: "timeOutForResource") as! String
        return Float(timeOutForResource)!
    }()
    
    /// Property timeOutForRequest
    /// 세션 Configuration 에 대입 용도.  요청부터 시작해서,  응답 메시지의 처음 받은 부분 까지 시간 . timeOutForRequest 초과 되면 Fail
    /// info.plist 의 사용자 키 timeOutForResource 값을 읽는다.
    /// 디폴트 : 30초
    lazy var timeOutForRequest : Float = {
      var timeOutForRequest = Bundle.main.object(forInfoDictionaryKey: "timeOutForRequest") as! String
        return Float(timeOutForRequest)!
    }()
    
    
    static var appStoreScheme : Bool = {
        var appStoreSchemeString = Bundle.main.object(forInfoDictionaryKey: "AppStoreScheme") as! String
        
        if appStoreSchemeString == "YES" {
            return true
        } else {
            return false
        }
    }()
    
  
    
    private init() {
        
    }
   
    
}
