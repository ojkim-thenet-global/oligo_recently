//
//  NetworkInterface.swift
//  cereal
//
//  Created by srkang on 2018. 7. 3..
//  Copyright © 2018년 srkang. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireActivityLogger

class NetworkInterface: NSObject {

    
    static let URL_LOGIN            = "api/login.do"                // 로그인
    static let URL_LOGOUT           = "api/logout.do"               // 로그아웃
    static let URL_USER_ASSET_INFO  = "api/userAssetInfo.do"        // 자산정보 갱신
    static let URL_REGISTER_PROFILE = "api/uploadImg.do"            // 사용하지 않음. 프로필 이미지는 하이브리드 통해서 전송한다.
    static let URL_PUSH_TOKEN       = "api/pushToken.do"            // FCM 푸시 토큰 등록
    static let URL_VERIFY_APP       = "api/start.do"                // Intro
    static let URL_KEYBOARD_E2E     = "xkservice"                   // 보안키패드

    
    static let SERVER_ROOT_DEBUG_DOMAIN = "dm.mycereal.co.kr"
    static let SERVER_ROOT_REAL_DOMAIN  = "m.mycereal.co.kr"
    
    static let SERVER_URL_DEBUG = "https://dm.mycereal.co.kr:8443/matcs"
    static let SERVER_URL_REAL  = "https://m.mycereal.co.kr:8443/matcs"
    
    static let APPVERIFY_CHECKAPPURL_DEBUG    = "https://dm.mycereal.co.kr:8444/appshield/xasservice.do"   // 한컴 시큐어 앱위변조 1차 검증 서버 URL
    static let APPVERIFY_CHECKAPPURL_REAL     = "https://m.mycereal.co.kr:8444/appshield/xasservice.do"   // 한컴 시큐어 앱위변조 1차 검증 서버 URL
    static let APPVERIFY_APPINFO        = {
        return "cereal|\(Configuration.appVersion)"                                                // 앱위변조 등록되어 있는 앱버전 정보
    }()
    
//    static let MAIN_URL        = "https://m.mycereal.co.kr:8443/matcs/main.do"
    
    static let MAIN_URL = SERVER_URL + "/main.do"
    
    // 개발(debug)서버, 품질(uat) 서버 , 운영(real) 서버를 구별 용도로 만들었으나,
    // 실제로 운영에서 개발하고, 나중에 개발서버 분리 작업이 있을 경우 대비 함.
    //
    enum SERVER_ALL_URL  {
        case debug
        case real
        
        static let allValues = [debug, real]
        
        static func allURL() -> [String] {
            
            var array : [String] = []
            for url in SERVER_ALL_URL.allValues{
                
                if url == .debug {
                    array.append(SERVER_URL_DEBUG)
                }  else if url == .real {
                    array.append(SERVER_URL_REAL)
                }
            }
            return array
        }
    }
    
    enum  ServerOperation {
        case login
        case logout
        case user_asset_info
        case registerProfile
        case pushToken
        case verifyApp
        case keyboardE2E
        
        func URLString() -> String {
            switch self {
            case .login:
                return URL_LOGIN
            case .logout:
                return URL_LOGOUT
            case .user_asset_info:
                return URL_USER_ASSET_INFO
            case .registerProfile:
                return URL_REGISTER_PROFILE
            case .pushToken:
                return URL_PUSH_TOKEN
            case .verifyApp:
                return URL_VERIFY_APP
            case .keyboardE2E:
                return URL_KEYBOARD_E2E
            }
        }
        
        func fullURLString() -> String {
            
            let urlString = URLString()
            var url = URL(string: SERVER_URL)!
            url.appendPathComponent(urlString)
            
            return url.absoluteString
        }
        
    }
    
    static let configuration = Configuration.shared
    
 
    static var defaultManager : SessionManager! = {
        
        let sessionManager = Alamofire.SessionManager(configuration: urlSessionConfiguration())
        return sessionManager;
    }()
    
    static func urlSessionConfiguration() -> URLSessionConfiguration {
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.requestCachePolicy = .useProtocolCachePolicy // reloadIgnoringLocalCacheData
        urlSessionConfiguration.timeoutIntervalForResource = TimeInterval(configuration.timeOutForResource)
        urlSessionConfiguration.timeoutIntervalForRequest  = TimeInterval(configuration.timeOutForRequest)
        return urlSessionConfiguration
    }
    
    // configuration.serverHost 로 부터 debug,uat, real 에 따라,  서버 URL 
    static let SERVER_URL : String = {
        switch(configuration.serverHost) {
        case .debug :
            return SERVER_URL_DEBUG
        case .real :
            return SERVER_URL_REAL
        }
    }()
    
    static let APPVERIFY_CHECKAPPURL : String = {
        switch(configuration.serverHost) {
        case .debug :
            return APPVERIFY_CHECKAPPURL_DEBUG
        case .real :
            return APPVERIFY_CHECKAPPURL_REAL
        }
    }()
    
     
    
    static let SERVER_ROOT_DOMAIN : String = {
        switch(configuration.serverHost) {
        case .debug :
            return SERVER_ROOT_DEBUG_DOMAIN
        case .real :
            return SERVER_ROOT_REAL_DOMAIN
        }
    }()
    

    
    static func buildURL(serverDomainUse : Bool ,   appendString : String) -> URL {
        
        var url : URL!
        if !serverDomainUse {
            url = URL(string: appendString)!
        } else {
            let serverURLString = SERVER_URL as NSString
            let appendURLString = serverURLString.appendingPathComponent(appendString)
            
            url  = URL(string: appendURLString)
        }
        
        return url
        
    }
    
    class func transferServer(viewController : UIViewController, operation: NetworkInterface.ServerOperation , parameters  : [String:Any]? = nil , completion:  @escaping ( Error?, [String:Any]? )->Void) {
   
       
        
        NetworkInterface.request(operation, method: .post, parameters: parameters,  encoding: URLEncoding.default)
            .validate().log(level:alamofireLevel).responseJSON { (response) in
                
                guard response.result.isSuccess,
                    let value = response.result.value else {
                        CommonErrorHandler.errorHandler(viewController: viewController, error: response.result.error!)
                        completion(response.result.error!,nil)
                        return
                }
                
                if let response = value as? [String:Any] {
                    completion(nil,response)
                    log?.debug("response response:\(response)")
                }
                
                log?.debug("response value:\(value)")
                log?.debug("type:\(type(of: value))")
        }
        
    }
    
    
      static  func request(
        _ operation: ServerOperation,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil)
        -> DataRequest  {
        
        let urlString = operation.URLString()
        
        var url = URL(string: SERVER_URL)!
        url.appendPathComponent(urlString)
        
        return defaultManager.request(
            url,
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers
        )
    }
    
    static func upload(
         _ operation: ServerOperation,
        multipartFormData: @escaping (MultipartFormData) -> Void,
        encodingCompletion: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        let urlString = operation.URLString()
        var url = URL(string: SERVER_URL)!
        url.appendPathComponent(urlString)
        

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        return defaultManager.upload(multipartFormData: multipartFormData, with: urlRequest, encodingCompletion: encodingCompletion)
    }
    
}
